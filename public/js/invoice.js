 function cal() {
     var currentmeter = $('#currentmeter').val();
     if (currentmeter == "") {
            $('#used_water_net').val("");
            $('#must_paid').val("")
            $('.check').addClass('error_minus_value')
            $('#submit_btn').addClass('hidden');
     } else {
        var lastmeter = $('#lastmeter').val();
        var price_per_unit = $('#price_per_unit').val();
        var used_water_net = currentmeter - lastmeter;
        $('#used_water_net').val(currentmeter);
        var must_paid = used_water_net * price_per_unit
        $('#must_paid').val(must_paid);
        if (used_water_net < 0) {
             $('.check').addClass('error_minus_value')
             $('#submit_btn').addClass('hidden');
        } else {
             $('.check').removeClass('error_minus_value')
             $('#submit_btn').removeClass('hidden');
        }

     }
 }
