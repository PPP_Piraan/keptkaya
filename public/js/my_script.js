$.fn.datepicker.dates['ww'] = {
    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    months: ["Januaryw", "Februaryw", "March", "April", "Mayw", "Junew", "Julyw", "August", "September", "October", "November", "December"],
    monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    today: "Today",
    clear: "Clear",
    format: "mm/dd/yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};


function printtag(tagid) {
    // var hashid = "#"+ tagid;
    // var tagname =  $(hashid).prop("tagName").toLowerCase() ;
    // var attributes = ""; 
    // var attrs = document.getElementById(tagid).attributes;
    //     $.each(attrs,function(i,elem){
    //     attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
    //     })
    // var divToPrint= $(hashid).html() ;
    // var head = "<html><head>"+ $("head").html() + "</head>" ;
    // var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
    // var newWin=window.open('','Print-Window');
    // newWin.document.open();
    // newWin.document.write(allcontent);
    // newWin.document.close();
    // setTimeout(function(){newWin.close();},10);
}

function exportExcel(_filename){
    $("#DivIdToExport").table2excel({
        // exclude CSS class
        exclude: ".noExl",
        name: "Worksheet Name",
        filename: _filename, //do not include extension
        fileext: ".xls" // file extension
    })
}

function format(d){
    let text =  `
            <div class="table table-responsive border-success">
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th>เลขมิเตอร์</th>
                    <th>วันที่</th>
                    <th>รอบบิล</th>
                    <th>ยอดครั้งก่อน</th>
                    <th>ยอดปัจจุบัน</th>
                    <th>จำนวนที่ใช้</th>
                    <th>คิดเป็นเงิน(บาท)</th>
                    <th>สถานะ</th>
                    </tr>
                </thead>
                <tbody>`;
        d[0].invoice.forEach(element => {
            let status
            if(element.status == 'paid'){
                status = `<span class="text-success">ชำระแล้ว</span>`
            }else if(element.status == 'owe'){
                status = `<span class="text-danger">ค้างชำระ</span>`
            }else if(element.status == 'invoice'){
                status = `<span class="text-warning">ออกใบแจ้งหนี้แล้ว</span>`
            }
            text += `
                    <tr>
                      <td>${d[0].user_meter_infos.meternumber}</td>
                      <td>${element.updated_at_th}</td>
                      <td>${element.invoice_period.inv_period_name}</td>
                      <td>${element.lastmeter}</td>
                      <td>${element.currentmeter}</td>
                      <td>${element.currentmeter - element.lastmeter }</td>
                      <td>${(element.currentmeter - element.lastmeter)*8 }</td>
                      <td>${status}</td>
                    </tr>
            `;
        });
                    
        text +=`</tbody>
            </table>
            </div>`;
    return text;
}

function owe_by_user_id_format(d){
    let a = 0;
    let text =  `
            <div class="table table-responsive  border border-success rounded ml-3 mr-3">
            <table class="table table-striped">
                <thead>
                    <tr class="bg-info">
                    <th class="text-center">วันที่</th>
                    <th class="text-center">รอบบิล</th>
                    <th class="text-center">ยอดครั้งก่อน</th>
                    <th class="text-center">ยอดปัจจุบัน</th>
                    <th class="text-center">จำนวนที่ใช้(หน่วย)</th>
                    <th class="text-center">คิดเป็นเงิน(บาท)</th>
                    <th class="text-center">สถานะ</th>
                    </tr>
                </thead>
                <tbody>`;
                    d[0].invoice.forEach(element => {
                        let status
                        if(element.status === 'owe' || element.status === 'invoice'){
                            if(element.status == 'owe'){
                                status = `<span class="text-danger">ค้างชำระ</span>`
                            }else if(element.status == 'invoice'){
                                status = `<span class="text-warning">กำลังออกใบแจ้งหนี้</span>`
                            }
                            let diff = element.currentmeter - element.lastmeter 
                            let total = diff > 0 ? diff * 8 : 10
                            text += `
                                    <tr>
                                    <td class="text-center">${element.updated_at_th}</td>
                                    <td class="text-center">${element.invoice_period.inv_period_name}</td>
                                    <td class="text-right">${element.lastmeter}</td>
                                    <td class="text-right">${element.currentmeter}</td>
                                    <td class="text-right">${diff }</td>
                                    <td class="text-right">${ total }</td>
                                    <td class="text-center">${status}</td>
                                    </tr>
                            `;
                            a =1;

                        }// if
                    });
                    if(a === 0){
                        console.log('sss')
                        text += `<tr><td colspan="7" class="text-center h4">ไม่พบข้อมูลการค้างชำระ</td></tr>`
                    }        
        text +=`</tbody>
            </table>
            </div>`;
    return text;
}

function show_cutmeter_history_by_user_format(d){
    
        let text =  `
        <center>
        <table class="table table-striped" style="width:80%">
            <thead>
                <tr class="bg-info">
                <th class="text-center">วันที่</th>
                <th class="text-center">เวลา</th>
                <th class="text-center">สถานะ</th>
                <th class="text-center">ผู้รับผิดชอบ</th>
                </tr>
            </thead>
            <tbody>`;
            d.forEach(ele => {
                text+=`<tr>
                        <td class="text-center">${ele.operate_date}</td>
                        <td class="text-center">${ele.operate_time}</td>
                        <td class="text-center">${ele.status}</td>
                        <td class="text-center">`;
                            ele.twmans.forEach(e => {

                                text += `<div>${e.name}</div>`;
                            })
                text +=` </td>
                        </tr>`;
            });
        text +=`</tbody>
        </table> 
        </center>`;
        return text;
}

//input ค้นหา ใน datatable