<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itemsCategories = [
            [1, 'ขยะรีไซเคิล', 'active', null, null],
            [2, 'ขยะมีพิษ', 'active', null, '2019-02-15 19:08:20'],
            [3, 'ขยะเปียก', 'active', null, '2019-02-15 19:08:55'],
        ];

        foreach ($itemsCategories as $itemCategory) {
            DB::table('items_categories')->insert([
                'id' => $itemCategory[0],
                'items_cate_name' => $itemCategory[1],
                'status' => $itemCategory[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}