<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countUnitsArray = [
            [1, 'กิโลกรัม', 'กก.', 'active', null, '2019-02-15 19:47:44'],
            [2, 'ลัง', 'ลัง', 'active', null, null],
            [3, 'ขวด', 'ขวด', 'active', null, null],
            [4, 'หลอด', 'หลอด', 'active', null, null],
            [5, 'อัน', 'อัน', 'active', null, null],
            [6, 'ก้อน', 'ก้อน', 'active', null, null],
            [7, 'ใบ', 'ใบ', 'active', null, null],
            [8, 'ตัว', 'ตัว', 'active', null, null],
        ];

        foreach ($countUnitsArray as $countUnits) {
            DB::table('count_units')->insert([
                'id' => $countUnits[0],
                'c_unit_name' => $countUnits[1],
                'c_unit_short_name' => $countUnits[2],
                'c_unit_status' => $countUnits[3],
                'created_at' => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);
        }
    }
}