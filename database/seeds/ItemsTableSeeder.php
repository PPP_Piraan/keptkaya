<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itemsArray = [
            [1, 'เหล็กรวม', '1', 1, 7.00, 6.00, 10.00, 1, 17, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [2, 'เหล็กหล่อ', '1', 1, 7.00, 5.00, 20.50, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [3, 'สังกะสี', '1', 1, 5.00, 3.00, 0.00, 1, 19, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [4, 'กระดาษแข็ง', '1', 2, 4.00, 3.00, 0.00, 1, 30, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [5, 'กระดาษสี', '1', 2, 3.00, 2.00, 0.00, 1, 27, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [6, 'กระดาษปูน', '1', 2, 2.00, 1.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [7, 'หนังสือพิมพ์', '1', 2, 3.50, 3.00, 0.00, 1, 12, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [8, 'กระดาษขาว-ดำ', '1', 2, 6.00, 7.00, 0.00, 1, 15, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [9, 'รองเท้าแตะยาง', '1', 3, 1.50, 1.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [10, 'รองเท้าบู้ท', '1', 3, 7.00, 5.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [11, 'สายยาง', '1', 3, 2.00, 1.00, 0.00, 1, 1, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [12, 'อลูมิเนียม', '1', 5, 35.00, 30.00, 0.00, 1, 1, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [13, 'อลูมิเนียมกระป๋อง', '1', 5, 34.00, 30.00, 0.00, 1, 13, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [14, 'อลูมิเนียมติด กระทะ', '1', 5, 22.00, 20.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [15, 'อลูมิเนียมกันสาด', '1', 5, 12.00, 10.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [16, 'อลูมิเนียมมุ้งลวด', '1', 5, 12.00, 10.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [17, 'อลูมิเนียมหม้อน้ำ', '1', 5, 23.00, 20.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [18, 'อลูมิเนียมฝาแกะ', '1', 5, 21.00, 20.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [19, 'อลูมิเนียมฝาไม่แกะ', '1', 5, 12.00, 10.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [20, 'สแตนเลส', '1', 5, 17.00, 15.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [21, 'สแตนเลสติดเหล็ก', '1', 5, 4.00, 3.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [22, 'ทองแดง', '1', 4, 155.00, 150.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [23, 'ทองเหลือง', '1', 6, 92.00, 90.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [24, 'ทองเหลืองติดเหล็ก', '1', 6, 43.00, 40.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [25, 'ทองเหลืองหม้อน้ำ', '1', 6, 82.00, 80.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [26, 'ตะกั่วแข็ง', '1', 7, 22.00, 20.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [27, 'ตะกั่วติดเหล็ก', '1', 7, 13.00, 10.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [28, 'แบตเตอรี่-ขาว', '1', 7, 0.00, 25.00, 0.00, 8, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [29, 'แบตเตอรี่-ดำ', '1', 7, 0.00, 15.00, 0.00, 8, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [30, 'แบตเตอรี่-เล็ก', '1', 7, 0.00, 25.00, 0.00, 8, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [31, 'พลาสติกอ่อน', '1', 8, 7.00, 6.00, 0.00, 1, 52, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2020-01-27 15:08:26'],
            [32, 'พลาสติก-แข็ง', '1', 8, 2.00, 1.00, 0.00, 1, 24, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [33, 'พลาสติก-ขาว   [ขุ่น]', '1', 8, 18.00, 15.00, 0.00, 1, 19, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [34, 'พลาสติก-เพชร', '1', 8, 9.50, 6.00, 0.00, 1, 85, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2020-01-27 15:10:07'],
            [35, 'พี.วี.ซี. [ฟ้า]', '1', 8, 0.00, 4.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [36, 'ข้อต่อ พี.วี.ซี', '1', 8, 0.00, 2.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [37, 'เปลือกสายไฟ', '1', 8, 0.00, 1.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [38, 'พัดลม [ตัวละ]', '1', 8, 0.00, 15.00, 0.00, 8, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [39, 'ถังปูน [ใบละ]', '1', 8, 0.00, 0.50, 0.00, 7, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [40, 'เสื่อน้ำมัน', '1', 8, 0.00, 0.50, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [41, 'ขวดน้ำปลา [ใบละ]', '1', 9, 2.00, 27.50, 0.00, 2, 8, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [42, 'น้ำปลา [กล่อง]', '1', 9, 20.00, 18.00, 0.00, 2, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [43, 'เบียร์ลีโอ [กล่องละ]', '1', 9, 12.00, 10.00, 0.00, 2, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [44, 'เบียร์ช้างเขียว [กล่องละ]', '1', 9, 0.00, 7.00, 0.00, 2, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [45, 'เหล้าเล็ก [กล่องละ]', '1', 9, 18.00, 15.00, 0.00, 2, 4, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [46, 'ขวดน้ำอัดลมเล็กพร้อมลัง', '1', 9, 0.00, 80.00, 0.00, 2, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [47, 'ขวดน้ำอัดลมเล็ก[ใบละ]', '1', 9, 0.00, 0.00, 0.00, 7, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [48, 'ขวดน้ำอัดลมใหญ่ [ใบละ]', '1', 9, 0.00, 1.00, 0.00, 7, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [49, 'ลังเปล่าอัดลมเล็ก [ใบละ]', '1', 9, 0.00, 0.00, 0.00, 7, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [50, 'ลังเปล่าอัดลมใหญ่ [ใบละ]', '1', 9, 0.00, 50.00, 0.00, 7, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [51, 'ลังเปล่าโซดา [ใบละ]', '1', 9, 0.00, 0.00, 0.00, 7, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [52, 'ขวดโซดาสิงห์ [ใบละ]', '1', 9, 0.00, 0.00, 0.00, 7, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [53, 'ขวดน้ำอัดลมใหญ่พร้อมลัง', '1', 9, 0.00, 100.00, 0.00, 2, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [54, 'เศษแก้วขาว', '1', 1, 1.50, 1.00, 0.00, 1, 39, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [55, 'เศษแก้วแดง', '1', 1, 1.00, 1.00, 0.00, 1, 38, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2020-01-26 14:20:41'],
            [56, 'นุ่นเก่า+นุ่นใหม่', '1', 1, 0.00, 0.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [57, 'ข้าวแห้ง', '1', 1, 0.00, 3.00, 0.00, 1, 2, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [58, 'เม็ดนุ่น', '1', 1, 0.00, 2.50, 0.00, 1, 1, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2020-01-27 15:10:07'],
            [59, 'เศษเทียน', '1', 1, 0.00, 3.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [60, 'แผ่นซีดี', '1', 1, 0.00, 5.00, 0.00, 1, 0, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:18'],
            [61, 'กล่องนม', '1', 1, 0.25, 0.25, 0.00, 1, 15, 'active', 'trashes.jpg', '2019-02-15 18:48:35', '2019-12-24 02:44:17'],
            [62, 'แก้วเขียว', '1', 13, 7.00, 1.00, 0.00, 1, 18, 'active', 'trashes.jpg', '2019-03-04 01:14:07', '2019-12-24 02:44:17'],
            [63, 'ลังลีโอ', '1', 13, 12.00, 10.00, 0.00, 1, 23, 'active', 'trashes.jpg', '2019-03-04 01:18:35', '2019-12-24 02:44:17'],
            [64, 'ลังช้าง', '1', 13, 12.00, 10.00, 0.00, 1, 9, 'active', 'trashes.jpg', '2019-03-04 01:19:03', '2019-12-24 02:44:17'],
            [65, 'ถุงพลาสติก', '1', 13, 2.00, 1.00, 0.00, 1, 14, 'active', 'trashes.jpg', '2019-03-04 01:19:43', '2019-12-24 02:44:17'],
            [66, 'ลังน้ำปลา', '1', 13, 6.00, 5.00, 0.00, 1, 8, 'active', 'trashes.jpg', '2019-03-04 01:21:03', '2019-12-24 02:44:17'],
        ];

        foreach ($itemsArray as $items) {
            DB::table('items')->insert([
                'id' => $items[0],
                'name' => $items[1],
                'items_cate_id' => $items[2],
                'items_group_id' => $items[3],
                'factory_price' => $items[4],
                'price' => $items[5],
                'reward_point' => $items[6],
                'count_unit_id' => $items[7],
                'favorite' => $items[8],
                'status' => $items[9],
                'image' => $items[10],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}