<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itemsGroupArray = [
            [1, 'เหล็ก', 1],
            [2, 'กระดาษ', 1],
            [3, 'รองเท้า-สายยาง', 1],
            [4, 'ทองแดง', 1],
            [5, 'อลูมิเนียม', 1],
            [6, 'ทองเหลือง', 1],
            [7, 'ตะกั่ว', 1],
            [8, 'พลาสติก', 1],
            [9, 'ขวด', 1],
            [10, 'เศษแก้ว', 1],
            [11, 'นุ่น', 1],
            [12, 'ขยะมีพิษ', 2],
            [13, 'อื่นๆ', 1],

        ];

        foreach ($itemsGroupArray as $itemGroup) {
            DB::table('items_group')->insert([
                'id' => $itemGroup[0],
                'items_group_name' => $itemGroup[1],
                'items_cate_id' => $itemGroup[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}