<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->string('invoice');
            $table->integer('buyer_id');
            $table->integer('seller_id');
            $table->date('buy_date');
            $table->time('buy_time');
            $table->float('balance',8,2);
            $table->enum('status', ['active', 'inactive']);
            $table->integer('recorder');
            $table->timestamps();
            $table->primary('invoice');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items');
    }
}
