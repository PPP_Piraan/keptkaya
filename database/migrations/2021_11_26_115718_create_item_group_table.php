<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('items_group_name');
            $table->integer('items_cate_id');
            $table->timestamps();
            // $table->foreign('items_cate_id')->references('items_cate_id')
            //     ->on('items_categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_group');
    }
}