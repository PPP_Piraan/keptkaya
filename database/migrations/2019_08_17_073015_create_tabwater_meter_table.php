<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabwaterMeterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabwater_meter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('typemetername');
            $table->float('price_per_unit', 5,2);
            $table->integer('metersize')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabwater_meter');
    }
}
