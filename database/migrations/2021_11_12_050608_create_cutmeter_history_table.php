<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCutmeterHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutmeter_history', function (Blueprint $table) {
            $table->id();
            $table->integer('cutmeter_id');
            $table->integer('user_id');
            $table->string('operate_date');
            $table->string('operate_time');
            $table->string('twman_id')->comment('ถ้ามากกว่า 1 ให้ใส่ , คั่น');
            $table->enum('status', ['disambled', 'paid_and_installing', 'complete', 'cancel']);
            $table->integer('pending')->default(0)->comment('0=>ปกติ, 1=>state ปัจจุบัน,2=>สำเร็จ');
            $table->string('comment')->nullable();
            $table->integer('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutmeter_history');
    }
}