<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_id');
            $table->integer('item_id')->comment('id ขยะ');
            $table->float('price',8,2);
            $table->float('amount',8,2);
            $table->float('balance',8 ,2);
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();

            $table->foreign('invoice_id')->references('invoice')->on('purchase_items')
                ->onDelete('cascade')->onUpdate('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items_transaction');
    }
}
