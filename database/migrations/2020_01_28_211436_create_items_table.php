<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('items_group_id');
            $table->float('factory_price', 8, 2);
            $table->float('price', 8, 2);
            $table->float('reward_point', 8, 2);
            $table->integer('count_unit_id');
            $table->integer('favorite');
            $table->enum('status', ['active', 'inactive']);
            $table->string('image');
            $table->timestamps();
            // $table->foreign('items_group_id')->references('items_group_id')
            //     ->on('items_group')->onDelete('cascade')->onUpdate('cascade');
            // $table->foreign('count_unit_id')->references('count_unit_id')
            //     ->on('count_unit')->onDelete('cascade')->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}