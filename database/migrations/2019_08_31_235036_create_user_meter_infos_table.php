<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMeterInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meter_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('meternumber');
            $table->integer('metertype');
            $table->integer('counter_unit');
            $table->integer('metersize');
            $table->integer('undertake_zone_id');
            $table->integer('undertake_subzone_id');
            $table->date('acceptace_date');
            $table->enum('status', ['active', 'inactive', 'cutmeter']);
            $table->string('comment')->nullable();
            $table->integer('deleted')->default(0);
            $table->integer('owe_count')->default(0);
            $table->integer('payment_id');
            $table->integer('discounttype');
            $table->integer('recorder_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_meter_infos');
    }
}