<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoicePeriod extends Model
{
    protected $table = 'invoice_period';
    protected $fillable = ['status'];
    public  function budgetyear()
    {
        return $this->belongsTo('App\BudgetYear', 'budgetyear_id', 'id');
    }
}
