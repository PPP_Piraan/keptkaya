<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];
    protected $table = 'users';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_profile()
    {
        return $this->hasOne('App\UserProfile');
    }

    public function usercategory(){
        return $this->belongsTo('App\Usercategory', 'user_cat_id');
    }

    public function invoice(){
        return $this->hasMany('App\Invoice', 'user_id');
    }

    public function undertaker_subzone(){
        return $this->hasMany('App\UndertakerSubzone', 'twman_id');
    }

    public function usermeter_info(){
        return $this->hasOne('App\UserMeterInfos', 'user_id');
    }
}
