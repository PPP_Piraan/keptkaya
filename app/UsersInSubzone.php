<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersInSubzone extends Model
{
    protected $table = 'users_in_subzone';

    public function user(){
        
        return $this->belongsTo('App\User','user_id','id');
    }
    public function subzone(){
        
        return $this->belongsTo('App\Subzone','id','subzone_id');
    }
}
