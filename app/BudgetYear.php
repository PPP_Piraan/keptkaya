<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetYear extends Model
{
    protected $table = 'budget_year';

    public function invoicePeriod(){
        return $this->hasMany('App\InvoicePeriod','budgetyear_id', 'id'); 
    }
}
