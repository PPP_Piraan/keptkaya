<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CutmeterHistory extends Model
{
    protected $table = 'cutmeter_history';
}