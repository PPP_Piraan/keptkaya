<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';

    public function invoice_period(){
        return $this->belongsTo('App\InvoicePeriod', 'inv_period_id', 'id');
    }
    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function user_profile(){
        return $this->belongsTo('App\UserProfile', 'user_id', 'user_id');
    }

    public function recorder(){
        return $this->belongsTo('App\User', 'recorder_id');
    }

    public function usermeterinfos(){
        return $this->belongsTo('App\UserMeterInfos', 'user_id', 'user_id');
    }

    public function accounting(){
        return $this->belongsTo('App\Accounting', 'receipt_id', 'id');
    }

}
