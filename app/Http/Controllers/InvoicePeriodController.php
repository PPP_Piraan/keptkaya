<?php

namespace App\Http\Controllers;

use App\BudgetYear;
use App\Http\Controllers\Api\FunctionsController;
use App\Invoice;
use App\InvoicePeriod;
use App\UserMeterInfos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoicePeriodController extends Controller
{
    public function index()
    {
        $funcCtrl = new FunctionsController();

        //1.check ว่ามีปีงบประมาณที่ active ไหม ถ้าไม่มีให้ทำการสร้างปีงบประมาณก่อน
        $budgetyearModel = BudgetYear::where('status', 'active')->get();
        $budgetyear = collect($budgetyearModel)->count();

        $invoice_periods = InvoicePeriod::with('budgetyear')
            ->where('deleted', '<>', 1)->orderBy('startdate', 'desc')
            ->where('budgetyear_id', $budgetyearModel[0]->id)
            ->get();

        foreach ($invoice_periods as $invoice_period) {
            $invoice_period->startdate = $funcCtrl->engDateToThaiDateFormat($invoice_period->startdate);
            $invoice_period->enddate = $funcCtrl->engDateToThaiDateFormat($invoice_period->enddate);
        }

        return view('invoice_period.index', compact('invoice_periods', 'budgetyear'));
    }

    public function create()
    {
        $budgetyear = BudgetYear::where('status', 'active')->first();
        if (collect($budgetyear)->isEmpty()) {
            //ยังไม่ได้สร้างปีงบประมาณ ให้ redirect ไปสร้าง
            return redirect()->action('BudgetYearController@index');
        }
        return view('invoice_period.create', compact('budgetyear'));
    }

    public function create_invoices($id)
    {
        //สร้างใบแจ้งหนี้เริ่มต้นของแต่ละ รอบบิลใหม่
        return $invoice_period = InvoicePeriod::with('budgetyear')->where('id', $id)->get()->first();

        return view('invoice_period.create_invoices', \compact('invoice_period'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'startdate' => 'required',
            'enddate' => 'required',
            'inv_period_name' => 'required',
        ]);
        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();

        //update invoice_period เดิม ล่าสุดให้เป็น status == inactive
        $last_prev_inv_period_sql = InvoicePeriod::where('status', 'active');
        $last_prev_inv_period = $last_prev_inv_period_sql->get();

        if (collect($last_prev_inv_period)->isNotEmpty()) {
            $last_prev_inv_period_update = $last_prev_inv_period_sql->update([
                'status' => 'inactive',
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $find_status_eq_invoice_in_inv_table_sql = Invoice::where('inv_period_id', $last_prev_inv_period[0]->id)
                ->where('status', 'invoice');

            $inv_status_eq_invoice = $find_status_eq_invoice_in_inv_table_sql->get(['user_id']);
            //ทำการ เพิ่ม owe_count ใน user_meter_infos  ของแต่ละ user
            foreach ($inv_status_eq_invoice as $item) {
                $user_meter_infos_get_owe_count_sql = UserMeterInfos::where('user_id', $item->user_id)
                    ->where('deleted', 0);
                $user_meter_infos_get_owe_count = $user_meter_infos_get_owe_count_sql->get(['owe_count']);
                $owe_count_increase = $user_meter_infos_get_owe_count[0]->owe_count + 1;

                // update owe_count ถ้า >=3 ให้ update status == cutmeter
                $user_meter_infos_get_owe_count_update = $user_meter_infos_get_owe_count_sql->update([
                    'owe_count' => $user_meter_infos_get_owe_count[0]->owe_count + 1,
                    'status' => $owe_count_increase >= 3 ? 'cutmeter' : 'active',
                    'comment' => $owe_count_increase >= 3 ? 'ค้างชำระเกิน 3 งวด' : '',
                ]);

            }

            //ทำการ  update  invoice table โดย ให้ status ของ inv_peroid_id
            //ล่าสุดที่เป็น inactive  -> status = 'owe'
            $updatePrevInvPrdInInvStatusToOwe = $find_status_eq_invoice_in_inv_table_sql->update([
                'status' => 'owe',
                'recorder_id' => Auth::id(),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        }

        //บันทึกข้อมูล รอบบิล ใหม่
        $invoice_period = new InvoicePeriod();
        $invoice_period->inv_period_name = $request->get('inv_period_name') . "-" . $request->get('inv_period_name_year');
        $invoice_period->budgetyear_id = $request->get('budgetyear_id');
        $invoice_period->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('startdate'));
        $invoice_period->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('enddate'));
        $invoice_period->created_at = date('Y-m-d H:i:s');
        $invoice_period->updated_at = date('Y-m-d H:i:s');
        $invoice_period->save();

        return redirect('/invoice_period')->with(['message' => 'ทำการเพิ่มข้อมูลเรียบร้อยแล้ว']);
    }

    public function edit($id)
    {
        $funcCtrl = new FunctionsController();

        $invoice_period = InvoicePeriod::with('budgetyear')->find($id);
        $invoice_period->startdate = $funcCtrl->engDateToThaiDateFormat($invoice_period->startdate);
        $invoice_period->enddate = $funcCtrl->engDateToThaiDateFormat($invoice_period->enddate);
        return view('invoice_period.edit', compact('invoice_period'));
    }

    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();
        $invoice_period = InvoicePeriod::find($id);
        $invoice_period->inv_period_name = $request->get('inv_period_name');
        $invoice_period->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('startdate'));
        $invoice_period->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('enddate'));
        $invoice_period->status = $request->get('status');
        $invoice_period->save();

        return redirect('/invoice_period')->with(['message' => 'ทำการอัพเดทข้อมูลเรียบร้อยแล้ว']);
    }

    public function delete($id)
    {
        // ถ้ารอบบิลนี้มีการใช้ในการบันทึก invoice แล้วแจ้งเตือนก่อนว่าจะต้องการลบไหม
        $delete = InvoicePeriod::find($id);
        $delete->update([
            'status' => 'deleted',
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return redirect('/invoice_period')->with(['message' => 'ทำการลบข้อมูลเรียบร้อยแล้ว']);
    }
}