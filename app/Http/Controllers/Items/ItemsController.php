<?php

namespace App\Http\Controllers\Items;

use App\Http\Controllers\Controller;
use App\Http\Controllers\utilities\DatetimeController;
use App\Http\Controllers\utilities\ImageController;
use App\Models\bottles\Bottles;
use App\Models\Items\Items;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    private $update_price_items = ['id', 'name', 'items_cate_id', 'factory_price', 'price', 'reward_point', 'items_group_id', 'count_unit_id', 'image', 'updated_at'];

    public function __construct()
    {
        //
    }

    private $response = array('status' => 1);
    private $path = 'images/';
    private $items = [
        'items.id',
        'items.name',
        'items.price',
        'items.image',
        'items.favorite',
        'items.status',
        'items.updated_at',
        'item_categories.name as item_categories_name',
        'unit_categories.name as unit_name',
    ];

    public function index()
    {
        $items = Items::where('status', 'active')->orderBy('favorite', 'desc')->get($this->update_price_items);
        return view('items.items.index', compact('items'));

    }

    private function getItems()
    {
        $results = Items::where(['items.status' => 1])
            ->leftJoin('item_categories', 'items.item_cate_id', '=', 'item_categories.id')
            ->leftJoin('unit_categories', 'items.unit_id', '=', 'unit_categories.id')
            ->get($this->items);

        $image = new ImageController;
        $results = $image->getImagesUrl($results, $this->path, $index = 'image');
        $datetime = new DatetimeController;
        $results = $datetime->setDateTimeTH($results);

    }

    public function create(Request $request)
    {
        $result = new Items;
        $result->name = $request->name;
        $result->item_cate_id = $request->item_cate_id;
        $result->price = $request->price;
        $result->unit_id = $request->unit_id;
        $result->favorite = $request->favorite;
        $result->status = 1;
        $result->save();
        // $this->response['lastId'] = $result->id;
        return response()->json($this->response, 201);
    }

    public function edit(Request $request)
    {
        $result = Items::find($request->id);
        $result->name = $request->name;
        $result->item_cate_id = $request->item_cate_id;
        $result->price = $request->price;
        $result->unit_id = $request->unit_id;
        $result->favorite = $request->favorite;
        $result->status = 1;
        $result->updated_at = date("Y-m-d H:i:s");
        $result->save();

        return response()->json($this->response, 200);
    }

    public function delete($id)
    {
        $results = Items::find($id);
        $results->status = 0;
        $results->save();

        return response()->json($this->response, 204);
    }

    public function get_bottle_barcode($bacode)
    {
        $result = Bottles::where(['barcode' => $bacode])
            ->leftJoin('items', 'items.id', '=', 'bottles.items_id')
            ->get(['items.*', 'bottles.*']);
        return response()->json($result, 200);
    }

}