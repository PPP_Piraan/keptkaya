<?php

namespace App\Http\Controllers;

use App\Accounting;
use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoiceController as ApiInvoiceCtrl;
use App\Invoice;
use App\InvoicePeriod;
use App\Settings;
use App\Subzone;
use App\User;
use App\UserMeterInfos;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    private $apiInvoiceCtrl;
    public function __construct(ApiInvoiceCtrl $apiInvoiceCtrl)
    {
        $this->apiInvoiceCtrl = $apiInvoiceCtrl;
    }
    public function index($_budgetyearId = '', $_invPeriod = '')
    {
        //ปรับ owe_count ใน user_meter_infos table ใหม่
        // return $this->manageOweCount();
        $funcCtrl = new FunctionsController();
        $currentInvPeriod = InvoicePeriod::where('status', 'active')->get('id')->first();
        if (collect($currentInvPeriod)->isEmpty()) {
            //ถ้ายังไม่มีการสร้างรอบบิลเลย ให้กลับหน้า index
            return view('invoice.index', ['invoice_period' => $currentInvPeriod]);
        }

        //หาว่าเป็นรอบบิลใหม่นี้มีdata แล้วหรือยัง
        $findNewInvPeriod = Invoice::where('inv_period_id', $currentInvPeriod->id)->count();

        //หาสมาชิกผู้ใช้น้ำที่ใช้งานอยู่
        $userMeterInfos = UserMeterInfos::orderBy('undertake_zone_id')
            ->where('status', 'active')
            ->where('deleted', 0)
            ->orderBy('id', 'asc')
            ->get(['id', 'user_id', 'undertake_zone_id', 'undertake_subzone_id']);

        $userMeterInfosGroup = collect($userMeterInfos)->groupBy(['undertake_zone_id', 'undertake_subzone_id']);

        $zones = [];
        foreach ($userMeterInfosGroup as $users_in_zone) {
            //จัดการข้อมูลแยกตามหมู่ที่ สังกัด
            foreach ($users_in_zone as $key => $user_in_zone) {
                $invoice_sum = 0;
                if ($findNewInvPeriod == 0) {
                    // table =0; ยังไม่มีการบันทึกข้อมูล ของรอบบิลใหม่
                } else {
                    $invoicesTemp = UserMeterInfos::where('undertake_subzone_id', $key)
                        ->where('deleted', 0)
                        ->with(['invoice' => function ($query) use ($currentInvPeriod) {
                            return $query->where('inv_period_id', $currentInvPeriod->id)
                                ->where('status', 'invoice')
                                ->get()->first();
                        }])
                        ->get();
                    $invoices = collect($invoicesTemp)->filter(function ($inv) {
                        return collect($inv->invoice)->isNotEmpty();
                    });

                    foreach ($invoices as $inv) {
                        $sum = ($inv->invoice[0]['currentmeter'] - $inv->invoice[0]['lastmeter']) * 8;
                        $sumtemp = $sum == 0 ? 10 : $sum;
                        $invoice_sum += $sumtemp == 0 ? 10 : $sumtemp;

                    }
                } //else

                $subzone = Subzone::where('id', $user_in_zone[0]->undertake_subzone_id)->get(['id', 'zone_id', 'subzone_name'])->first();
                //หา จำนวนที่โยนยกหม้อมิเตอร์ใน subzone นี้
                $cutmeter_count = UserMeterInfos::where('status', 'cutmeter')
                    ->where('undertake_subzone_id', $user_in_zone[0]->undertake_subzone_id)->count();
                array_push($zones, [
                    'zone_id' => $user_in_zone[0]->undertake_zone_id,
                    'zone_name' => $subzone->zone->zone_name,
                    'subzone_id' => collect($subzone)->isEmpty() ? 999 : $subzone->id,
                    'subzone_name' => collect($subzone)->isEmpty() ? 999 : $subzone->subzone_name,
                    'total' => collect($user_in_zone)->count(),
                    //function invStatusCountBySubzone() นับจำนวนตามสถานะ
                    'invoice' => $this->invStatusCountBySubzone('invoice', $key, $currentInvPeriod->id),
                    'owes' => $this->invStatusCountBySubzone('owe', $key, $currentInvPeriod->id),
                    'paid' => $this->invStatusCountBySubzone('paid', $key, $currentInvPeriod->id),
                    'invoice_sum' => $invoice_sum,
                    'cutmeter_count' => $cutmeter_count,
                ]);
            }
        } //foreach

        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

        return view('invoice.index', compact('zones', 'invoice_period'));
    }

    private function manageOweCount()
    {
        $aa = UserMeterInfos::
            with([
            'invoice' => function ($query) {
                return $query->select('inv_period_id', 'status', 'user_id')
                    ->where('status', 'owe');
            },
        ])->where('deleted', 0)->get(['user_id', 'owe_count', 'id', 'status']);

        $arr = collect([]);
        foreach ($aa as $a) {
            //นับ invoice status เท่ากับ owe หรือ invioce แล้วทำการ update
            //owe_count ให้  user_meter_infos ใหม่
            $oweInvCount = collect($a->invoice)->count() + 1; // บวก row  status == invoice

            $update = UserMeterInfos::where('id', $a->id)->update([
                'owe_count' => $oweInvCount,
                'status' => $oweInvCount >= 3 ? 'cutmeter' : $a->status,
            ]);
        };
        return $arr;
    }

    private function invStatusCountBySubzone($status, $subzone_id, $currentInvPeriodId)
    {
        $query = DB::table('user_meter_infos')
            ->join('invoice', 'invoice.user_id', '=', 'user_meter_infos.user_id')
        // ->join('invoice', 'invoice.meter_id', '=', 'user_meter_infos.id')
            ->where('invoice.status', $status)
            ->where('user_meter_infos.status', 'active')
            ->where('user_meter_infos.undertake_subzone_id', $subzone_id)
            ->where('invoice.inv_period_id', $currentInvPeriodId);
        if ($status != 'owe') {
            $query = $query->join('invoice_period', 'invoice_period.id', '=', 'invoice.inv_period_id')
                ->where('invoice_period.status', 'active');
        }
        return $query->count();
    }

    public function paid($id)
    {
        $inv = $this->apiInvoiceCtrl->get_user_invoice($id);
        $invoice = json_decode($inv->getContent());
        // dd($invoice);
        return view('invoice.paid', compact('invoice'));
    }

    public function create($invoice_id)
    {
        return view('invoice.create', compact('invoice_id'));
    }

    public function store(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        //filter หาเฉพาะที่มีการกรอกข้อมูลขมิเตอร์ปัจจุบัน
        $filters = collect($request->get('data'))->filter(function ($val) {
            return $val['currentmeter'] != null;
        });

        $presentInvoicePeriod = InvoicePeriod::where("status", "active")->first(); //หารอบบิลล่าสุด
        //เพิ่มข้อมูลลง invoice
        foreach ($filters as $inv) {
            $invoice = new Invoice();
            $invoice->inv_period_id = $presentInvoicePeriod->id;
            $invoice->user_id = $inv['user_id'];
            $invoice->meter_id = $inv['meter_id'];
            $invoice->lastmeter = $inv['lastmeter'];
            $invoice->currentmeter = $inv['currentmeter'];
            $invoice->status = 'invoice';
            $invoice->recorder_id = 5;
            $invoice->created_at = date('Y-m-d H:i:s');
            $invoice->updated_at = date('Y-m-d H:i:s');
            $invoice->save();

            $updateAddress = UserProfile::where('user_id', $inv['user_id'])
                ->update([
                    'address' => $inv['address'],
                    'updated_at' => date('Y-m-d H:i:s'),

                ]);
        }

        return \redirect('invoice/index')->with([
            'massage' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว',
            'color' => 'success',
        ]);
    }

    public function edit($invoice_id)
    {
        $inv = $this->apiInvoiceCtrl->get_user_invoice($invoice_id);
        $invoice = json_decode($inv->getContent());
        return view('invoice.edit', compact('invoice'));
    }

    public function update(REQUEST $request, $id)
    {
        date_default_timezone_set('Asia/Bangkok');
        $invoice = Invoice::find($id);
        $invoice->currentmeter = $request->get('currentmeter');
        $invoice->status = $request->get('status');
        $invoice->recorder_id = 5;
        $invoice->updated_at = date('Y-m-d H:i:s');
        $invoice->save();

        return \redirect('invoice/index');
    }

    public function invoiced_lists($subzone_id)
    {
        return view('invoice.invoiced_lists', compact('subzone_id'));
    }

    public function print_multi_invoice(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        if ($request->get('mode') == 'payment') {
            //การเป็นการจ่ายเงิน ให้ทำการบันทึกยอดเงินใน accounting
            foreach ($request->get('payments') as $key => $val) {
                $acc = new Accounting();
                $acc->net = $val['total'];
                $acc->recorder_id = Auth::id();
                $acc->printed_time = 1;
                $acc->status = 1;
                $acc->created_at = date('Y-m-d H:i:s');
                $acc->updated_at = date('Y-m-d H:i:s');
                $acc->save();

                //แล้ว update invoice  status = paid
                Invoice::where('id', $key)->update([
                    'status' => 'paid',
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
        }

        //เตรียมการปริ้น
        $setting_tambon_infos_json = Settings::where('name', 'tambon_infos')->get(['values']);
        $setting_tambon_infos = json_decode($setting_tambon_infos_json[0]['values'], true);
        //หาวันสุดท้ายที่จะมาชำระหนี้ได้ ให้เวลา 30 วันนับแต่ออกใบแจ้งหนี้
        $setting_invoice_expired = Settings::where('name', 'invoice_expired')->get(['values']);
        $strStartDate = date('Y-m-d');
        $invoice_expired_next30day = date("Y-m-d", strtotime("+" . $setting_invoice_expired[0]['values'] . " day", strtotime($strStartDate)));

        $invoiceArray = [];
        $apiInvoiceCtrl = new ApiInvoiceCtrl();
        foreach ($request->get('inv_id') as $key => $on) {
            if ($on == 'on') {
                $data = json_decode($apiInvoiceCtrl->get_user_invoice_by_invId_and_mode($key, $request->get('mode'))->getContent(), true);
                array_push($invoiceArray, $data);

                //บวกจำนวนครั้งที่ปริ้นใน invoice->printed_time
                $printed_time_plus = Invoice::where('id', $data['id'])->update([
                    'printed_time' => $data['printed_time'] + 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'recorder_id' => Auth::id(),
                ]);
            }
        }
        $mode = "multipage";
        $subzone_id = $request->get('subzone_id');
        return view('invoice.print', compact('invoiceArray', 'mode', 'subzone_id', 'setting_tambon_infos', 'invoice_expired_next30day'));

    }

    public function search_from_meternumber($meternumber, $zone_id)
    {
        //หาเลขมิเตอร์
        $usermeterInfos = UserMeterInfos::orWhere('meternumber', 'LIKE', '%' . $meternumber . '%')
            ->where('zone_id', $zone_id)
            ->with('user', 'user.user_profile', 'user.usermeter_info.zone')->get()->first();

        if (collect($usermeterInfos)->count() == 0) {
            return $arr = ['usermeterInfos' => null, 'invoice' => null];
        }
        $invoice = Invoice::where('user_id', $usermeterInfos->user_id)
            ->orderBy('id', 'desc')
            ->get()->first();
        return $arr = ['usermeterInfos' => $usermeterInfos, 'invoice' => $invoice];

    }

    public function not_invoiced_lists()
    {
        //แสดงตาราง user ที่ยังไม่ถูกออกใบแจ้งหนี้
        $invoice = Invoice::where('inv_period_id', 1)->get('user_id');
        $invoiced_array = collect($invoice)->pluck('user_id');
        return $users = User::whereNotIn('id', $invoiced_array)
            ->where('user_cat_id', 3)
            ->get();

    }

    public function zone_info($subzone_id)
    {
        $funcCtrl = new FunctionsController();
        $presentInvoicePeriod = InvoicePeriod::where('status', 'active')->get()->first();
        $userMeterInfos = UserMeterInfos::where('undertake_subzone_id', $subzone_id)
            ->where('status', 'active')
            ->with('user_profile', 'zone', 'subzone')
            ->orderBy('undertake_zone_id')->get(['user_id', 'undertake_zone_id', 'meternumber', 'undertake_subzone_id']);
        //หาว่า มีการบันทึก invoice ในรอบบิลปัจจุบันหรือยัง
        foreach ($userMeterInfos as $user) {
            $user->invoice = Invoice::where('inv_period_id', $presentInvoicePeriod->id)
                ->where('user_id', $user->user_id)->get()->first();
        }

        $totalMemberCount = $userMeterInfos->count();

        $memberNoInvoice = collect($userMeterInfos)->filter(function ($value, $key) {
            return collect($value->invoice)->isEmpty();
        });
        $memberHasInvoiceFilter = collect($userMeterInfos)->filter(function ($value, $key) {
            return !collect($value->invoice)->isEmpty();
        });

        $memberHasInvoice = collect($memberHasInvoiceFilter)->sortBy('user_id');

        $memberHasInvoiceCount = $totalMemberCount - collect($memberNoInvoice)->count();
        $zoneInfo = collect($userMeterInfos)->first();
        return view('invoice.zone_info', compact('zoneInfo', 'memberHasInvoice', 'memberNoInvoice'));

    }

    public function zone_edit($subzone_id)
    {
        return view('invoice.zone_edit', compact('subzone_id'));
    }

    public function zone_create($subzone_id)
    {
        $presentInvoicePeriod = InvoicePeriod::where("status", "active")->first();
        $lastInvoicePeriod = InvoicePeriod::where("status", "inactive")->orderBy('id', 'desc')->first();
        if (collect($lastInvoicePeriod)->isEmpty()) {
            //
            $lastInvoicePeriod = $presentInvoicePeriod;
        }
        $currentInvPeriod_id = $presentInvoicePeriod->id;
        $prevInvPeriod_id = $lastInvoicePeriod->id;

        //เอาค่า invoice เดือน ปัจจุบัน ของ user ใน subzone ที่เลือก
        $subzone_members = UserMeterInfos::where('undertake_subzone_id', $subzone_id)
            ->where('status', 'active')
            ->with([
                'user_profile:name,address,user_id',
                'invoice' => function ($query) use ($prevInvPeriod_id, $currentInvPeriod_id) {
                    return $query->select('inv_period_id', 'currentmeter', 'id as iv_id', 'user_id')
                        ->where('inv_period_id', '>=', $prevInvPeriod_id)
                        ->where('inv_period_id', '<=', $currentInvPeriod_id);

                },
                'zone' => function ($query) {
                    return $query->select('zone_name', 'id');
                },
                'subzone' => function ($query) {
                    return $query->select('subzone_name', 'id');
                },
            ])
            ->orderBy('user_id')
            ->get(['meternumber', 'undertake_zone_id', 'undertake_subzone_id', 'id', 'user_id']);
        $member_not_yet_recorded_present_inv_period = collect($subzone_members)->filter(function ($v) {
            return collect($v->invoice)->count() == 1;
        });
        //filter หา invoice ที่ว่าง ยังไม่ได้กรอกเลขมิเตอร์ inv_period รอบบิลปัจจุบัน

        // $memberNoInvoice = collect($zone_member)->filter(function ($val) {
        //     //filter หา invoice ที่ว่าง ยังไม่ได้กรอกเลขมิเตอร์
        //     $presentInvoicePeriod = InvoicePeriod::where("status", "active")->first();
        //     $val->invoice = Invoice::where('user_id', $val->user_id)
        //         ->where('inv_period_id', $presentInvoicePeriod->id)
        //         ->where('deleted', 0)
        //         ->get();
        //     return collect($val->invoice)->isEmpty();
        // });
        // foreach ($memberNoInvoice as $newInv) {
        //     //หาค่าเลขมิเตอร์ ของครั้งก่อน มาเป็นตั้งต้น ของเดือนปัจจุบัน
        //     $inv = Invoice::where('user_id', $newInv->use_id)
        //         ->where('inv_period_id', $lastInvoicePeriod->id)->get(['currentmeter'])->first();
        //     $newInv->lastmeter = collect($inv)->isEmpty() ? 0 : $inv['currentmeter'];
        // }
        // $totalMemberCount = $memberNoInvoice->count();
        // $zoneInfo = collect($zone_member)->first();

        // return view('invoice.zone_create', compact('presentInvoicePeriod', 'zoneInfo', 'memberNoInvoice'));
        return view('invoice.zone_create', compact('member_not_yet_recorded_present_inv_period', 'presentInvoicePeriod'));
    }
    public function zone_create2($subzone_id)
    {
        $presentInvoicePeriod = InvoicePeriod::where("status", "active")->first();
        $lastInvoicePeriod = InvoicePeriod::where("status", "inactive")->orderBy('id', 'desc')->first();
        if (collect($lastInvoicePeriod)->isEmpty()) {
            //
            $lastInvoicePeriod = $presentInvoicePeriod;
        }

        $zone_member = UserMeterInfos::where('undertake_subzone_id', $subzone_id)
            ->where('status', 'active')
            ->with('user_profile')
            ->orderBy('undertake_subzone_id')
            ->get();

        $memberNoInvoice = collect($zone_member)->filter(function ($val) {
            //filter หา invoice ที่ว่าง ยังไม่ได้กรอกเลขมิเตอร์
            $presentInvoicePeriod = InvoicePeriod::where("status", "active")->first();
            $val->invoice = Invoice::where('user_id', $val->user_id)
                ->where('inv_period_id', $presentInvoicePeriod->id)
                ->where('deleted', 0)
                ->get();
            return collect($val->invoice)->isEmpty();
        });
        foreach ($memberNoInvoice as $newInv) {
            //หาค่าเลขมิเตอร์ ของครั้งก่อน มาเป็นตั้งต้น ของเดือนปัจจุบัน
            $inv = Invoice::where('user_id', $newInv->use_id)
                ->where('inv_period_id', $lastInvoicePeriod->id)->get(['currentmeter'])->first();
            $newInv->lastmeter = collect($inv)->isEmpty() ? 0 : $inv['currentmeter'];
        }
        $totalMemberCount = $memberNoInvoice->count();
        $zoneInfo = collect($zone_member)->first();

        return view('invoice.zone_create', compact('presentInvoicePeriod', 'zoneInfo', 'memberNoInvoice'));
    }

    public function zone_update(REQUEST $request, $id)
    {
        date_default_timezone_set('Asia/Bangkok');
        $zonefilter = collect($request->get('zone'))->filter(function ($val) {
            return $val['changevalue'] == 1;
        });
        foreach ($zonefilter as $key => $vals) {
            $invoice = Invoice::where('id', $key)->update([
                "currentmeter" => $vals['currentmeter'],
                "lastmeter" => $vals['lastmeter'],
                "status" => 'invoice',
                "recorder_id" => Auth::id(),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

        }
        return \redirect('invoice/index')->with([
            'massage' => 'ทำการบันทึกการแก้ไขข้อมูลเรียบร้อยแล้ว',
            'color' => 'success',
        ]);
    }

    public function show($user_id)
    {
        $user = UserProfile::where('user_id', $user_id)
            ->with('userMeterInfos', 'invoice', 'invoice.invoice_period')
            ->get();
        return view('invoice.show', compact('user'));
    }

    public function delete($invoice_id, $comment)
    {
        $inv = Invoice::where('id', $invoice_id)->update([
            'status' => 'deleted',
            'deleted' => 1,
            'recorder_Id' => Auth::id(),
            'updated_at' => date('Y-m-d H:i:s'),
            'comment' => $comment,
        ]);
        return redirect('invoice/index')->with(['message' => 'ทำการลบข้อมูลเรียบร้อยแล้ว']);
    }

    public function invoice_by_user($type = "data", $status = "", $user_id = "")
    {
        if ($type == 'count') {
            $active_users = $this->oweSql($status)->where('umf.user_id', '=', $user_id)->count();
        } else {
            $active_users = $this->oweSql($status)->where('umf.user_id', '=', $user_id)->get();
        }
        return $active_users;
    }

    public function invoice_by_subzone($type = "data", $status = "", $subzone_id = "")
    {
        if ($type == 'count') {
            $owes = $this->oweSql($status)->where('umf.undertake_subzone_id', '=', $subzone_id)->count();
        } else {
            $owes = $this->oweSql($status)->where('umf.undertake_subzone_id', '=', $subzone_id)->get();
        }
        return $owes;
    }

    public function owe_by_all($type = "data")
    {
        if ($type == 'count') {
            $owes = $this->oweSql()->count();
        } else {
            $owes = $this->oweSql()->get();
        }
        return $owes;
    }
    private function oweSql($status)
    {
        $sql = DB::table('user_meter_infos as umf')
            ->join('user_profile as uf', 'uf.user_id', '=', 'umf.user_id')
            ->leftJoin('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
        // ->leftJoin('invoice as iv', 'iv.meter_id', '=', 'umf.id')
            ->where('umf.status', '=', 'active')
            ->where('iv.status', '=', $status)
            ->select('umf.meternumber',
                'umf.user_id',
                'iv.*',
                'uf.name', 'uf.address',
            );
        return $sql;
    }
}