<?php

namespace App\Http\Controllers;

use App\User;

class TestController extends Controller
{
    public function index()
    {
        $time_start = $this->microtime_float();
        $users = User::with('user_profile')
            ->get();

        // $users = DB::table('users')
        //     ->join('user_profile', 'users.id', '=', 'user_profile.user_id')
        //     ->get();

        // $users = DB::select('select * from users join user_profile on users.id = user_profile.user_id ');

        $time_end = $this->microtime_float();
        $time = $time_end - $time_start;
        return $time;
    }

    private function microtime_float()
    {
        list($usec, $sec) = \explode(" ", \microtime());
        return ((float) $usec + (float) $sec);
    }
}