<?php

namespace App\Http\Controllers;
use App\InvoicePeriod;
use App\BudgetYear;
use App\Http\Controllers\Api\FunctionsController;
use Illuminate\Http\Request;

class BudgetYearController extends Controller
{
    public function index(){
        $funcCtrl = new FunctionsController();

        $budgetyears = BudgetYear::orderBy('budgetyear', 'desc')->get();
        foreach($budgetyears as $budgetyear){
            $budgetyear->startdate = $funcCtrl->engDateToThaiDateFormat($budgetyear->startdate);
            $budgetyear->enddate = $funcCtrl->engDateToThaiDateFormat($budgetyear->enddate);
        }

        return view('budgetyear.index', \compact('budgetyears'));
    }

    public function create()
    {

        return view('budgetyear.create');
    }
    
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Bangkok');

        $funcCtrl = new FunctionsController();
        $budgetyear = new BudgetYear();
        $budgetyear->budgetyear = $request->get('budgetyear');
        $budgetyear->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('start'));
        $budgetyear->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('end'));
        $budgetyear->status = $request->get('status');
        $budgetyear->created_at = date('Y-m-d H:i:s');
        $budgetyear->updated_at = date('Y-m-d H:i:s');
        $last_budgetyear = BudgetYear::all();
        $invoice_period = InvoicePeriod::all();
        if($budgetyear->save()){
            foreach($last_budgetyear as $key){
                if((int)$key->budgetyear < (int)$budgetyear->budgetyear){
                    $key->status = "inactive";
                    if($key->save()){
                        foreach($invoice_period as $invoice_period_key){
                            if($key->id == $invoice_period_key->budgetyear_id){
                                $invoice_period_key->status = "inactive";
                                $invoice_period_key->save();
                            }
                        }
                    }
                }
                
            }
           
            
        }
        

        return redirect('/budgetyear');
    }

    public function edit($id)
    {
        $budgetyear = BudgetYear::find($id);
        $funcCtrl = new FunctionsController();

        $budgetyear->startdate = $funcCtrl->engDateToThaiDateFormat($budgetyear->startdate);
        $budgetyear->enddate = $funcCtrl->engDateToThaiDateFormat($budgetyear->enddate);
        
        return view('budgetyear.edit', compact('budgetyear'));
    }

    public function delete($id)
    {
        $budgetyear = BudgetYear::find($id);
        
        $budgetyear->delete();
        
        return view('budgetyear.edit', compact('budgetyear'));
    }

    public function update(Request $request, $id){
        $funcCtrl = new FunctionsController();

        $budgetyear = BudgetYear::find($id);
        $budgetyear->startdate = $funcCtrl->thaiDateToEngDateFormat($request->get('start'));
        $budgetyear->enddate = $funcCtrl->thaiDateToEngDateFormat($request->get('end'));
        $budgetyear->save();

        return redirect('/budgetyear');
    }
}
