<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*//
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    //  protected function authenticated(Request $request , $user){

    //     // $adminRole = Role::where('name', 'superadministrator')->first();
    //     // $user = User::all();
    //      if($user->hasRole('superadministrator')){
    //         return redirect('/admin');
    //      }
    //      return redirect('/');
    //  }
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = \DB::table('users')->where('username', $request->input('username'))->first();
        if (auth()->guard('web')->attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {

            $user = auth()->guard('web')->user();

            if ($user->hasRole('superadministrator') || $user->hasRole('twman')) {
                return redirect('/admin');
            }
            return redirect('/');
        }
        \Session::put('login_error', 'Your email and password wrong!!');
        return back();

    }

    public function logout(Request $request)
    {
        \Session::flush();
        // \Session::put('success', 'you are logout Successfully');
        return redirect()->to('/login');
    }

    public function username()
    {
        return 'username';
    }
}