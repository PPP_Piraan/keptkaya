<?php

namespace App\Http\Controllers;

use App\Accounting;
use App\CutmeterHistory;
use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoiceController as ApiInvoiceController;
use App\Http\Controllers\Api\OwepaperController;
use App\Http\Controllers\Api\PaymentController as ApiPaymentController;
use App\Invoice;
use App\InvoicePeriod;
use App\UserMeterInfos;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function index()
    {
        //หาจำนวน status  เท่ากับ  [invoice, owe] และ reciept_id = 0
        //ในตาราง invoice
        $apiOwepaper = new OwepaperController();
        $oweInvCountGroupByUserId = $apiOwepaper->oweAndInvoiceCount();

        $zones = Zone::all();
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

        return view('payment.index', compact('oweInvCountGroupByUserId', 'zones', 'invoice_period'));
    }

    public function create(Request $request)
    {
        $paymentsArr = [];
        $total = 0;
        foreach ($request->get('data') as $inv) {
            $v = Invoice::where('id', $inv['inv_id'])
                ->with('usermeterinfos', 'usermeterinfos.user_profile', 'invoice_period')
                ->get();
            array_push($paymentsArr, $v);

            $total += ($v[0]->currentmeter - $v[0]->lastmeter) * 8;
        }
        $payments = collect($paymentsArr)->flatten();
        return view('payment.invoice_sum', compact('payments', 'total'));
    }

    public function store(Request $request)
    {
        // return $request;
        date_default_timezone_set('Asia/Bangkok');
        $payments = collect($request->get('payments'))->filter(function ($v) {
            return isset($v['on']);
        });
        $receipt = new Accounting();
        $receipt->total = $request->get('mustpaid');
        $receipt->cashier = Auth::id();
        $receipt->created_at = date('Y-m-d H:i:s');
        $receipt->updated_at = date('Y-m-d H:i:s');
        $receipt->save();

        $apiInvCtrl = new ApiInvoiceController();
        foreach ($payments as $key => $payment) {
            //ทำการupdate invoice ให้ status เท่ากับ paid
            $update = Invoice::where('id', $payment['iv_id'])->update([
                'receipt_id' => $receipt->id,
                'status' => 'paid',
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        }
        //ทำการ update user_meter_infos table โดยการลบ owe_count
        //ของ user ถ้า มากกว่า 0 ออกไป count($payments)
        $decreateOweCountSql = UserMeterInfos::where('user_id', $request->get('user_id'));
        $decreateOweCount = $decreateOweCountSql->get('owe_count', 'comment');
        $decreateOweCountUpdate = $decreateOweCountSql->update([
            'owe_count' => $decreateOweCount[0]->owe_count > 0 ? $decreateOweCount[0]->owe_count - collect($payments)->count() : 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'comment' => $decreateOweCount[0]->owe_count - collect($payments)->count() < 3 ? '' : $decreateOweCount[0]->comment,
        ]);

        //ทำการสร้าง และ update  cutmeterHistory table ให้ status == 'paid_and_installing'
        $cutmeter_history_sql = CutmeterHistory::where('user_id', $request->get('user_id'))
            ->where('pending', 1);
        $cutmeter_history = $cutmeter_history_sql->get()->first();
        //ถ้าสถานะ เป็นการถอดมิเตอร์ให้ update panding ==0
        if ($cutmeter_history->status == 'disambled') {
            $update_cutmeter_history = $cutmeter_history_sql->update([
                'pending' => 0,
                'updated_at' => date('Y-m-d H:i:s'),

            ]);
            $thai_year = date('Y') + 543;
            $cutmeter_history_create_piad_installing_row = new CutmeterHistory;
            $cutmeter_history_create_piad_installing_row->cutmeter_id = $cutmeter_history->cutmeter_id;
            $cutmeter_history_create_piad_installing_row->user_id = $cutmeter_history->user_id;
            $cutmeter_history_create_piad_installing_row->operate_date = date('m/d/' . $thai_year);
            $cutmeter_history_create_piad_installing_row->operate_time = date('H:i') . " น.";
            $cutmeter_history_create_piad_installing_row->twman_id = \json_encode([['user_id' => "" . Auth::id()]]);
            $cutmeter_history_create_piad_installing_row->status = 'paid_and_installing';
            $cutmeter_history_create_piad_installing_row->pending = 1;
            $cutmeter_history_create_piad_installing_row->created_at = date('Y-m-d H:i:s');
            $cutmeter_history_create_piad_installing_row->updated_at = date('Y-m-d H:i:s');
            $cutmeter_history_create_piad_installing_row->save();
        }

        return redirect('payment/receipt_print/' . $receipt->id);
    }

    public function receipt_print($reciept_id)
    {
        $apiPaymentCtrl = new ApiPaymentController();
        $invoicesPaidForPrint = $apiPaymentCtrl->history($reciept_id, 'receipt');
        $newId = FunctionsController::createInvoiceNumberString($reciept_id);
        $type = 'paid_receipt';
        $cashier = DB::table('user_profile')
            ->where('user_id', '=', Auth::id())
            ->select('name')
            ->get();
        $cashiername = $cashier[0]->name;
        return view('payment.receipt_print', compact('invoicesPaidForPrint', 'newId', 'type', 'cashiername'));
    }

    public function storeBackup(Request $request)
    {
        return $request;
        $paymentsFilter = collect($request->get('payments'))->filter(function ($v, $key) {
            return isset($v['on']);
        });
        date_default_timezone_set('Asia/Bangkok');

        $receipt = new Accounting();
        $receipt->total = $request->get('mustpaid');
        $receipt->cashier = Auth::id();
        $receipt->created_at = date('Y-m-d H:i:s');
        $receipt->updated_at = date('Y-m-d H:i:s');
        $receipt->save();
        //ทำการupdate invoice
        $apiInvCtrl = new ApiInvoiceController();
        foreach ($paymentsFilter as $key => $val) {
            $update = Invoice::where('id', $val['iv_id'])->update([
                'receipt_id' => $receipt->id,
                'status' => 'paid',
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $apiPaymentCtrl = new ApiPaymentController();
        $invoicesPaidForPrint = $apiPaymentCtrl->history($receipt->id, 'receipt');
        $newId = FunctionsController::createInvoiceNumberString($receipt->id);
        $type = 'paid_receipt';
        $cashier = DB::table('user_profile')
            ->where('user_id', '=', Auth::id())
            ->select('name')
            ->get();
        $cashiername = $cashier[0]->name;
        return view('payment.receipt_print', compact('invoicesPaidForPrint', 'newId', 'type', 'cashiername'));
    }

    public function print_payment_history($receipt_id)
    {
        // $invoicesPaidForPrint = collect([]);
        $apiPaymentCtrl = new ApiPaymentController();

        $invoicesPaidForPrint = $apiPaymentCtrl->history($receipt_id, 'receipt');

        $newId = FunctionsController::createInvoiceNumberString($receipt_id);
        $type = 'history_receipt';
        $cashiername = $invoicesPaidForPrint['reciepts'][0][0]->cashiername;
        return view('payment.receipt_print', compact('invoicesPaidForPrint', 'newId', 'type', 'cashiername'));
    }

    public function search()
    {
        $apiOwepaper = new OwepaperController();
        $zones = Zone::all();
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();

        return view('payment.search', compact('zones', 'invoice_period'));
    }

    public function remove($receiptId)
    {
        date_default_timezone_set('Asia/Bangkok');

        //เปลี่ยนสถานะของ invoice table
        // receipt_id เป็น 0,
        //ถ้า inv_period_id เท่ากับ invoice period table ที่ status  เท่ากับ active (ปัจจุบัน) ให้
        // - invoice.status เท่ากับ invoice นอกเหนือจากนั้นให้ status เป็น owe
        $currentInvoicePeriod = InvoicePeriod::where('status', 'active')->get('id')->first();
        $invoicesTemp = Invoice::where('receipt_id', $receiptId);
        $invoices = $invoicesTemp->get();
        foreach ($invoices as $invoice) {
            if ($invoice->inv_period_id == $currentInvoicePeriod->id) {
                $status = 'invoice';
            } else {
                $status = 'owe';
            }
            $invoicesTemp->update([
                'receipt_id' => 0,
                'status' => $status,
                'recorder_id' => Auth::id(),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        //เปลี่ยนสถานะของ accouting table เป็น  0
        Accounting::where('id', $receiptId)->update([
            'status' => 0,
            'cashier' => Auth::id(),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);

        return \redirect('payment/search');
    }

    public function paymenthistory($inv_period = '', $subzone_id = '')
    {
        $sql = DB::table('user_meter_infos as umf')
            ->join('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
        // ->join('invoice as iv', 'iv.meter_id', '=', 'umf.id')
            ->join('user_profile as upf', 'upf.user_id', '=', 'umf.user_id')
            ->join('zone as z', 'z.id', '=', 'umf.undertake_zone_id')
            ->join('subzone as sz', 'sz.id', '=', 'umf.undertake_subzone_id')
            ->where('iv.inv_period_id', '=', $inv_period)
            ->where('iv.status', '=', 'paid')
            ->where('umf.undertake_zone_id', '=', $subzone_id);

        $paid = $sql->get([
            'umf.user_id', 'umf.meternumber', 'umf.undertake_subzone_id', 'umf.undertake_zone_id',
            'upf.name', 'upf.address', 'iv.lastmeter', 'iv.currentmeter', 'iv.printed_time', 'iv.id',
            'upf.zone_id as user_zone_id', 'iv.comment',
            DB::raw('iv.currentmeter - iv.lastmeter as meter_net'),
            DB::raw('(iv.currentmeter - iv.lastmeter)*8 as total'),
        ]);
        $zoneInfoSql = $sql->get([
            'z.zone_name as undertake_zone', 'z.id as undertake_zone_id',
            'sz.subzone_name as undertake_subzone', 'sz.id as undertake_subzone_id',
        ]);
        $zoneInfo = collect($zoneInfoSql)->take(1);
        foreach ($paid as $iv) {
            $funcCtrl = new FunctionsController();
            $iv->user_id_string = $funcCtrl->createInvoiceNumberString($iv->user_id);
        }

        $presentInvoicePeriod = InvoicePeriod::where('id', $inv_period)->get('inv_period_name')[0];

        $memberHasInvoice = collect($paid)->sortBy('user_id');

        return view('payment.paymenthistory', compact('presentInvoicePeriod', 'zoneInfo', 'memberHasInvoice'));
    }

    public function receipted_list($user_id)
    {
        $apiPaymentCtrl = new ApiPaymentController;
        $receipted_list = $apiPaymentCtrl->history($user_id, 'receipt_history');
        return view('payment.receipted_list', compact('receipted_list'));
    }

}