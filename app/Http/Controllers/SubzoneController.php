<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Subzone;
use App\Zone;
use App\Tambon;
use App\District;
use App\Province;
use App\User;
use App\UsersInSubzone;
use App\UserProfile;

class SubzoneController extends Controller
{
    public function index(){
        $subzone = Subzone::with('zone','zone.tambon','zone.tambon.district','zone.tambon.district.province')
        ->orderBy('zone_id', 'asc')->get();
        return view('subzone.index', compact('subzone'));
    }

    public function create(){
        $provinces = Province::where('province_code', '35')->first();
        $adminInfo = User::where('id', 1)
            ->with('user_profile', 'user_profile.province', 'user_profile.district', 'user_profile.tambon', 'user_profile.tambon.zone')->first();
        return view('subzone.create', compact('provinces', 'adminInfo'));
    }

    public function store(Request $request){
        // 1.Filter collection array $request get subzone

        $collection = collect($request->get('subzone'));
        
        $filtered = $collection->filter(function ($value, $key) {
            return $value != null;
        });
        // 2.เอาค่าที่ filter ได้มาวนลูป แล้ว save ลงใน table subzone ผ่าน model subzone
        foreach($filtered as $subzone_name){
            
            $subzone = new Subzone();
            $subzone->zone_id = $request->get('zone');
            $subzone->subzone_name = $subzone_name;
            $subzone->save();
        }

        return redirect('/subzone');
    }

    public function edit($id){

        $zone = Zone::where('id', $id)->with(['subzone' => function($query){
            return $query->where('deleted', 0);
        }])->get();
        return view('subzone.edit', compact('zone'));
    }

    public function update(Request $request, $id){
        date_default_timezone_set('Asia/Bangkok');
        foreach($request->get('subzone') as $s_zone){
            $index = key($s_zone);

            if(collect( $s_zone[$index]['subzone_name'])->isNotEmpty() && $index == 'new'){
                $new_s_zone = new Subzone();
                $new_s_zone->zone_id        = $id;
                $new_s_zone->subzone_name   =  $s_zone[$index]['subzone_name'];
                $new_s_zone->created_at     = date('Y-m-d H:i:s');
                $new_s_zone->updated_at     = date('Y-m-d H:i:s');
                $new_s_zone->save();
            }else{
                Subzone::where('id', $index)->update([
                    'subzone_name' => $s_zone[$index]['subzone_name']
                ]);
            }
                
        }
        return redirect()->action(
            'ZoneController@index'
        )->with(['success'  => 'ทำการบันทึกการแก้ไขข้อมูลเรียบร้อยแล้ว', 'color' => 'success']);
    }

    public function delete($id){
        $subzone = Subzone::where('id', $id)->update([
            'status' => 0,
            'deleted' => 1
        ]);
       
        return redirect('/subzone')->with(['success' => 'ทำการลบข้อมูลเรียบร้อยแล้ว']);
    }

    public function getSubzone($zone_id){
        $subzone = Subzone::where("zone_id",$zone_id)->get();
        // $subzone = Subzone::all();
        return response()->json($subzone);
    }

    public function add_staff_in_subzone(){
        // return$users = DB::table('users')
        //     ->join('user_profile', 'users.id', '=', 'user_profile.user_id')
        //     ->join('zone', 'user_profile.zone', '=', 'zone.id')
        //     ->join('subzone', 'user_profile.subzone', '=', 'subzone.id')
        //     ->select('user_profile.*', 'zone.zone_name',
        //              'zone.id as zone_id',
        //              'subzone.id as subzone_id',
        //              'subzone.subzone_name'
        //             )
        //     ->get();

        $users = UserProfile::with('zone', 'zone.subzone')->get();
        //grouping  user เข้าเป็นแต่ละโซน
        $zoneGrouped = collect($users)->groupBy('zone')->all();
        foreach($zoneGrouped as $key => $grouped){
           $zoneGrouped[$key] = (collect($grouped)->groupBy('subzone')->all());
        }
        
        foreach($zoneGrouped as $key => $collected){
            return $collected;
            $collected[$key]['subzone_members'] = collect($collected)->count();
        }
        return $zoneGrouped;
        return view('staff_in_subzone.index', \compact('zoneGrouped'));
    }

    public function users_in_subzone_create($subzone_id){
       return $users = User::with('user_profile')
                    ->where('user_cat_id', 4)
                    // ->
                    ->get();
        return view('subzone.users_in_subzone/create.', \compact('users'));
        // $subzone = Subzone::where    
        // return $subzone_id;
    }
}
