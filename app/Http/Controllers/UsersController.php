<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Province;
use App\District;
use App\Tambon;
use App\Zone;
use App\Subzone;
use App\UserProfile;
use App\UsersInSubzone;
use App\Usercategory;
use App\UserMeterInfos;
use App\TabwaterMeter;
use App\Invoice;

use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\TabwaterMeterController;

class UsersController extends Controller
{
    public function index(){
        //checkว่ามีการตั้งค่า ค่า zone, tabwatermeterหรือยัง
        $checkZone = Zone::all();
        $apiTabwatermeterCtrl = new TabwaterMeterController();
        $checkTabwatermeter = $apiTabwatermeterCtrl->checkTabwatermeters();
        if(collect($checkZone)->isEmpty() || $checkTabwatermeter == 0){
            return view('users.index', compact('checkZone', 'checkTabwatermeter'));
        }
        $activeMeter = UserMeterInfos::where('status', 'active')->get('user_id');
        $inactiveMeterQuery = UserMeterInfos::whereIn('status', ['inactive', 'deleted'])
                ->where('deleted', 0)
                ->with('user_profile', 'user_profile.zone')    
                ->get();
        $inactiveMeter = collect($inactiveMeterQuery)->filter(function($item){
            return collect($item->user_profile)->isNotEmpty();
        });
        $changeMeters = UserMeterInfos::where('status', 'changemeter')
            ->with('user_profile')    
            ->get();
        
        $changeMeter = collect($changeMeters)->filter(function($val){
            //หาว่ามีการลงทะเบียนเปลี่ยนมิเตอร์ใหม่หรือยัง
            $findNewActiveRow = UserMeterInfos::where('user_id', $val->user_id)
                        ->where('status', 'active')->get();
            return collect($findNewActiveRow)->isEmpty();
        });
        
        return view('users.index', compact('activeMeter', 'inactiveMeter', 'changeMeter', 'checkZone', 'checkTabwatermeter'));
    }

    public function create($renew = 0 , $user_id = 0){
        $user = new User();
        $provinces = Province::all();
        $usercategories = Usercategory::all();
        $last_user_id = UserProfile::get('id')->last();//UserMeterInfos::get()->last();

        $funcCtrl = new FunctionsController();
        $nextId = collect($last_user_id)->isEmpty() ? 1 : $last_user_id->id+1;
        $nextMeter =  $funcCtrl->createInvoiceNumberString($nextId);
        $zones = Zone::all();

        if($renew == 1){
            $user = UserMeterInfos::where('id', $user_id)
                ->with('user_profile')
                ->get()->first(); 
        }
        $tabwatermeters = TabwaterMeter::all(['id', 'typemetername']);
        return view('users.create', compact('user','provinces', 'usercategories', 'zones', 'tabwatermeters', 
                    'nextMeter', 'nextId', 'renew'));
    }

    public function store(REQUEST $request){
        date_default_timezone_set('Asia/Bangkok');
        $user_id = "";
        //ผู้ขอใช้น้ำรายใหม่ 
        $user = new User;
        $newUserId = $request->get('nextId');//หา rowสุดท้ายของ user table
        // $user->id = $newUserId;
        $user->username = 'user'.$newUserId;
        $user->password = Hash::make("1234");
        $user->email = 'user'.$newUserId.'@gmail.com';
        $user->user_cat_id = 3;
        $user->created_at = date('Y-m-d H:i:s');
        $user->updated_at = date('Y-m-d H:i:s');
        $user->save();

        $user_profile = new UserProfile;
        $user_profile->user_id = $user->id;
        $user_profile->name = $request->get('name');
        $user_profile->gender = $request->get('gender');
        $user_profile->id_card = $request->get('id_card');
        $user_profile->phone = $request->get('phone');
        $user_profile->address = $request->get('address');
        $user_profile->province_code = $request->get('province_code');
        $user_profile->district_code = $request->get('district_code');
        $user_profile->tambon_code = $request->get('tambon_code');
        $user_profile->zone_id = $request->get('zone_id');
        $user_profile->subzone_id = $request->get('zone_id');
        $user_profile->created_at = date('Y-m-d H:i:s');
        $user_profile->updated_at = date('Y-m-d H:i:s');
        $user_profile->save();

        $usermeterInfo = new UserMeterInfos;
        $usermeterInfo->user_id                 = $user->id;
        $usermeterInfo->meternumber             = $request->get('meternumber');
        $usermeterInfo->metertype               = $request->get('metertype');
        $usermeterInfo->counter_unit            = $request->get('counter_unit');
        $usermeterInfo->metersize               = $request->get('metersize');
        $usermeterInfo->undertake_zone_id       = $request->get('undertake_zone_id');
        $usermeterInfo->undertake_subzone_id    = $request->get('undertake_subzone_id');
        
        $date = explode('/', $request->get('acceptance_date'));
        $usermeterInfo->acceptace_date          = date('Y').'-'.$date[1].'-'.$date[0];
        $usermeterInfo->payment_id              = $request->get('payment_id');
        $usermeterInfo->discounttype            = $request->get('discounttype');
        $usermeterInfo->recorder_id             = Auth::id();
        $usermeterInfo->created_at              = date('Y-m-d H:i:s');
        $usermeterInfo->updated_at              = date('Y-m-d H:i:s');
        $usermeterInfo->save();

        // เพิ่ม user_role  = 3
         $user_role = User::find($user->id);
         $user_role->attachRole('user');

        return redirect('users')->with(['success' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    public function edit($user_cat_id, $id, $changemeter = 'nochange', $meter_id = 0){
        $funcCtrl = new FunctionsController();
        $user = UserMeterInfos::where('id', $id)
                ->with('user_profile')
                ->get()->first();
        $user->acceptace_date =  $funcCtrl->engDateToThaiDateFormat($user->acceptace_date);
        $provinces = Province::all();
        $usercategories = Usercategory::all();
        $zones = Zone::all();
        $tabwatermeters = TabwaterMeter::all(['id', 'typemetername']);
        $user_subzone = Subzone::where('zone_id', $user->undertake_zone_id)->get();
        return view('users.edit', compact('user', 'zones', 'provinces', 'usercategories', 'tabwatermeters',
                 'user_subzone', 'changemeter'));
    }

    public function update(REQUEST $request, $id){
        date_default_timezone_set('Asia/Bangkok');
        $funcCtrl = new FunctionsController();
        if($request->get('status') == 'inactive' || $request->get('status') == 'changemeter'){
            //ยกเลิกการใช้มิเตอร์  หรือ เปลี่ยนมิเตอร์
            $updateInactive = UserMeterInfos::where('id', $id)
                        ->update([
                            'status' => 'deleted',
                            'comment' => $request->get('status') == 'changemeter' ? 'เปลี่ยนมิเตอร์'  :'ยกเลิกการใช้งาน',
                            'recorder_id'           => Auth::id(),
                            'updated_at'            => date('Y-m-d H:i:s')
                        ]);
            $inactiveUserProfile = UserProfile::where('user_id', $request->get('user_id'))
                       ->update([
                            'status'        => 0,
                            'deleted'       => 1,
                            'updated_at'    => date('Y-m-d H:i:s')
                        ]); 
            $inactiveUser = User::where('id', $request->get('user_id'))
                        ->update([
                             'status' => 'deleted',
                             'updated_at'            => date('Y-m-d H:i:s')
                         ]);  
           $inactiveInvoice = Invoice::where('meter_id', $id)
                            ->update([
                                'deleted' => 1,
                                'updated_at'    => date('Y-m-d H:i:s')
                            ]);
           
        }else{// แก้ไขข้อมูล UserProfile และ UserMeterInfos (active)
            $user = UserProfile::where('user_id', $request->get('user_id'))->update([
                'name' => $request->get('name'),
                'id_card' => $request->get('id_card'),
                'phone' => $request->get('phone'),
                'gender' =>$request->get('gender'),
                'address' => $request->get('address'),
                'zone_id' => $request->get('zone_id'),
                'subzone_id' => $request->get('zone_id'),
                'tambon_code' => $request->get('tambon_code'),
                'district_code' => $request->get('district_code'),
                'province_code' => $request->get('province_code'),
            ]);

            $userMeterInfo = UserMeterInfos::where('user_id', $request->get('user_id'))->update([
                'meternumber'          => $request->get('meternumber'),
                'metertype'            => $request->get('metertype'),
                'counter_unit'         => $request->get('counter_unit'),
                'metersize'            => $request->get('metersize'),
                'undertake_zone_id'    => $request->get('undertake_zone_id'),
                'undertake_subzone_id' => $request->get('undertake_subzone_id'),
                'status'               => $request->get('status'),
                'recorder_id'           => Auth::id(),
                'acceptace_date'       => $funcCtrl->thaiDateToEngDateFormat($request->get('acceptance_date')),
                'updated_at'           => date('Y-m-d H:i:s'),
            ]); 
        }
        

        return redirect('users');

    }

    public function show($user_id){
        $apiInvoiceCtrl = new InvoiceController(); 
        $user = UserMeterInfos::where('user_id', $user_id)
                            ->with('user_profile', 'invoice_by_user_id', 'invoice.invoice_period')
                            ->get();
        $fn = new FunctionsController;
        foreach($user[0]->invoice as $u){
            $date = explode(" ",$u->updated_at);
            $u->updated_at_th = $fn->engDateToThaiDateFormat($date[0]);

        }
        return view('users.show', compact('user'));
    }

    public function delete($id){
        $userSqlTemp = User::where('id', $id);
        $user = $userSqlTemp->update([
            'status' => 'deleted'
        ]);
        $userPRofileDelete = UserProfile::where('user_id', $id)->update([
            'status' => 0
        ]);
        $userMeterInfosDeleteTemp = UserMeterInfos::where('user_id', $id);
        
        return$meterId = $userMeterInfosDeleteTemp->get();

        $userMeterInfosDeleteTemp->update([
                'status'=> 'inactive'
            ]);
        $oweInv = Invoice::where('meter_id', $meterId->id)
                ->where('status', 'invoice')
                ->update([
                    'status' => 'owe'
                ]);
        return redirect('users');
    }

    public function userInfos($user_id){
        $sql = DB::table('user_meter_infos as umf')
            ->join('user_profile as uf', 'uf.user_id', '=', 'umf.user_id')
            ->join('zone', 'zone.id', '=', 'umf.undertake_zone_id')
            ->join('subzone', 'subzone.id', '=', 'umf.undertake_subzone_id')        
            ->where('umf.status', '=', 'active')
            ->select(   'umf.meternumber', 
                        'umf.user_id',
                        'uf.name','uf.address',
                        'zone.zone_name',
                        'subzone.subzone_name'
                    );
        return $sql;
    }
}
