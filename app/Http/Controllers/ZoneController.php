<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Zone;
use App\Subzone;
use App\Http\Controllers\SettingsController;

class ZoneController extends Controller
{
    public function index(REQUEST $request){
        $zone = Zone::with(['subzone' => function($query){
            $query->where('deleted', 0);
        }])->get();
        
        return view('zone.index', compact('zone'));
    } 

    public function create(){
        $zone = new Zone;
        $settingsCtrl = new SettingsController();
        $tambonInfos = $settingsCtrl->getTambonInfos();
        return view('zone.create', compact('zone', 'tambonInfos'));
    }

    public function store(REQUEST $request){
        date_default_timezone_set('Asia/Bangkok');

        foreach(collect($request->get('zone'))->sortKeys() as $zone){

            $storeZone = new Zone;
            $storeZone->zone_name = $zone['zonename'];
            $storeZone->location = $zone['zoneAddress'];
            $storeZone->created_at = date('Y-m-d H:i:s');
            $storeZone->updated_at = date('Y-m-d H:i:s');
            $storeZone->save();

            //สร้าง default subzone
            $subzone  = new Subzone();
            $subzone->zone_id       = $storeZone->id;
            $subzone->subzone_name  = "เส้น".$zone['zonename'];
            $subzone->created_at = date('Y-m-d H:i:s');
            $subzone->updated_at = date('Y-m-d H:i:s');
            $subzone->save();
        }
        
        return redirect('zone')->with(['massage' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    public function update($id){
        $zone = Zone::find($id);
        return view('zone.update', compact('zone'));
    }

    public function edit(REQUEST $request , $id){
        date_default_timezone_set('Asia/Bangkok');

        $zone = Zone::find($id);
        $zone->zone_name = $request->get('zone_name');
        $zone->location = $request->get('location');
        $zone->updated_at = date('Y-m-d H:i:s');
        $zone->save();
        return redirect('zone')->with(['massage' => 'ทำการบันทึกข้อมูลเรียบร้อยแล้ว']);
    }

    public function delete($id){
        $subzone = Subzone::where('zone_id', $id)->delete();
        $zone = Zone::find($id);
        $zone->delete();
        return redirect('zone')->with(['success' => 'ทำการลบข้อมูลเรียบร้อยแล้ว', 'color' => 'danger']);

    }

    public function getZone($tambon_code){
        $zone = Zone::where("tambon_code",$tambon_code)->get();
        return response()->json($zone);
    }
}
