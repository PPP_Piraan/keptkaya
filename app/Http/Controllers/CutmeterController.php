<?php

namespace App\Http\Controllers;

use App\CutmeterHistory;
use App\InvoicePeriod;
use App\Subzone;
use App\User;
use App\UserMeterInfos;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class CutmeterController extends Controller
{
    public function index($subzone_id = "")
    {
        $zones = Zone::all();
        $subzone_selected = 'all';
        $zone_selected = 'all';
        $invoice_period = InvoicePeriod::where('status', 'active')->get()->first();
        if ($subzone_id > 0) {
            $subzone = Subzone::where('id', $subzone_id)->get(['id', 'zone_id'])->first();
            $subzone_selected = $subzone->id;
            $zone_selected = $subzone->zone_id;
        }
        return view('cutmeter.index', compact('zones', 'invoice_period', 'zone_selected', 'subzone_selected'));
    }

    public function edit($user_id)
    {
        $tambon_infos_db = DB::table('settings')
            ->where('name', 'organization')
            ->select('values')
            ->get();
        $tambon_infos = collect(json_decode($tambon_infos_db[0]->values, true))->toArray();
        $user = UserMeterInfos::where('user_id', $user_id)
            ->where('deleted', 0)
            ->with([
                'user_profile' => function ($query) {
                    $query->select('name', 'address', 'phone', 'user_id', 'zone_id');
                },
                'zone:zone_name,id',
                'subzone:subzone_name,id',
            ])
            ->get(['user_id', 'meternumber', 'undertake_zone_id', 'undertake_subzone_id']);
        $cutmeter_user_eq = CutmeterHistory::where('user_id', $user_id)->where('pending', 1)->get(['status']);
        $cutmeter_user_status = collect($cutmeter_user_eq)->count() == 0 ? '' : $cutmeter_user_eq[0]->status;

        $cutmeter_status = [['id' => 'disambled', 'value' => 'ล็อคมิเตอร์'],
            // ['id' => 'paid_and_installing', 'value' => 'ชำระเงินแล้ว รอการตั้งมิเตอร์'],
            ['id' => 'complete', 'value' => 'ติดตั้งมิเตอร์สำเร็จ'],
            ['id' => 'cancel', 'value' => 'ยกเลิก']];
        $tabwatermans = User::where('user_cat_id', 4)
            ->with(['user_profile:name,user_id'])
            ->where('status', 'active')
            ->get(['id']);

        //ถ้า status == disambled สเตป ต่อไป จะ เปลี่ยน status เป็น  complete
        //ให้ check owe_count ใน user_data_infos table ว่า <2 ไหม
        //ถ้าไม่ให้ $show_submit_btn = 'hidden' และ $owe_count_text = 'ยังมีไม่การชำระเงินค่าน้ำประปาที่ค้าง'
        $show_submit_btn = '';
        $owe_count_text = '';

        if ($cutmeter_user_status == 'disambled') {
            $check_owe_count = UserMeterInfos::where('user_id', $user_id)->where('status', 'cutmeter')->get(['owe_count']);
            if ($check_owe_count[0]->owe_count >= 3) {
                $show_submit_btn = 'hidden';
                $owe_count_text = 'ยังมีไม่การชำระเงินค่าน้ำประปาที่ค้าง';
            }
        }

        return view('cutmeter.edit', compact('user', 'cutmeter_status', 'tabwatermans', 'tambon_infos', 'cutmeter_user_status', 'show_submit_btn', 'owe_count_text'));
    }

    public function update(REQUEST $request, $user_id)
    {
        // return $request;
        //ถ้าเป็นการยกหม้อครั้งแรกให้  create
        $cutmeter_id = 1;
        $pending = 1; //ตัววิ่งว่าถึงสถานะไหนแล้ว โดยยึดตาม cutmeter_id
        if ($request->get('status') == 'disambled') {
            $invoice_period = InvoicePeriod::where('status', 'active')->get(['id'])->first();
            $cutmeter_id = $invoice_period->id;
        } else {
            //หา row ของ user_id ที่  pending == 1
            $active_pending_sql = CutmeterHistory::where('user_id', $user_id)->where('pending', 1);
            $active_pending = $active_pending_sql->get();
            $cutmeter_id = $active_pending[0]->cutmeter_id;
            //update pending == 0
            $active_pending_sql->update([
                'pending' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        if ($request->get('status') == 'cancel') {
            //ถ้าทำการยกเลิก
            //ให้ทำการ update pending == 2 และ deleted = 1 ของ user ที่มี cutmeter_id ปัจจุบัน
            $update_when_renew_use_meter = CutmeterHistory::where('cutmeter_id', $cutmeter_id)
                ->where('user_id', $user_id)->update([
                'pending' => 2,
                'deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            $create_cutmeter = new CutmeterHistory;
            $create_cutmeter->cutmeter_id = $cutmeter_id;
            $create_cutmeter->user_id = $user_id;
            $create_cutmeter->operate_date = $request->get('operate_date');
            $create_cutmeter->operate_time = $request->get('operate_time');
            $create_cutmeter->twman_id = \json_encode($request->get('tabwaterman'));
            $create_cutmeter->status = $request->get('status');
            $create_cutmeter->comment = $request->get('comment');
            $create_cutmeter->pending = $pending;
            $create_cutmeter->created_at = date('Y-m-d H:i:s');
            $create_cutmeter->updated_at = date('Y-m-d H:i:s');
            $create_cutmeter->save();

            if ($request->get('status') == 'complete') {
                //ทำการติดตั้งมิเตอร์แล้ว หลังจากที่ user มาจ่ายเงิน
                //ให้ทำการ update pending == 2 ของ user ที่มี cutmeter_id ปัจจุบัน
                $update_when_renew_use_meter = CutmeterHistory::where('cutmeter_id', $cutmeter_id)
                    ->where('user_id', $user_id)->update([
                    'pending' => 2,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                //update  user_meter_infos
                //ใหั status == active
                $update_usermeter_infos = UserMeterInfos::where('user_id', $user_id)->where('deleted', 0)
                    ->where('status', 'cutmeter')->update([
                    'status' => 'active',
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
        }
        return redirect('cutmeter/index');

    }
}