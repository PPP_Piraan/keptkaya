<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\TabwaterManPerArea;
use App\Zone;
class TawatermanPerAreasController extends Controller
{
    public function store(REQUEST $request){
        date_default_timezone_set('Asia/Bangkok');

        $tabwaterman = $request->tabwaterman;
        $twmPerAreas = new TabwaterManPerArea;
        $twmPerAreas->tabwaterman_id = $tabwaterman['value'];
        $twmPerAreas->areas =json_encode($request->selectedzone);
        $twmPerAreas->created_at = date("Y-m-d H:i:s");
        $twmPerAreas->updated_at = date("Y-m-d H:i:s");
        $twmPerAreas->save();
        //เปลี่ยนstatus ใน zone table เป็นมีผู้รับผิดชอบแล้ว 
        foreach($request->selectedzone as $zone){
            $responsed_zone = Zone::where('id' , $zone['value'])->first();
            $responsed_zone->status = "1";
            $responsed_zone->save();
        }
        return response()->json("success");
    }
}
