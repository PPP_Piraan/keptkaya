<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TabwaterMeter; 
use App\UserMeterInfos; 

class TabwaterMeterController extends Controller
{
    public function index(){
        $tabwatermeter = TabwaterMeter::all();
        return \response()->json($tabwatermeter);
    }

    public function store(REQUEST $request){
        $tabwaterMeter = new TabwaterMeter();
        $tabwaterMeter->typemetername = $request->get('tabwatermetertype');
        $tabwaterMeter->price_per_unit = $request->get('price_per_unit');
        $tabwaterMeter->metersize = $request->get('metersize'); 
        $tabwaterMeter->save();
        return \response()->json("success");
    }

    public function edit($id){
        $tabwaterMeter = TabwaterMeter::find($id);
        
        return \response()->json($tabwaterMeter);
    }

    public function update(Request $request ,$id){
        $tabwaterMeter = TabwaterMeter::find($id);
        $tabwaterMeter->typemetername = $request->get('tabwatermetertype');
        $tabwaterMeter->price_per_unit = $request->get('price_per_unit');
        $tabwaterMeter->metersize = $request->get('metersize'); 
        $tabwaterMeter->update();
        return \response()->json("อัพเดทข้อมูลเรียบร้อย");
    }

    public function delete($id){
        $tabwaterMeter = TabwaterMeter::find($id);
        $tabwaterMeter->delete();
        return \response()->json("ลบข้อมูลเรียบร้อย");
    }

    public function checkTabwatermeters(){
        //หาว่ามีการตั้งค่ามิเตอร์หรือยัง
        $tabwaterMeter = TabwaterMeter::all();
        return collect($tabwaterMeter)->count();
    }

    public function checkTabwatermeterMatchedUserMeterInfos($tabwaterMeterId){
        $matched = UserMeterInfos::where('metertype', $tabwaterMeterId)
                ->where('status', 'active')
                ->first();
        return collect($matched)->count();
    }
}
