<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\InvoicePeriod;
use App\UserProfile;
class AdminController extends Controller
{
    public function invoice()
    {
        //Get current invoice period
        $currInvPeriod = InvoicePeriod::where('status', 'active')->first();
        $currInvoice = Invoice::
                    where('invoice.inv_period_id', $currInvPeriod->id)
                    ->get();
        foreach($currInvoice as $inv){    
            $profile =  UserProfile::where('user_id', $inv->user_id)->first();   
            $inv['net_meter'] = $this->calculateNetMeter($inv);
            $inv['paid_amount'] =  $this->calculatePaidAmount($inv);
            $inv['owe'] = $this->findOwe($inv);
            $inv['profile'] = $profile;
        }
        return \response()->json($currInvoice);
    }

    public function invoice_by_user_id($id){
        $currInvoice = Invoice::where('invoice.id', $id)->with('invoice_period')->first();
        $profile =  UserProfile::where('user_id', $currInvoice->user_id)->first();   
        $currInvoice['net_meter'] = $this->calculateNetMeter($currInvoice);
        $currInvoice['paid_amount'] =  $this->calculatePaidAmount($currInvoice);
        $currInvoice['owe'] = $this->findOwe($currInvoice);
        $currInvoice['profile'] = $profile;
            
        return \response()->json($currInvoice);
    }

    private function calculateNetMeter($res){
        $net = $res->currentmeter - $res->lastmeter;
        return $net;
    } 

    private function calculatePaidAmount($res){
        $pricePerUnit = 6;
        $paidAmount = $this->calculateNetMeter($res) * $pricePerUnit;
        return $paidAmount;
    }

    private function findOwe($res){
        $owe = Invoice::where('user_id', $res->user_id)
                        ->where('status', 'owe')
                        ->get();
        $count = $owe->count();
        $resOwe = $count == 0 ? ['count' => 0] :  ['owe'=> $owe, 'count' => $count];
        return $resOwe;
    }
}
