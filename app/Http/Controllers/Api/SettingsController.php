<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Settings;

class SettingsController extends Controller
{
    public function get_values($values_name)
    {
        $setting = Settings::where('name', $values_name)->get();
        return response()->json($setting);
    }
}