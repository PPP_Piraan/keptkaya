<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Zone;
use App\Subzone;
use App\User;
use App\UserProfile;
class ZoneController extends Controller
{
    public function index(){
        $zones = Zone::where('status', "0")->get();
        $zoneArr = [];
        if(count($zones) > 0){
            foreach($zones as $zone){
                $z = ["value" => $zone->id, "name" => $zone->zone_name];
                array_push($zoneArr, $z);
            }
        }
        return response()->json($zoneArr);
    }

    public function getZoneAndSubzone(){
        $zones = Zone::with('subzone')->get();
        return response()->json($zones);
    }

    public function undertakenZoneAndSubzone($twman_id){
        $twMan = User::where('id', $twman_id)
            ->with('undertaker_subzone', 'undertaker_subzone.subzone', 'undertaker_subzone.subzone.zone')
            ->get();
        $twMan[0]->undertaker_subzone[0]->subzone[0]->subzone_name = json_decode($twMan[0]->undertaker_subzone[0]->subzone[0]->subzone_name);
        $twMan[0]->undertaker_subzone[1]->subzone[0]->subzone_name= json_decode($twMan[0]->undertaker_subzone[1]->subzone[0]->subzone_name );
        return response()->json($twMan);
    }

    public function users_by_zone($zone_id){
        $users = UserProfile::where('zone_id', $zone_id)->get(['name', 'user_id', 'phone']);
        return response()->json($users);

    }
    public function delete($id){
        $subzone = Subzone::where('zone_id', $id)->delete();

        $zone = Zone::where('id', $id)->delete();

        return 1;

    }
}
