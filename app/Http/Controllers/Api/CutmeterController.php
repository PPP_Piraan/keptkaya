<?php

namespace App\Http\Controllers\Api;

use App\CutmeterHistory;
use App\Http\Controllers\Api\FunctionsController;
use App\Http\Controllers\Api\ReportsController as apiReportCtrl;
use App\Http\Controllers\Controller;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CutmeterController extends Controller
{
    public function index(REQUEST $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $fnCtrl = new FunctionsController();
        $apiReportCtrl = new apiReportCtrl();
        if (collect($request)->isEmpty()) {
            $a = [
                'invperiodstart' => 'all',
                'invperiodend' => 'all',
                'zone_id' => 'all',
                'subzone_id' => 'all',
                'type' => 'payment',
            ];
            $request->merge($a);
        }
        //หา user  ที่ invoice.status  เป็น cutmeter
        $oweInfosArr = DB::table('user_meter_infos as umf')
            ->join('invoice as iv', 'iv.user_id', '=', 'umf.user_id')
            ->join('user_profile as upf', 'upf.user_id', '=', 'umf.user_id')
            ->join('zone as z', 'z.id', '=', 'upf.zone_id')
            ->join('subzone as udt_sz', 'udt_sz.id', '=', 'umf.undertake_subzone_id')
            ->join('zone as udt_z', 'udt_z.id', '=', 'umf.undertake_zone_id')
            ->where('umf.status', '=', 'cutmeter')
            ->where('iv.deleted', '=', 0);

        // $oweInfosArr = $oweInfosArr->where('umf.status', ['owe']);

        if ($request->get('zone_id') != 'all') {
            if ($request->get('subzone_id') != 'all') {
                $oweInfosArr = $oweInfosArr->where('umf.undertake_subzone_id', '=', $request->get('subzone_id'));
            } else if ($request->get('subzone_id') == 'all') {
                $oweInfosArr = $oweInfosArr->where('umf.undertake_zone_id', '=', $request->get('zone_id'));
            }
        }

        $oweInfosArr = $oweInfosArr->select(
            'upf.name', 'upf.address',
            'z.zone_name',
            'udt_sz.subzone_name',
            'umf.meternumber', 'umf.user_id', 'umf.status as umf_status', 'umf.owe_count',
            'iv.status',
        )
            ->groupBy('umf.user_id')
            ->get();

        foreach ($oweInfosArr as $arr) {
            $cth = CutmeterHistory::where('user_id', $arr->user_id)->where('pending', 1)->get('status');
            $arr->cutmeter_status = isset($cth[0]->status) ? $this->cutmeter_status_Th($cth[0]->status) : '<button class="btn btn-block btn-sm btn-outline-warning disabled">รอดำเนินการถอดมิเตอร์</button>';
        }
        return $oweInfosArr;

    }

    public function get_cutmeter_history($user_id)
    {
        $historys = CutmeterHistory::where('user_id', $user_id)
            ->where('pending', '<>', 2)
            ->where('deleted', 0)
            ->get();
        $res = collect([]);
        foreach ($historys as $history) {
            $history_twmans = json_decode($history->twman_id, true);
            $twmans = collect([]);
            foreach ($history_twmans as $key => $twman) {
                if ($twman["user_id"] != null) {
                    $rers = UserProfile::where('user_id', $twman['user_id'])
                        ->get(['name'])->first();
                    $twmans->push($rers);
                }
            }
            $res->push([
                'twmans' => $twmans,
                'operate_date' => $history->operate_date,
                'operate_time' => $history->operate_time,
                'status' => $this->cutmeter_status_Th($history->status),
            ]);
        }
        return $res;

    }

    private function cutmeter_status_Th($status)
    {
        $str = '';
        if ($status == 'disambled') {
            $str = '<button class="btn btn-block btn-outline-danger btn-sm disabled">ถอดมิเตอร์แล้ว</button>';
        } else if ($status == 'paid_and_installing') {
            $str = '<button class="btn btn-block btn-outline-info btn-sm disabled">ชำระเงินแล้ว รอติดตั้งมิเตอร์</button>';
        } else if ($status == 'complete') {
            $str = '<button class="btn btn-block btn-outline-success btn-sm disabled">ติดตั้งมิเตอร์สำเร็จ</button>';
        } else if ($status == 'cancel') {
            $str = '<button class="btn btn-block btn-outline-secondary btn-sm disabled">ยกเลิก</button>';
        }
        return $str;
    }
}