<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabWaterUser extends Model
{
    protected $table = 'tab_water_user';
}
