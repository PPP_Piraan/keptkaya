<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subzone extends Model
{
    protected $table = 'subzone';

    public function zone()
    {
        return $this->belongsTo('App\Zone', 'zone_id', 'id');
    }

    public function users_in_subzone(){
        return $this->hasMany('App\UserMeterInfos','undertake_subzone_id', 'id');
    }

    public function undertaker_subzone(){
        return $this->hasOne('App\UndertakerSubzone', 'subzone_id', 'id');
    }

}
