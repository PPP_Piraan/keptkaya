<?php

namespace App\Models\items;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{

    protected $table = 'items';

    public function item_categories()
    {
        return $this->belongsTo('App\Models\ItemCategories', 'items_cate_id');
    }

    public function itemgroup()
    {
        return $this->belongsTo('App\Models\ItemsGroup', 'items_group_id', 'id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'count_unit_id', 'id');
    }

}