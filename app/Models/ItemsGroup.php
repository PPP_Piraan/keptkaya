<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemsGroup extends Model
{
    protected $table = 'items_group';

    public function items(){
            return $this->hasMany('App\Models\Items','id','items_group_id');
    }
}
