<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCategories extends Model
{
    protected $table = 'items_categories';

    public function items(){
        return $this->hasMany('item_cate_id', 'id');
    }
}
