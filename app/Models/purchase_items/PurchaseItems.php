<?php

namespace App\Models\purchase_items;

use Illuminate\Database\Eloquent\Model;

class PurchaseItems extends Model
{
  protected $table = "purchase_items";

  public function seller_profiles(){
    return $this->belongsTo('App\Models\Profiles','seller_id','user_id');
  }

  public function buyer_profiles(){
    return $this->belongsTo('App\Models\Profiles','buyer_id','user_id');
  }

  public function purchaseTransaction(){
    return $this->hasMany('App\Models\purchase_transaction\PurchaseTransaction', 'invoice_id', 'invoice');
  }

   public function items(){
        return $this->belongsTo('App\Models\items\Items', 'item_id', 'id');
      }

}

