<?php

namespace App\Models\purchase_transaction;

use Illuminate\Database\Eloquent\Model;

class PurchaseTransaction extends Model
{
  protected $table = "purchase_items_transaction";

  public function items(){
    return $this->belongsTo('App\Models\items\Items', 'item_id', 'id');
  }
}

