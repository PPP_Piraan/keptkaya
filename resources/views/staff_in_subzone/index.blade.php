@extends('layouts.admin')

@section('title')
ตั้งค่า / เส้นทางจดมิเตอร์น้ำประปา/เพิ่มผู้ใช้งานกับพื้นที่
@endsection

@section('content')
  @foreach ($zoneGrouped as $key => $zone)
        {{-- {{dd($zone)}} --}}
        @foreach ($zone as $subzone)
            {{collect($subzone)->count()}}
        @endforeach
  @endforeach
    {{dd($zoneGrouped)}}
<div class="row">
  @foreach ($zoneGrouped as $key => $zone)

    <div class="col-lg-4">
        <div class="card card-small user-teams mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">พื้นที่: {{$zone[0][0]->zone_name}}</h6>
              <div class="block-handle"></div>
            </div>
            <div class="card-body p-0">
              <div class="container-fluid">
                  @foreach ($zone as $subzone)
                {{-- {{dd($item->id)}} --}}
                        <div class="row px-3">
                            <div class="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                                <img class="rounded" src="{{asset('shards/images/avatars/team-thumb-1.png')}}">
                            </div>
                            <div class="col-8 user-teams__info pl-3">
                                {{-- <div class="form-group"> --}}
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">เส้นทาง</span>
                                      </div>
                                      <input type="text" value="{{$item->subzone_name}}" class="form-control is-valid" readonly>
                                    </div>
                            
                                <span class="text-light mt-2">{{$item->users_in_subzone_count}} Members</span>
                            </div>
                            <div class="col user-teams__info pl-4">
                                <a href="{{url('subzone/users_in_subzone_create/'.$item->id)}}"  style="font-size: 2rem" >
                                    <i class="fa fa-plus-circle text-success"></i>
                                </a>
                            </div>
                        </div>
                   
                @endforeach

              </div>
            </div>
          </div>
    </div>

    @endforeach
    {{-- <div class="col-lg-4">
        <div class="card card-small user-teams mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">พื้นที่: หมู่  </h6>
              <div class="block-handle"></div>
            </div>
            <div class="card-body p-0">
              <div class="container-fluid">
                <div class="row px-3">
                  <div class="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                    <img class="rounded" src="{{asset('shards/images/avatars/team-thumb-1.png')}}">
                  </div>
                  <div class="col user-teams__info pl-3">
                    <h6 class="m-0">ซอยวัดโพธฺ์</h6>
                    <span class="text-light">21 Members</span>
                  </div>
                </div>
                <div class="row px-3">
                  <div class="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                    <img class="rounded" src="{{asset('shards/images/avatars/team-thumb-2.png')}}">
                  </div>
                  <div class="col user-teams__info pl-3">
                    <h6 class="m-0">Team Shelby</h6>
                    <span class="text-light">21 Members</span>
                  </div>
                </div>
                <div class="row px-3">
                  <div class="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                    <img class="rounded" src="{{asset('shards/images/avatars/team-thumb-3.png')}}">
                  </div>
                  <div class="col user-teams__info pl-3">
                    <h6 class="m-0">Team Dante</h6>
                    <span class="text-light">21 Members</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div> --}}
</div>
@endsection
