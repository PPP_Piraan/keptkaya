<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ประปา</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon-96x96.png')}}">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Sarabun:wght@100;300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css')}}" />
    {{-- <link href="{{asset('/css/mycss.css')}}"> --}}
    {{-- <link rel="stylesheet" href="{{asset('/datatables.1.10.20/dataTables.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
    <style>
        * {
            font-family: 'Sarabun', sans-serif, 'Font Awesome 5 Free' !important;
        }

        .hidden {
            display: none
        }

        .show {
            display: block
        }
        .nav-header{
            text-decoration: underline
        }

    </style>
    {{-- <link rel="stylesheet" type="text/css"
    href="{{url('css/mycss.css')}}"> --}}
    
    <script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script>
    <!-- Chartisan -->
    <script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>
    @yield('style')
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    {{-- <a href="{{asset('adminlte/index3.html')}}" class="nav-link">Home</a> --}}
                </li>
            </ul>


            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link text-primary" data-toggle="dropdown" href="#" aria-expanded="true">
                        {{-- <i class="far fa-user-circle"></i>  --}}
                        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="User Avatar"
                            class="img-size-32 mr-3 img-circle">
                        {{Auth::user()->user_profile->name}}
                        @role('twman')
                          (เจ้าหน้าที่จดมิเตอร์)
                        @endrole

                        @role('superadministrator')
                        (ผู้ดูแลระบบ)
                        @endrole

                        @role('accounting')
                        (การเงิน)
                        @endrole

                        
                    </a>

                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="User Avatar"
                                    class="img-size-50 mr-3 img-circle ">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        ข้อมูลส่วนตัว
                                    </h3>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>

                        <div class="dropdown-divider"></div>
                        <form action="{{route('logout')}}" method="POST">
                            @csrf
                            <button type="submit" class="dropdown-item dropdown-footer text-danger"><i
                                    class="far fa-sign-out" aria-hidden="true"></i>ออกจากระบบ</button>
                        </form>
                    </div>
                </li>

            </ul>

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="javascript:void(0)" class="brand-link">
                <img src="{{asset('logo/'.session('logo'))}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3 organization_logo"
                    style="opacity: .8">
                <span class="brand-text font-weight-light">{{ session('organization_short_name') != "" ? session('organization_short_name') : 'ทด. ห้องแซง'}}</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item has-treeview">
                            <a href="{{url('/admin')}}" class="nav-link active bg-warning @yield('dashboard')">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    หน้าหลัก
                                </p>
                            </a>
                        </li>
                      
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                            <i class="nav-icon fas fa-edit"></i>
                              <p>
                                จัดการใบแจ้งหนี้
                                <i class="right fas fa-angle-left ml-2"></i>
                              </p>
                            </a>
                            <ul class="nav nav-treeview">
                              <li class="nav-item">
                                <a href="{{url('invoice/index')}}" class="nav-link @yield('invoice')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ออกใบแจ้งหนี้</p>
                                </a>
                              </li>
                              @role(['superadministrator', 'administrator', 'accounting']) 

                              <li class="nav-item">
                                <a href="{{url('owepaper/index')}}" class="nav-link @yield('owepaper')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ออกใบแจ้งเตือนชำระหนี้</p>
                                </a>
                              </li>
                              @endrole
                              <li class="nav-item">
                                <a href="{{url('cutmeter/index')}}" class="nav-link @yield('cutmeter')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ค้างชำระเกิน 3 รอบบิล <span id="owe_over3_period" class="badge badge-danger"></span></p>
                                </a>
                              </li>
                              
                            </ul>
                        </li>
                        @role(['superadministrator', 'administrator', 'accounting']) 
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-hand-holding-usd"></i>
                              <p>
                                รับชำระค่าน้ำประปา
                                <i class="right fas fa-angle-left"></i>
                              </p>
                            </a>
                            <ul class="nav nav-treeview">
                              <li class="nav-item">
                                <a href="{{url('payment')}}" class="nav-link  @yield('payment')">
                                    <i class="far fa-circle nav-icon"></i>
                                  <p>รับชำระค่าน้ำประปา</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{url('payment/search')}}" class="nav-link  @yield('payment-search')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ค้นหาใบเสร็จรับเงิน</p>
                                </a>
                              </li>
                              
                            </ul>
                        </li>
                        @endrole
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-file-excel"></i>
                              <p>
                                รายงาน
                                <i class="right fas fa-angle-left"></i>
                              </p>
                            </a>
                            <ul class="nav nav-treeview">
                              <li class="nav-item">
                                <a href="{{url('reports/users')}}" class="nav-link @yield('report-users')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ประวัติการใช้น้ำของผู้ใช้น้ำ</p>
                                </a>
                              </li>
                              @role(['superadministrator', 'administrator', 'accounting']) 
                              <li class="nav-item">
                                <a href="{{url('reports/owe')}}" class="nav-link @yield('report-owe')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ผู้ค้างชำระค่าน้ำประปา</p>
                                </a>
                              </li>
                              {{-- <li class="nav-item">
                                <a href="{{url('reports/cutmeter')}}" class="nav-link @yield('report-cutmeter')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ผู้ค้างชำระเกิน 3 เดือน</p>
                                </a>
                              </li> --}}
                              <li class="nav-item">
                                <a href="{{url('reports/summary')}}" class="nav-link @yield('report-summary')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>สรุปค่าน้ำประปารายเดือน</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{url('reports/payment')}}" class="nav-link @yield('report-payment')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>การชำระค่าน้ำประจำวัน</p>
                                </a>
                              </li>
                              @endrole
                              <li class="nav-item">
                                <a href="{{url('reports/meter_record_history')}}" class="nav-link @yield('report-meter_record_history')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ประวัติจดมาตรวัดน้ำ(ป.31)</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{url('reports/water_used')}}" class="nav-link @yield('report-water_used')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ปริมาณการใช้น้ำประปา</p>
                                </a>
                              </li>
                            </ul>
                        </li>
                        @role(['superadministrator', 'administrator', 'accounting']) 
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-cogs"></i>
                              <p>
                                ตั้งค่า
                                <i class="right fas fa-angle-left"></i>
                              </p>
                            </a>
                            <ul class="nav nav-treeview">
                              <li class="nav-item">
                                <a href="{{url('/users')}}" class="nav-link @yield('users')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ข้อมูลผู้ใช้น้ำ</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{url('/staff')}}" class="nav-link @yield('staff')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>เจ้าหน้าที่ประปา</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link @yield('zone')" href="{{url('zone')}}">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>พื้นที่จัดเก็บค่าน้ำประปา</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link @yield('items')" href="{{url('items')}}">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Items</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link @yield('undertaker-subzone')" href="{{url('undertaker_subzone')}}">
                                  <i class="far fa-circle nav-icon" style="vertical-align: top"></i>
                                  <p>กำหนดผู้รับผิดชอบพื้นที่
                                    <br class="ml-3">จดมิเตอร์</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{('/tabwatermeter')}}" class="nav-link @yield('tabwatermeter')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ขนาดมิเตอร์</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{url('/budgetyear')}}" class="nav-link @yield('budgetyear')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>สร้างปีงบประมาณ</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{url('/invoice_period')}}" class="nav-link @yield('invoice_period')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>สร้างรอบบิล</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{url('/settings')}}" class="nav-link @yield('settings')">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>ทั่วไป</p>
                                </a>
                              </li>
                            </ul>
                        </li>
                        @endrole
                    </ul>

                    
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>@yield('mainheader')</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">หน้าหลัก</a></li>
                                <li class="breadcrumb-item">@yield('nav')</li>

                                <li class="breadcrumb-item">@yield('mainheader')</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.0.4
            </div>
            <strong>Copyright &copy; 2014-2019 <a href="#">AdminLTE.io</a>.</strong> All rights
            reserved.
        </footer>


    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    {{-- <script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script> --}}
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script> --}}
    {{-- <script src="{{asset('../resources/js/app.js')}}"></script> --}}
    <!-- Bootstrap 4 -->
    <script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="{{asset('adminlte/dist/js/demo.js')}}"></script> --}}
    <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/js/bootstrap-datepicker-thai.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}">
    </script>
    <script src="{{asset('datatables.1.10.20/dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.7/pagination/select.js"></script>


    <script>
        $(document).ready(()=>{
            $('ul.nav-treeview').find('a.active').parent().parent().css( "display", "block" );
            $('ul.nav-treeview').find('a.active').parent().parent().parent().addClass( "open-menu menu-open" );
        })
        
    </script>
    <!-- Charting library -->
    @yield('script')

  <script type="text/javascript">
    $(document).ready(()=>{
      let params = { zone_id:  'all', subzone_id:  'all'}
        $.get(`../../api/cutmeter/index`,params).done(function (data) {
            $('#owe_over3_period').html(data.length)
      });

   
    })
  </script>
</body>

</html>
