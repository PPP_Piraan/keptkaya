@extends('layouts.adminlte')
<?php
     use App\Http\Controllers\Api\FunctionsController; 
     $apiFuncCtrl = new FunctionsController();
?>
@section('mainheader')
รายงานสรุปการชำระค่าน้ำประปา
@endsection
@section('nav')
<a href="{{'reports'}}">รายงาน</a>
@endsection
@section('report-payment')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }

</style>
@endsection

@section('content')


{{-- ตารางสรุปแยกรอบบิล --}}
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <form action="{{ url('reports/summary') }}" method="GET" class="row">
                ปีงบประมาณ
                <select name="budgetyear" id="budgetyear" class="form-control col-md-4">
                    @foreach ($budgetyears as $budgetyear)
                        <option value="{{$budgetyear->id}}" {{$budgetyear->id == $selectedBudgetYear[0]->id ? 'selected': ''}}> {{ $budgetyear->budgetyear }} </option>
                    @endforeach
                </select>
                <input type="submit" value="ค้นหา" class="btn btn-info col-md-3 ml-1">
            </form>
        </div>
        <div class="card-tools">
            <button class="btn btn-primary" id="printBtn">ปริ้น</button>
            <button class="btn btn-success" id="excelBtn">Excel</button>
        </div>
    </div>
    <div id="DivIdToExport"> 
        <div class="card-body">
            <div class="card">
                <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                    <h3 class="card-title">
                        <i class="fas fa-th mr-1"></i>
                        <table><tr><td>สรุปยอดการจ่ายค่าน้ำประปาแยกตามรอบบิล ปีงบประมาณ {{ $selectedBudgetYear[0]->budgetyear }}</td></tr></table>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table  table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center align-top" rowspan="2">รอบบิล</th>
                                <th class="text-center align-top" rowspan="2">เดือน</th>
                                <th colspan="3" class="text-center bg-success">ชำระเงินแล้ว</th>
                                <th colspan="3" class="text-center bg-warning">ค้างชำระ</th>
                                <th class="text-center align-top bg-info" rowspan="2">รวมทั้งหมด</th>
                            </tr> 
                            <tr>
                            
                                <th class="text-right">ค่าน้ำประปา</th>
                                <th class="text-right">ค่ารักษามิเตอร์</th>
                                <th class="text-right bg-success">รวม</th>
                                <th class="text-right">ค่าน้ำประปา</th>
                                <th class="text-right">ค่ารักษามิเตอร์</th>
                                <th class="text-right bg-warning">รวม</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $paid_water_used_total      = 0;
                                $paid_reserve_meter_total   = 0;
                                $paid_total_total           = 0;
                                $owe_water_used_total       = 0;
                                $owe_reserve_meter_total    = 0;
                                $owe_total_total            = 0;
                                $total                      = 0;
                            ?>
                            @foreach ($groupByinvoicePeriod as $item)
                            <?php
                                $paid_water_used_total += $item['paid']['water_used_sum'];
                                $paid_reserve_meter_total += $item['paid']['reservemeter_sum'];
                                $paid_total_total += $item['paid']['total'];
                                $owe_water_used_total += $item['oweAndInvoice']['water_used_sum'];
                                $owe_reserve_meter_total += $item['oweAndInvoice']['reservemeter_sum'];
                                $owe_total_total += $item['oweAndInvoice']['total'];
                                $total += $item['paid']['total'] + $item['oweAndInvoice']['total'];
                            ?>
                            <tr>
                                {{-- {{dd($item)}} --}}
                                <td class="text-right">{{ $item['inv_period_name'] }}</td>
                                <td class="text-right">{{ $item['monthName'] }}</td>
                                <td class="text-right">{{ number_format($item['paid']['water_used_sum']) }}</td>
                                <td class="text-right">{{ number_format($item['paid']['reservemeter_sum']) }}</td>
                                <td class="text-right bg-success">{{ number_format($item['paid']['total']) }}</td>
                                <td class="text-right">{{ number_format($item['oweAndInvoice']['water_used_sum']) }}</td>
                                <td class="text-right">{{ number_format($item['oweAndInvoice']['reservemeter_sum']) }}</td>
                                <td class="text-right bg-warning">{{ number_format($item['oweAndInvoice']['total']) }}</td>
                                <td class="text-right bg-info">{{ number_format($item['paid']['total'] + $item['oweAndInvoice']['total'] ) }}</td>            
                            </tr>
                            @endforeach
                            <tr>
                                <th class="bg-primary text-center" colspan="2"> รวม</th>
                                <th class="bg-primary text-right">{{ number_format($paid_water_used_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($paid_reserve_meter_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($paid_total_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($owe_water_used_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($owe_reserve_meter_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($owe_total_total) }}</th>
                                <th class="bg-primary text-right">{{ number_format($total) }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card">
                <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                    <h3 class="card-title">
                        <i class="fas fa-th mr-1"></i>
                        <table><tr><td>สรุปยอดการจ่ายค่าน้ำประปาแยกตามเดือน ปีงบประมาณ {{ $selectedBudgetYear[0]->budgetyear }}</td></tr></table>
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
            
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center align-top" rowspan="2">เดือน</th>
                                <th colspan="3" class="text-center bg-success">ชำระเงินแล้ว</th>
                                <th colspan="3" class="text-center bg-warning">ค้างชำระ</th>
                                <th class="text-center align-top bg-info" rowspan="2">รวมทั้งหมด</th>
                            </tr> 
                            <tr>
                                <th class="text-center">ค่าน้ำประปา</th>
                                <th class="text-center">ค่ารักษามิเตอร์</th>
                                <th class="text-center bg-success">รวม</th>
                                <th class="text-center">ค่าน้ำประปา</th>
                                <th class="text-center">ค่ารักษามิเตอร์</th>
                                <th class="text-center bg-warning">รวม</th>
                            </tr>
                        </thead>
                        <?php
                            $paid_water_used_total2      = 0;
                            $paid_reserve_meter_total2   = 0;
                            $paid_total_total2           = 0;
                            $owe_water_used_total2       = 0;
                            $owe_reserve_meter_total2    = 0;
                            $owe_total_total2            = 0;
                            $total2                      = 0;
                        ?>
                    @foreach ($groupedByMonth as $item)
                        <?php
                            $paid_water_used_total2     += $item['data']['paid']['used_water_paid'];
                            $paid_reserve_meter_total2  += $item['data']['paid']['paid_reserve'];
                            $paid_total_total2          += $item['data']['paid']['paidTotal'];
                            $owe_water_used_total2      += $item['data']['owe']['used_water_paid'];
                            $owe_reserve_meter_total2   += $item['data']['owe']['owe_reserve'];
                            $owe_total_total2           += $item['data']['owe']['oweTotal'];
                            $total2                     += $item['data']['paid']['paidTotal']  + $item['data']['owe']['oweTotal'];
                        ?>
                        <tr>
                            <td class="text-right">{{$item['month']}}</td>
                            <td class="text-right">{{number_format($item['data']['paid']['used_water_paid'])}}</td>
                            <td class="text-right">{{number_format($item['data']['paid']['paid_reserve'])}}</td>
                            <td class="text-right bg-success">{{ number_format( $item['data']['paid']['paidTotal'] ) }}</td>
                            <td class="text-right">{{number_format($item['data']['owe']['used_water_paid'])}}</td>
                            <td class="text-right">{{number_format($item['data']['owe']['owe_reserve'])}}</td>
                            <td class="text-right bg-warning">{{ number_format( $item['data']['owe']['oweTotal'] ) }}</td>
                            <td class="text-right bg-info">{{ number_format( $item['data']['paid']['paidTotal']  + $item['data']['owe']['oweTotal'] ) }}</td>
                        </tr>
                    @endforeach
                        <tr>
                            <th class="bg-primary text-center"> รวม</th>
                            <th class="bg-primary text-right">{{ number_format($paid_water_used_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($paid_reserve_meter_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($paid_total_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($owe_water_used_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($owe_reserve_meter_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($owe_total_total2) }}</th>
                            <th class="bg-primary text-right">{{ number_format($total2) }}</th>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div><!--<div id="DivIdToExport">-->
</div>

@endsection


@section('script')

<script
    src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
</script>

<script src="{{asset('/js/my_script.js')}}"></script>
<script>
    $('#printBtn').click(function () {
        var tagid = 'DivIdToExport'
        var hashid = "#" + tagid;
        var tagname = $(hashid).prop("tagName").toLowerCase();
        var attributes = "";
        var attrs = document.getElementById(tagid).attributes;
        $.each(attrs, function (i, elem) {
            attributes += " " + elem.name + " ='" + elem.value + "' ";
        })
        var divToPrint = $(hashid).html();
        var head = "<html><head>" + $("head").html() + "</head>";
        var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
            divToPrint + "</" + tagname + ">" + "</body></html>";
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write(allcontent);
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    })

    $('#excelBtn').click(function () {
        $("#DivIdToExport").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    });

</script>
@endsection

                         
