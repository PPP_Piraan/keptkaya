@extends('layouts.adminlte')

@section('mainheader')
ข้อมูลปริมาณการใช้น้ำประปา
@endsection
@section('nav')
<a href="{{url('/reports')}}"> รายงาน</a>
@endsection
@section('report-water_used')
active
@endsection
@section('style')
<style href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"></style>
    <style>
        th,
        td {
        white-space: nowrap;
        }
        .hide{
            display: none
        }
        .username{
            color: blue;
            cursor: pointer;
            /* padding-left: 20px */
        }
    </style>
@endsection

@section('content')
<div class="card">
  <div class="card-body">
      <div class="info-box">

          <div class="info-box-content">
            <form action="{{url('reports/water_used')}}" method="GET">
              @csrf
              <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                    <label class="col-form-label">ปีงบประมาณ</label>
                    <select class="form-control" name="budgetyear" id="budgetyear">
                        {{-- <option value="now" selected>เลือก..</option> --}}
                        @foreach ($budgetyears as $budgetyear)
                            <option value="{{$budgetyear->id}}" {{$budgetyear->status == 'active' ? 'selected' : ''}}>{{$budgetyear->budgetyear}}</option>   
                        @endforeach
                    </select>
                    </div>
                </div>
                  <div class="col-md-2">
                      <div class="form-group">
                          <label for="search" class="col-form-label">หมู่ที่:</label>
                              <select class="form-control" name="zone_id" id="zone_id">
                                  <option value="all" {{$zone_id == "all" ? 'selected' : ''}}>ทั้งหมด</option>
                                  @foreach ($zones as $zone)
                                  <option value="{{$zone->id}}" {{$zone_id == $zone->id ? 'selected' : ''}}>
                                      {{$zone->zone_name}}</option>
                                  @endforeach
                              </select>
                      </div>
                  </div>
                  <div class="col-md-2">
                      <div class="form-group">
                          <label for="search" class="col-form-label">เส้นทาง:</label>
                              <select class="form-control" name="subzone_id" id="subzone_id">
                                  @if ($subzone_id == 'all')
                                  <option value="all" selected>ทั้งหมด</option>
                                  @else
                                  @foreach ($subzones as $subzone)
                                  <option value="{{$subzone->id}}"
                                      {{$subzone_id == $subzone->id ? 'selected' : ''}}>
                                      {{$subzone->subzone_name}}
                                  </option>
                                  @endforeach
                                  @endif
                              </select>
                      </div>
                  </div>  
                  <div class="col-md-2">
                    <div class="form-group">
                        <label for="search" class="col-form-label">เดือน:</label>
                            <?php
                                $thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 
                            ?>
                            <select class="form-control" name="fromdate" id="fromdate">
                                <option value="all" selected>ทั้งหมด</option>
                            @foreach ($thaimonth as $key => $item)
                                <option value="{{$key}}">{{ $item }}</option>
                            @endforeach
                            </select>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group  text-center">
                        <label for="search" class="col-form-label text-center">&nbsp;</label>
                        <br>
                            ถึง
                    </div>
                </div>    
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="search" class="col-form-label">เดือน:</label>
                        <select class="form-control" name="todate" id="todate">
                            <option value="all" selected>ทั้งหมด</option>
                            @foreach ($thaimonth as $key => $item)
                                <option value="{{$key}}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>            
                  {{-- <div class="col-md-2">
                      <div class="form-group">
                          <label for="search" class="col-form-label">วันที่:</label>
                              <input class="form-control datepicker" type="text" name="fromdate" id="fromdate">
                      </div>
                  </div>
                  <div class="col-md-2">
                      <div class="form-group">
                          <label for="search" class="col-form-label">วันที่:</label>
                              <input class="form-control datepicker" type="text" name="todate" id="todate">
                      </div>
                  </div> --}}
                  <div class="col-md-1">
                      <label class="control-label mt-1">&nbsp;</label>
                      <button type="submit" id="searchBtn" class="form-control btn btn-primary">ค้นหา</button>
                  </div>
              </div><!--row-->
            </form>
          </div><!-- /.info-box-content -->
      </div><!--info-box-->
  </div><!--card-body-->
</div><!--card-->
<div class="row">
    <div class="col-12">
      <div class="card direct-chat direct-chat-primary">
        <div class="card-body mb-3">
            @if ($infosCount == 0)
                <div class="text-center mt-3"><h2>ไม่พบข้อมูล</h2></div>
            @else
                <table class="table">
                    <tr>
                        <td>เส้น</td>
                        <td>ปริมาณการใช้น้ำ(หน่วย)</td>
                    </tr>
                @foreach ($dataArray as $item)
                    <tr>
                        <td>{{ $item[0] }}</td>
                        <td>{{ $item[1] }}</td>
                    </tr>
                @endforeach
                </table>
                {{-- <div id="stocks-div" style="margin-left: -30px"></div> --}}
            @endif
          
        </div><!--.card-body-->
  
      </div><!--card direct-chat-->
    </div><!--col-12-->
</div><!--row-->

@columnchart('Stocks', 'stocks-div')

@endsection


@section('script')
<script src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js"></script>

<script>
  $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true,
        }).datepicker("setDate", new Date());; //กำหนดเป็นวันปัจุบัน
  }); //$document

  $('#zone_id').change(function () {
      //get ค่าsubzone 
      $.get(`../api/subzone/${$(this).val()}`)
          .done(function (data) {
              let text = '<option value="all" selected>ทั้งหมด</option>';
              data.forEach(element => {
                  text += `<option value="${element.id}">${element.subzone_name}</option>`
              });
              $('#subzone_id').html(text)
          });
  });//$#zone_id


  // const chart2 = new Chartisan({
  //   el: '#waterUsed',
  //   url: "@chart('water_used_chart')",
  //   hooks: new ChartisanHooks()
  //       .datasets('bar')
  //       .pieColors(),
  // });
</script>  
   

@endsection