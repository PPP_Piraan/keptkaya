@extends('layouts.adminlte')

@section('mainheader')
 ผู้ค้างชำระเกิน 5 เดือน
@endsection
@section('nav')
<a href="{{'owepaper/cutmeter'}}"> ผู้ค้างชำระเกิน 5 เดือน</a>
@endsection
@section('report-cutmeter')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }
    .dt-buttons{
        margin-left:4rem !important
    }
    th{
        text-align:  center
    }
    table{
  margin: 0 auto;
  /* width: 100%; */
  clear: both;
  border-collapse: collapse;
  table-layout: fixed; 
  word-wrap:break-word; 
}
</style>
<meta name="description" content="Export with customisable file name" />
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    
    
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        {{-- ค้นหา --}}
        {{-- <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>
            <div class="info-box-content">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">หมู่ที่</label>
                        <select class="form-control" name="zone_id" id="zone_id">
                            <option value="all" selected>ทั้งหมด</option>
                            @foreach ($zones as $zone)
                            <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">เส้นทาง</label>
                        <select class="form-control" name="subzone_id" id="subzone_id">
                            <option value="all" selected>ทั้งหมด</option>

                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">&nbsp;</label>
                        <button type="button" class="form-control btn btn-primary searchBtn">ค้นหา</button>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<div class="row hidden" id="sum">
    {{-- <div class="col-md-4 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">ค่าใช้น้ำ (บาท)</span>
          <span class="info-box-number" id="water_used_sum"></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div> --}}
    <!-- /.col -->
    {{-- <div class="col-md-4 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">ค่ารักษามิเตอร์ (บาท)</span>
          <span class="info-box-number" id="reserve_sum"></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div> --}}
    <!-- /.col -->
    {{-- <div class="col-md-4 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">รวมทั้งสิ้น (บาท)</span>
          <span class="info-box-number" id="total"></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div> --}}
    <!-- /.col -->
  </div>
    <div class="card">
        <div class="card-header">
            <div class="card-title"></div>
            <div class="card-tools">
                    <button class="btn btn-primary" id="printBtn">ปริ้น</button>
                    <button class="btn btn-success" id="excelbtn2">Excel</button>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="example2" class="table table-striped">
                    <thead>
                        <tr><th colspan="6" class="text-center h4">
        ผู้ค้างชำระค่าน้ำประปาเกิน 5 รอบบิล
                            
                        </th></tr>
                        <tr>
                            <th class="text-center" width='18%'>ชื่อ</th><!--ชื่อ -->
                            <th class="text-center" width='10%'>เลขมิเตอร์</th><!-- เลขมิเตอร์-->
                            <th class="text-center" width='12%'>บ้านเลขที่</th><!--บ้านเลขที่ -->
                            <th class="text-center" width='9%'>หมู่ที่</th><!--หมู่ที่ -->
                            <th class="text-center" width='8%'>เส้นทาง</th><!--เส้นทาง -->
                            <th class="text-center" width='10%'>ค้างจ่าย</th><!--ค้างจ่าย -->
                            {{-- <th class=""></th>
                            <th class=""></th>
                            <th class=""></th>
                            <th class=""></th>
                            <th class="text-center" width='8%'></th><!--ค่าใช้น้ำ -->
                            <th class="text-center" width='12%'></th><!--ค่ารักษามิเตอร์ -->
                            <th class="text-center" width='7%'></th><!--รวม -->
                            <th></th><!-- --> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cutmeters as $cutmeter)
                        <tr>
                            <td class="text-left" width='18%'>{{ $cutmeter->user_profile->name }}</td><!--ชื่อ -->
                            <td class="text-center" width='10%'>{{ $cutmeter->meternumber }}</td><!-- เลขมิเตอร์-->
                            <td class="text-center" width='12%'>{{ $cutmeter->user_profile->address }}</td><!--บ้านเลขที่ -->
                            <td class="text-center" width='9%'>{{ $cutmeter->zone->zone_name }}</td><!--หมู่ที่ -->
                            <td class="text-center" width='8%'>{{ $cutmeter->subzone->subzone_name }}</td><!--เส้นทาง -->
                            <td class="text-center" width='10%'>{{ $cutmeter->owe_count }}</td><!--ค้างจ่าย -->
                            {{-- <th class=""></th>
                            <th class=""></th>
                            <th class=""></th>
                            <th class=""></th>
                            <th class="text-center" width='8%'></th><!--ค่าใช้น้ำ -->
                            <th class="text-center" width='12%'></th><!--ค่ารักษามิเตอร์ -->
                            <th class="text-center" width='7%'></th><!--รวม -->
                            <th></th><!-- --> --}}
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
      {{-- <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div> --}}
    </div>
@endsection


    @section('script')
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    {{-- <script src="https://cdn.datatables.net/buttons/2.0.0/js/buttons.print.min.js"></script> --}}
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>

    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{asset('/js/my_script.js')}}"></script>
    <script>
        var table
        var datas
        let a = true
        let col_index = -1
        var selected_row = []
        $(document).ready(function () {
           // getOweInfos()
        });
       

        function getOweInfos(zone_id= 'all', subzone_id = 'all'){
            let params = {
                zone_id: zone_id,
                subzone_id: subzone_id
            }
            
            $.get(`../api/owepaper/owe`, params).done(function (data) {
                datas =data
                table = $('#example').DataTable({
                    "pagingType": "listbox",
                    "searching": true,
                    "lengthMenu": [
                        [10, 25, 50, 150, -1],
                        [10, 25, 50, 150, "ทั้งหมด"]
                    ],
                    "language": {
                        "search": "ค้นหา:",
                        "lengthMenu": "แสดง _MENU_ แถว",
                        "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                        "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                        "paginate": {
                            "info": "แสดง _MENU_ แถว",
                        },
                    
                    },
                 
                    drawCallback: function () {
                        var sum = $('#example').DataTable().column(11).data().sum();
                        $('#water_used_sum').html(new Intl.NumberFormat().format(sum));
                        var reserve_sum = $('#example').DataTable().column(12).data().sum();
                        $('#reserve_sum').html(new Intl.NumberFormat().format(reserve_sum));
                        var total = $('#example').DataTable().column(13).data().sum();
                        $('#total').html(new Intl.NumberFormat().format(total));
                    },
                    select: {
                        style: 'multi'
                    },
                    data: datas,
                      
                    columns: [
                        {
                            'title': 'ชื่อ-สกุล',
                            data: 'name',
                        },
                        {
                            'title': 'เลขมิเตอร์',
                            data: 'meternumber',
                            className: 'text-center ',
                           
                        },
                        {
                            'title': 'บ้านเลขที่',
                            data: 'address',
                            className: ' text-center'
                        },
                        {
                            'title': 'หมู่ที่',
                            data: 'zone_name',
                            className: ' text-center'
                        },
                        {
                            'title': 'เส้นทาง',
                            data: 'subzone_name',
                            className: ' text-center'
                        }, 
                        {
                            'title': 'ค้างจ่าย (เดือน)',
                            data: 'oweInvCount',
                            'className': ' text-center'
                        },
                        {
                            data: 'empty',
                            className:'hidden'
                        },
                        {
                            data: 'empty',
                            className:'hidden'
                        },
                        {
                            data: 'empty',
                            className:'hidden'
                        },
                        {
                            data: 'empty',
                            className:'hidden'
                        },
                        {
                            'title':  'ค่าใช้น้ำ (บาท)',
                            data: 'sum_paid',
                            'className': 'text-right'
                        },
                        {
                            'title': 'ค่ารักษามิเตอร์ (บาท)',
                            data: 'sum_reserve_paid',
                            'className': 'text-right'
                        },
                        {
                            'title': 'รวม (บาท)',
                            data: 'total',
                            'className': 'text-right'
                        },
                        {
                            data: null,
                            className:'hidden last',
                            render: function (data, type, row, meta) {
                                return meta.row;
                            }
                        }
                    ],
                    dom: 'lBfrtip',
                    buttons: [
                        {
                        extend: 'excelHtml5',
                        'text':'Excel',
                        customize: function (xlsx) {
                            var table = $('#example').DataTable();

                            // Get number of columns to remove last hidden index column.
                            var numColumns = table.columns().header().count();

                            // Get sheet.
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            var col = $('col', sheet);
                            // Set the column width.
                            $(col[1]).attr('width', 20);

                            // Get a clone of the sheet data.        
                            var sheetData = $('sheetData', sheet).clone();
                            
                            // Clear the current sheet data for appending rows.
                            $('sheetData', sheet).empty();

                            // Row count in Excel sheet.
                            var rowCount = 1;
                            var selected_rows = [];
                                $('.selected').each((index)=>{
                                        selected_row.push(index)
                                })
                            // Itereate each row in the sheet data.
                            $(sheetData).children().each(function (index) {

                                // Used for DT row() API to get child data.
                               // if(row.indexOf(index)){
                                    var rowIndex = index - 1;
                                    
                                    // Don't process row if its the header row.
                                    if (index > 0) {

                                        // Get row
                                        var row = $(this.outerHTML);

                                        // Set the Excel row attr to the current Excel row count.
                                        row.attr('r', rowCount);

                                        var colCount = 1;

                                        // Iterate each cell in the row to change the rwo number.
                                        row.children().each(function (index) {
                                            var cell = $(this);

                                            // Set each cell's row value.
                                            var rc = cell.attr('r');
                                            rc = rc.replace(/\d+$/, "") + rowCount;
                                            cell.attr('r', rc);

                                            if (colCount === numColumns) {
                                                cell.html('');
                                            }

                                            colCount++;
                                        });

                                        // Get the row HTML and append to sheetData.
                                        row = row[0].outerHTML;
                                        $('sheetData', sheet).append(row);
                                        rowCount++;
                                    
                                        // Get the child data - could be any data attached to the row.
                                        var childData = table.row(':eq(' + rowIndex + ')').data().results;
                                        if (childData.length > 0) {
                                            // Prepare Excel formated row
                                            headerRow = `
                                            <row r="${rowCount}" s="2">
                                                <c t="inlineStr" r="A${rowCount} "><is><t></t></is></c>
                                                <c t="inlineStr" r="B${rowCount} "><is><t></t></is></c>
                                                <c t="inlineStr" r="C${rowCount} " s="2"><is><t>เลขใบแจ้งหนี้</t></is></c>
                                                <c t="inlineStr" r="D${rowCount} " s="2"><is><t>รอบบิล</t></is></c>
                                                <c t="inlineStr" r="E${rowCount} " s="2"><is><t>ยอดปัจจุบัน</t></is></c>
                                                <c t="inlineStr" r="F${rowCount} " s="2"><is><t>ยอดครั้งก่อน</t></is></c>
                                                <c t="inlineStr" r="G${rowCount} " s="2"><is><t>จำนวนที่ใช้</t></is></c>
                                                <c t="inlineStr" r="H${rowCount} " s="2"><is><t>ค่าใช้น้ำ(บาท)</t></is></c>
                                                <c t="inlineStr" r="I${rowCount} " s="2"><is><t>ค่ารักษามิเตอร์(บาท)</t></is></c>
                                                <c t="inlineStr" r="J${rowCount} " s="2"><is><t>เป็นเงิน(บาท)</t></is></c>
                                            </row>`;
                                            // Append header row to sheetData.
                                            if($('body input[type="checkbox"]').is(':checked')){
                                                $('sheetData', sheet).append(headerRow);
                                                rowCount++; // Inc excelt row counter.

                                            }

                                        }
                                        // The child data is an array of rows
                                        for (c = 0; c < childData.length; c++) {
                                            // Get row data.
                                            child = childData[c];
                                            // Prepare Excel formated row
                                            childRow = `
                                                <row r="${rowCount}" s="2">
                                                    <c t="inlineStr" r="A${rowCount}"><is><t></t></is></c>
                                                    <c t="inlineStr" r="B${rowCount}"><is><t></t></is></c>
                                                    <c t="inlineStr" r="C${rowCount}"><is><t> ${child.invoice_id}</t></is></c>
                                                    <c t="inlineStr" r="D${rowCount}"><is><t> ${child.inv_period_name}</t></is></c>
                                                    <c t="inlineStr" r="E${rowCount}"><is><t> ${child.currentmeter}</t></is></c>
                                                    <c t="inlineStr" r="F${rowCount}"><is><t> ${child.lastmeter}</t></is></c>
                                                    <c t="inlineStr" r="G${rowCount}"><is><t> ${child.water_used}</t></is></c>
                                                    <c t="inlineStr" r="H${rowCount}"><is><t> ${child.paid}</t></is></c>
                                                    <c t="inlineStr" r="I${rowCount}"><is><t> ${child.reserve_paid}</t></is></c>
                                                    <c t="inlineStr" r="J${rowCount}"><is><t> ${child.paid_net}</t></is></c>
                                                </row>`;

                                            // Append row to sheetData.
                                            if($('body input[type="checkbox"]').is(':checked')){
                                                $('sheetData', sheet).append(childRow);
                                                rowCount++; // Inc excelt row counter.

                                            }

                                        }
                                        // Just append the header row and increment the excel row counter.
                                    } else {
                                        var row = $(this.outerHTML);

                                        var colCount = 1;

                                        // Remove the last header cell.
                                        row.children().each(function (index) {
                                            var cell = $(this);

                                            if (colCount === numColumns) {
                                                cell.html('');
                                            }

                                            colCount++;
                                        });
                                        row = row[0].outerHTML;

                                        $('sheetData', sheet).append(row);
                                        
                                        rowCount++;
                                    }
                                //}//indexof
                            });
                        },
                    }],
                   
                });//table
                $('#sum').removeClass('hidden')
                if(a){
                       $('#example thead tr').clone().appendTo('#example thead');
                        a= false
                }
                $('#example thead tr:eq(1) th').each( function (index) {
                    var title = $(this).text();
                    $(this).removeClass('sorting')
                    $(this).removeClass('sorting_asc')
                    if(index < 4){
                        $(this).html( `<input type="text" data-id="${index}" class="form-control" id="search_col_${index}" placeholder="ค้นหา" />` );
                    }else{
                        $(this).html('')
                    }
                } );

                $('input[type="text"]').keyup(function(){
                    let that = $(this)
                    var col = parseInt(that.data('id'))
                    
                    var _val = that.val()
                    if(col === 1 || col===2){
                        var val = $.fn.dataTable.util.escapeRegex(
                            _val
                        );
                        table.column(col)
                        .search( val ? '^'+val+'.*$' : '', true, false )
                        .draw();
                    }else{
                        table.column(col)
                        .search( _val )
                        .draw();
                    }
                    col_index = col
                })

                $('input[type="text"]').focus(function(){
                    var col = parseInt($(this).data('id'))

                    if(col !==  col_index && col_index >= 0){
                         $('input[type="text"]').val('')

                    }
                    col_index = col
                })


                $('.overlay').remove()
                $('#example_filter').remove()
                $('.dt-buttons').prepend(`
                    <button class="dt-button buttons-excel buttons-html5 show_all_btn all" >เลือกทั้งหมด</button>
                    <input type="checkbox" class="dt-button buttons-excel buttons-html5 show_detail mr-1 mt-1" >แสดงข้อมูลค้างชำระ </input> 
                `)
                $('.dt-buttons').append(`<button class="dt-button buttons-excel buttons-html5" data-printtype="" id="printBtn">ปริ้น</button> `)
            })
        }

        $('body').on('click', '.show_all_btn',function(){
            let _val = $(this).hasClass('all') ? 'all' : '';
            openAllChildTable(_val)
        })

        $('body').on('click', '.show_detail',function(){
            if($(this).is(':checked')){
                 // dlet arr = [];
                $('tbody tr[role="row"]').each((index, value)=>{
                    if($(value).hasClass('selected')){
                        var tr = $(value).closest('tr');
                        var row = table.row(tr);
                        row.child(format(row.data())).show();
                    }
                });
            }else{

                $('tbody tr[role="row"]').each((index, value)=>{
                    if($(value).hasClass('selected')){
                        var tr = $(value).closest('tr');
                        var row = table.row(tr);
                        row.child.hide();
                        
                    }
                });
               
            }
        })

        // $('body').on('click','#printBtn',function () {
        //     let texttitle = `<tr id="texttitle"><th colspan="9">รายงานผู้ค้างชำระค่าน้ำประปาเกิน 5 รอบบิล</th></tr>`    
        //     $('#example2 thead tr:eq(1)').addClass('hidden')
            
        //     $('#example thead').prepend(texttitle)
        //     var tagid = 'example2'
        //     var hashid = "#" + tagid;
        //     var tagname = $(hashid).prop("tagName").toLowerCase();
        //     var attributes = "";
        //     var attrs = document.getElementById(tagid).attributes;
        //     $.each(attrs, function (i, elem) {
        //         attributes += " " + elem.name + " ='" + elem.value + "' ";
        //     })
        //     var divToPrint = $(hashid).html();
        //     var head = '<html><head>' 
        //         + $("head").html() 
        //         + ' <style>body{background-color:white !important;}@page { size: landscape; }</style></head>';
        //     var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
        //         divToPrint + "</" + tagname + ">" + "</body></html>";
        //     var newWin = window.open('', 'Print-Window');
        //     newWin.document.open();
        //     newWin.document.write(allcontent);
        //     newWin.document.close();
        //     setTimeout(function () {
        //         newWin.close();
        //         $('#texttitle').remove()

        //         $('#example thead tr:eq(1)').removeClass('hidden')

        //     }, 220);

        // })

        function openAllChildTable(_val){            
            if(_val === 'all'){
                $("table > tbody > tr[role='row']").each(function () {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    if($('.show_detail').is(':checked')){
                        row.child(format(row.data())).show();

                    }
                    tr.addClass('shown');
                    tr.addClass('selected')
                    $('.show_all_btn').removeClass('all')
                    $('.show_all_btn').text('ยกเลิกเลือกทั้งหมด')
                })

            }else{
                $("table > tbody > tr[role='row']").each(function () {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    row.child.hide();
                    tr.removeClass('shown');
                    tr.removeClass('selected');
                    $('.show_all_btn').addClass('all')
                    $('.show_all_btn').text('เลือกทั้งหมด')
                    $('.show_detail').prop('checked', false);
                   
                })

            }
            
        }

        $('#example').on('click', 'tr[role="row"]', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if(tr.hasClass('selected')){
                //เอา selected, shown  ออก 
                //เอา row.child ออก
                row.child.hide();
                tr.removeClass('selected')

                tr.removeClass('shown');
            }else{
                    var data = row.data();
                    if($('.show_detail').is(':checked')){
                        row.child(format(data)).show();
                    }
                    tr.addClass('shown');
                    tr.addClass('selected')
            }
                
        });

        $('#zone_id').change(function () {
            //get ค่าsubzone 
            let zone_id = $(this).val()

            $.get(`../api/subzone/${zone_id}`)
                .done(function (data) {
                    let text = zone_id !== 'all' ? '<option value="">เลือก</option>' : '<option value="all">ทั้งหมด</option>';
                    if(data.length > 1){
                        text += `<option value="all">ทั้งหมด</option>`;
                    }
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.subzone_name}</option>`
                    });
                    $('#subzone_id').html(text)
                });
        });

        $('.searchBtn').click(function(){
            let zone_id = $('#zone_id').val()
            let subzone_id = $('#subzone_id').val()
            if(zone_id !== 'all' && subzone_id === ''){
                alert('ยังไม่ได้เลือกเส้นทาง')
                $('#subzone_id').addClass('border border-danger rounded')
                return false
            }
            $('#example').DataTable().destroy();

            getOweInfos(zone_id, subzone_id)
        });

        function format(d) {
            var html = `
                        <table class="border border-info rounded-pill" cellpadding="5" cellspacing="0" border="0" style="margin-left:50px;font-size:14.5px !important">
                            <thead>
                                <tr>
                                    <th class=text-center> </th>
                                    <th class=text-center>ใบแจ้งหนี้ </th>
                                    <th class=text-center>รอบบิล </th>
                                    <th class=text-center>ยอดปัจจุบัน </th>
                                    <th class=text-center>ยอดครั้งก่อน </th>
                                    <th class=text-center>จำนวนที่ใช้ </th>
                                    <th class=text-center>ค่าใช้น้ำ(บาท) </th>
                                    <th class=text-center>ค่ารักษามิเตอร์(บาท) </th>
                                    <th class=text-center>เป็นเงิน(บาท) </th>
                                </tr>
                            </thead>
                            <tbody>`;
            for (i = 0; i < d.results.length; i++) {
                result = d.results[i];

                html += `<tr>
                            <td></td>
                            <td class="text-right">${result['invoice_id']}</td>
                            <td class="text-center"> ${result['inv_period_name']} </td>
                            <td class="text-right"> ${result['currentmeter']} </td>
                            <td class="text-right"> ${result['lastmeter']} </td>
                            <td class="text-right"> ${result['water_used']} </td>
                            <td class="text-right"> ${result['paid']} </td>
                            <td class="text-right"> ${result['reserve_paid']} </td>
                            <td class="text-right"> ${result['paid_net']} </td>
                        </tr>`;

            }
                html +=`</tbody></table>`;



            return html;
        }


        $('#printBtn').click(function(){
        var tagid = 'example2'
        var hashid = "#"+ tagid;
    var tagname =  $(hashid).prop("tagName").toLowerCase() ;
    var attributes = ""; 
    var attrs = document.getElementById(tagid).attributes;
        $.each(attrs,function(i,elem){
        attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
        })
    var divToPrint= $(hashid).html() ;
    var head = "<html><head>"+ $("head").html() + "</head>" ;
    var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write(allcontent);
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
    })
    $('#excelbtn2').click(function(){
        $("#example2").table2excel({
        // exclude CSS class
        exclude: ".noExl",
        name: "Worksheet Name",
        filename: 'aa', //do not include extension
        fileext: ".xls" // file extension
    })
    })

    </script>
    @endsection 
