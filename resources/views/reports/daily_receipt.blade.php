@extends('layouts.adminlte')

@section('mainheader')
รายรับค่าน้ำประปาประจำวัน
@endsection
@section('nav')
    <a href="{{'reports'}}">รายงาน</a>
@endsection
@section('report-daily_receipts')
    active
@endsection
@section('style')
    <style>
        .hidden{display: none}
         
        td.detailss-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
        }

        tr.shown td.detailss-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
        .hide{
            display: none
        }
        .show{
            display: block;
        }
        #example th{
            text-align: center;
            background-color:lightblue
        }
    </style>
@endsection

@section('content')
<?php 
    $owe_total_count        = 0;
    $reserve_total_sum      = 0;
    $owe_total_sum          = 0;
    $meter_unit_used_sum    = 0;
    $all_total_sum          = 0;
?>
 <div id="aa">  
    <form action="{{url('reports/daily_receipt')}}" method="GET">
        @csrf
        <div class="row">
        
            <div class="col-md-3">
                <div class="form-group row">
                    <label for="search" class="col-sm-3 col-form-label">วันที่:</label>
                    <div class="col-sm-9">
                        <input class="form-control datepicker" type="text" name="date"  id="date"> 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group row">
                    <label for="search" class="col-sm-4 col-form-label">ผู้รับเงิน:</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="receipt_id" id="receipt_id">
                            <option>เลือก..</option>
                            <option value="">ทั้งหมด</option>
                            <option value="">จงจิต</option>
                            <option>ปี</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="col-md-2">
                <button type="submit" class="form-control btn btn-primary">ค้นหา</button>
            </div>
        </div>
    </form>
    @if (collect($receipts)->isEmpty())
        <div class="card">
            <div class="card-body"><h4 class="text-center">ไม่พบข้อมูล</h4></div>
        </div>
              
    @else

    <div class="row">

        <div class="col-lg-3 col-md-3">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
            {{-- <h3>{{number_format(collect($oweInfos)->count())}}</h3> --}}

            <p>จำนวนใบเสร็จ</p>
            </div>
            <div class="icon">
            <i class="ion ion-bag"></i>
            </div>
        </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-md-3">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
            <h3 id="meter_unit_used_sum"></h3>

            <p>ค่าน้ำประปา (บาท)</p>
            </div>
            <div class="icon">
            <i class="ion ion-stats-bars"></i>
            </div>
        </div>
        </div>
        <!-- ./col -->
        
        <div class="col-lg-3 col-md-3">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
            <h3 id="reserve_total_sum"></h3>

            <p>ค่ารักษามิเตอร์ (บาท)</p>
            </div>
            <div class="icon">
            <i class="ion ion-pie-graph"></i>
            </div>
        </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-md-3">
            <!-- small box -->
            <div class="small-box bg-primary">
                <div class="inner text-right">
                <h3 id="all_total_sum"></h3>

                <p class="text-right">รวมทั้งสิ้น (บาท)</p>
                </div>
                <div class="icon">
                <i class="ion ion-pie-graph"></i>
                </div>
            </div>
            </div>
            <!-- ./col -->
    </div>
       
        <div class="card">
            <div class="card-header">
                <div class="card-title"></div>
                <div class="card-tools">
                    <button class="btn btn-primary" id="printBtn">ปริ้น</button>
                    <button class="btn btn-success" id="excelBtn">Excel</button>
                </div>
            </div>
            <div class="card-body table-responsive">
                <div id="DivIdToExport">
                    <table id="oweTable" class="table text-nowrap" width="100%">
                        <thead>
                            <tr>
                                <th colspan="11" class="text-center">
                                    รายงานรายรับค่าน้ำประปา ประจำวันที่ {{$date}}
                                </th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>เลขใบเสร็จรับเงิน</th>
                                <th>ชื่อ-สกุล</th>
                                <th>บ้านเลขที่</th>
                                <th>หมู่</th>
                                <th>เส้นทาง</th>
                                <th>เลขใบแจ้งหนี้</th>
                                <th>รอบบิล</th>
                                <th>ค่าน้ำประปา (บาท)</th>
                                <th>ค่ารักษามิเตอร์ (บาท)</th>
                                <th>รวมทั้งสิน (บาท)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $i = 0;
                                $reservemeterSum = 0;
                                $totalSum = 0;
                                $diff_meter_priceSum = 0;
                            ?>
            
                            @foreach ($receipts  as $item)
                                <?php  $i++;?>
                                @foreach ($item['invoice'] as $key => $invoice)
                                    <?php 
                                        $diff_meter = $invoice['currentmeter'] - $invoice['lastmeter'];
                                        $diff_meter_price = $diff_meter *8;
                                        $reservemeter =  $diff_meter_price == 0 ? 10 : 0;
                                        $total = $diff_meter_price + $reservemeter;
            
                                        $reservemeterSum += $reservemeter;
                                        $totalSum += $total;
                                        $diff_meter_priceSum += $diff_meter_price;
                                  
                                    ?>
            
                                    @if ($key == 0)
                                        <tr class="tr{{$i}} a" data-id="{{$i}}">
                                            <td class="">{{ $i}}</td>
                                            <td class="text-right">{{ $item['id'] }}</td>
                                            <td class=""> {{ $invoice['user_profile']['name'] }}</td>
                                            <td class="text-right">{{ $invoice['user_profile']['address'] }}</td>
                                            <td class="">{{ $invoice['user_profile']['zone']['zone_name'] }}</td>
                                            <td class="">{{ $invoice['usermeterinfos']['subzone']['subzone_name'] }}</td>
                                            <td class="text-right">{{ $invoice['id'] }}</td>
                                            <td class="text-center">{{ $invoice['invoice_period']['inv_period_name'] }}</td>
                                            <td class="text-right">{{ number_format($diff_meter_price) }}</td>
                                            <td class="text-right reservemeter">{{ number_format($reservemeter) }}</td>
                                            <td class="text-right">{{ number_format($item['net']) }}</td>
                                            
                                        </tr>
                                    @else
                                        <tr class="tr{{$i}} a" data-id="{{$i}}">
                                            <td class="duplicate">{{ $i}}</td>
                                            <td class="text-right duplicate">{{ $item['id'] }}</td>
                                            <td class="duplicate"> {{ $invoice['user_profile']['name'] }}</td>
                                            <td class="text-right duplicate">{{ $invoice['user_profile']['address'] }}</td>
                                            <td class="duplicate">{{ $invoice['user_profile']['zone']['zone_name'] }}</td>
                                            <td class="duplicate">{{ $invoice['usermeterinfos']['subzone']['subzone_name'] }}</td>
                                            <td class="text-right">{{ $invoice['id'] }}</td>
                                            <td class="text-center">{{ $invoice['invoice_period']['inv_period_name'] }}</td>
                                            <td class="text-right">{{ number_format($diff_meter_price) }}</td>
                                            <td class="text-right reservemeter">{{ number_format($reservemeter) }}</td>
                                            <td class="text-right duplicate">0</td><!--{ number_format($item['net']) }} -->
                                            
                                        </tr>
                                    @endif
                                @endforeach
                             
                            @endforeach
                        
                        </tbody>
                        <tfoot>
                            <tr>
                        
                                <th colspan="8" class="text-right">รวม:</th>
                                <th  class="text-right pr-0"></th>
                                <th  class="text-right pr-0"></th>
                                <th  class="text-right pr-0"></th>

                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div><!--card-body-->
        </div>
    @endif
 </div>


 @endsection


@section('script')

<script src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js"></script>

<script src="{{asset('/js/my_script.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true,
        }).datepicker("setDate", new Date());;  //กำหนดเป็นวันปัจุบัน


        //แสดงinitdate
        let invperiodstart = $('#invperiodstart').val();
        let invperiodend = $('#invperiodend').val();
        $('#initdate').text(`วันที่ ${invperiodstart} - ${invperiodend}`);

        //ทำ ข้อมูลใน .duplicate ให้เป้นสีข้าว
        $('.duplicate').css('opacity', 0)
        //ทำแถบสีสลับ tr
        makeTrBg();
    });//document

    $('#oweTable').DataTable({
        "pagingType": "listbox",
        "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "ทั้งหมด"]],
        "language": {
            "search": "ค้นหา:",
            "lengthMenu": "แสดง _MENU_ แถว", 
            "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
            "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
            "paginate": {
                "info": "แสดง _MENU_ แถว",
            },
            
        },
        columnDefs: [
            { targets: 10, orderable: false }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            var nf = new Intl.NumberFormat();

            // แถวค่าน้ำ
            total_water_price = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {  
                    
                    return intVal(a) + intVal(b);
                }, 0 );
            
            // Total over this page
            pageTotal_water_price = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 8 ).footer() ).html(
                ' ( ทั้งหมด: '+nf.format(total_water_price) +' )'
                // nf.format(pageTotal_water_price) +' ( ทั้งหมด: '+nf.format(total_water_price) +' )'
            );

            // แถวค่ารักษามิเตอร์
          
            total_reserve_meter_price = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal_reserve_meter_price = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 9 ).footer() ).html(
                ' ( ทั้งหมด: '+nf.format(total_reserve_meter_price) +' )'
                // nf.format(pageTotal_reserve_meter_price) +' ( ทั้งหมด: '+nf.format(total_reserve_meter_price) +' )'
            );

            // แถวรวมทั้งสิ้น
            total_all = api
                .column( 10 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal_all = api
                .column( 10, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
            
            id = api
                .column( 0, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    console.log('id a= ', intVal(a) +' b='+intVal(b))
                },0);//id
                
                let valsum =  intVal(a) + intVal(b);
                
                return valsum;
                }, 0 );
 
            // Update footer
            $( api.column( 10 ).footer() ).html(
                // nf.format(pageTotal_all) +' ( ทั้งหมด: '+nf.format(total_all) +' )'
                ' ( ทั้งหมด: '+nf.format(total_all) +' )'
            );
        }
        //
    });//oweTable

    $(document).ready(function(){
        $('.paginate_page').text('หน้า')
        let val = $('.paginate_of').text()
        $('.paginate_of').text(val.replace('of', 'จาก')); 

        //เอาค่าผลรวมไปแสดงตารางบนสุด
        $('#meter_unit_used_sum').html($('.meter_unit_used_sum').val())                     
        $('#owe_total_sum').html($('.owe_total_sum').val())                      
        $('#reserve_total_sum').html($('.reserve_total_sum').val())                       
        $('#all_total_sum').html($('.all_total_sum').val())
    })//document
 
    $('#printBtn').click(function(){
        var tagid = 'oweTable'
        var hashid = "#"+ tagid;
        var tagname =  $(hashid).prop("tagName").toLowerCase() ;
        var attributes = ""; 
        var attrs = document.getElementById(tagid).attributes;
            $.each(attrs,function(i,elem){
            attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
            })
        var divToPrint= $(hashid).html() ;
        var head = "<html><head>"+ $("head").html() + "</head>" ;
        var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write(allcontent);
        newWin.document.close();
        setTimeout(function(){newWin.close();},10);
        })
    
        $('#excelBtn').click(function(){
            $('.duplicate').text('')

            $("#DivIdToExport").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    });

    $('#oweTable_length select').change(function(){
        $('.duplicate').css('opacity', 0)
        makeTrBg();
    });

    function makeTrBg(){
        $('tr.a').each(function(){
            let id = $(this).data('id')
            if(id%2 == 0){
                $(this).css('background', 'rgba(0,0,0,.05)')
            }
        })
    }
    
</script>
@endsection
