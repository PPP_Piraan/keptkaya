@extends('layouts.adminlte')

@section('mainheader')
ข้อมูลผู้ใช้น้ำประปา
@endsection
@section('nav')
<a href="{{url('/reports')}}"> รายงาน</a>
@endsection
@section('report-users')
active
@endsection
@section('style')
<style href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"></style>
    <style>
        th,
        td {
        white-space: nowrap;
        }
        .hide{
            display: none
        }
        .username{
            color: blue;
            cursor: pointer;
            /* padding-left: 20px */
        }
    </style>
@endsection

@section('content')
 
    <div class="row">
        <div class="col-md-2">
            <label class="control-label">หมู่ที่</label>
            <select class="form-control" name="zone_id" id="zone_id">
                <option value="all" selected>ทั้งหมด</option>
                @foreach ($zones as $zone)
                <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <label class="control-label">เส้นทาง</label>
            <select class="form-control" name="subzone_id" id="subzone_id"></select>
        </div>

        <div class="col-md-2">
            <label class="control-label">&nbsp;</label>
            <button type="button" class="form-control btn btn-primary searchBtn">ค้นหา</button>
        </div>
    </div>
    <div class="card mt-2">
        <div class="card-header">
            <div class="card-title"></div>
            <div class="card-tools">
                    <button class="btn btn-primary" id="printBtn">ปริ้น</button>
                    <button class="btn btn-success" id="excelbtn2">Excel</button>
            </div>
        </div>
        <div class="card-body">
            <div class="p-0  pb-3">
                <table id="example" class="display " width="100%"></table>
            </div>
        </div>
    </div>

@endsection


@section('script')
<script src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js"></script>

    <script src="{{asset('/js/my_script.js')}}"></script>
    
    <script>  
    let table;  
        
      $(document).ready(function(){
        getUsers('all');
      })

      $('#zone_id').change(function(){
          let text = '';
          let zone_id = $(this).val();
          if(zone_id == 'all'){
            text += `<option value="all" selected>ทั้งหมด</option>`;
            $('#subzone_id').html(text)
          }else{
            $.get(`../../api/subzone/${zone_id}`).done(function(data){
                if(data.length > 1){
                    text += `<option value="all" selected>ทั้งหมด</option>`;
                }
                data.forEach(element => {
                   text +=  `<option value="${element.id}">${element.subzone_name}</option>`;
                });
                $('#subzone_id').html(text) 
            });
          }
      });

      $('.searchBtn').click(function(){
          let subzone = $('#subzone_id').val();
          console.log(subzone)
          getUsers(subzone);
      })

    function getUsers(subzone){
        $.get(`../../api/users/report_by_subzone/${subzone}`).done(function(data){
            // console.log('data',data)
            $('#example').html(`
                <tr class="overlay_tr">
                    <td>
                        <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div></div>
                    </td>
                </tr>
            `);


        // $.get('api/users').done(function(data){
         
        table =   $('#example').DataTable( {
            destroy: true,
            "pagingType": "listbox",
            "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "ทั้งหมด"]],
            "language": {
                "search": "ค้นหา:",
                "lengthMenu": "แสดง _MENU_ แถว", 
                "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
                "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
                "paginate": {
                    "info": "แสดง _MENU_ แถว",
                },
            },
                data: data,
                columns: [
                    {
                        'title': '',
                        data: 'user_id',
                        render: function(data){ 
                            return `<i class="fa fa-plus-circle text-success findInfo" 
                                        data-user_id="${data}"></i>`
                            }
                    },
                    {'title': 'เลขผู้ใช้งาน', data: 'user_id_str'},
                    {'title': 'ชื่อ-สกุล', data: 'name'},
                    {'title': 'เลขมิเตอร์', data: 'meternumber'},
                    {'title': 'บ้านเลขที่', data: 'address'},
                    {'title': 'หมู่ที่', data: 'zone_name'},
                    {'title': 'เส้นทาง', data: 'subzone_name'},
                     

              ]
            });
            // $('.overlay').css('display', 'none');
            $('#example').find('.overlay_tr').remove()
            $('#example thead').find('#title').remove()
            let title = 'ตารางข้อมูลผู้ใช้น้ำประปาบ้านห้องแซง';
            title += $('#zone_id').val() === 'all' ? 'หมู่ที่ 1 - 19' :data[0].zone_name;
            // $('#example thead').first().remove()
            $('#example thead').prepend(`<tr id="title"><td colspan="7" class="text-center h4">${title}</td></tr>`)

            $('.paginate_page').text('หน้า')
            let val = $('.paginate_of').text()
            $('.paginate_of').text(val.replace('of', 'จาก')); 
        });
        
      }

    $('body').on('click','.findInfo', function(){
        let user_id = $(this).data('user_id')
        
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        console.log('row', row)
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            //หาข้อมูลการชำระค่าน้ำประปาของ  user
        $.get(`../../api/users/user/${user_id}`).done(function(data){
           row.child( format(data)).show();

            tr.addClass('shown');
        });
            
        }
        if($(this).hasClass('fa-plus-circle')){
            $(this).removeClass('fa-plus-circle')
            $(this).removeClass('text-success')
            $(this).addClass('fa-minus-circle')
            $(this).addClass('text-danger')

            // aa(user_id, tr)

        }else{
            $(this).addClass('fa-plus-circle')
            $(this).addClass('text-success')
            $(this).removeClass('fa-minus-circle')
            $(this).removeClass('text-danger');
        }
        
    });

    function aa(user_id, tr){
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            //หาข้อมูลการชำระค่าน้ำประปาของ  user
        $.get(`../../api/users/user/${user_id}`).done(function(data){
            // console.log('xx',format(data))
            row.child( format(data)).show();
            tr.addClass('shown');
        });
            
        }

    }

    $('#printBtn').click(function(){
        var tagid = 'example'
        var hashid = "#"+ tagid;
    var tagname =  $(hashid).prop("tagName").toLowerCase() ;
    var attributes = ""; 
    var attrs = document.getElementById(tagid).attributes;
        $.each(attrs,function(i,elem){
        attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
        })
    var divToPrint= $(hashid).html() ;
    var head = "<html><head>"+ $("head").html() + "</head>" ;
    var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write(allcontent);
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
    })
    $('#excelbtn2').click(function(){
        $("#example").table2excel({
        // exclude CSS class
        exclude: ".noExl",
        name: "Worksheet Name",
        filename: 'aa', //do not include extension
        fileext: ".xls" // file extension
    })
    })

    </script>
@endsection