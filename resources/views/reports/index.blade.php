@extends('layouts.adminlte')

@section('mainheader')
รายงาน
@endsection

@section('report-users')
    active
@endsection
@section('style')
    <style>
        th,
        td {
        white-space: nowrap;
        }

        div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
        }

        /* td:first-child {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
        }

        tr.shown td:first-child {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        } */
        .hide{
            display: none
        }
    </style>
@endsection

@section('content')
    <form action="{{url('reports/search')}}" method="get">
        {{-- @csrf --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-bottom">ค้นหาการชำระค่าน้ำประปา</div>
                    <div class="card card-body">
                        <div class="row">
                            {{-- <div class="col-md-1">
                                <label class="control-label">รอบบิลที่</label>
                                <select class="form-control" name="inv_period" id="inv_period">
                                    @foreach ($inv_periods as $inv_period)
                                        <option value="{{$inv_period->id}}">{{ $inv_period->inv_period_name }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="col-md-2">
                                <label class="control-label">หมู่ที่</label>
                                <select class="form-control" name="zone_id" id="zone_id">
                                    <option value="all" selected>ทั้งหมด</option>
                                    @foreach ($zones as $zone)
                                        <option value="{{$zone->id}}">{{$zone->zone_name}}</option>   
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">ประเภท</label>
                                <select class="form-control" name="invoicetype" id="invoicetype">
                                    <option value="all" selected>ทั้งหมด</option>
                                    <option value="owe">ค้างชำระ</option>
                                    <option value="paid">ชำระแล้ว</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">รอบบิล</label>
                                <input class="form-control datepicker" type="text" name="invperiodstart" id="invperiodstart">
                            </div>
                            <div class="col-md-1 text-center">
                                <label class="control-label">&nbsp;</label>
                                <input class="form-control text-center border-0 font-weight-bold" type="text" value="ถึง">

                            </div>
                            <div class="col-md-2">
                                <label class="control-label">รอบบิล</label>
                                <input class="form-control datepicker" type="text" name="invperiodend" id="invperiodend">
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">&nbsp;</label>
                                <button type="submit" class="form-control btn btn-primary">ค้นหา</button>
                            </div>
                        </div>
                        <div class="p-0 mt-5 pb-3 text-center">
                            {{-- <div class="alert alert-success search_res_div hidden ">
                                    <strong>ผลการค้นหา </strong> <span class="search_res"></span> 
                            </div> --}}
                            {{-- <table class="table mb-0 datatable">
                                <thead class="bg-light">
                                    <tr>
                                            <th>view</th>
                                            <th scope="col-md-1" class="border-0">#</th>
                                            <th scope="col-md-2" class="border-0">ชื่อผู้ประปา</th>
                                            <th scope="col-md-2" class="border-0">รอบบิลที่</th>
                                            <th scope="col-md-2" class="border-0">สถานะ</th>
                                    </tr>
                                </thead>
                                <tbody id="info">
                                    @foreach ($invs as $item)
                                    <tr>
                                        <td class="details-control">view</td>
                                        <td scope="col-md-1" class="border-0">ll</td>
                                        <td scope="col-md-2" class="border-0">ชื่อผู้ประปา</td>
                                        <td scope="col-md-2" class="border-0">รอบบิลที่</td>
                                        <td scope="col-md-2" class="border-0">สถานะ</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table> --}}
                            <input type="button" id="btnExport" value=" Export Table data into Excel " />
                            <div id="text">
                                <table id="example" class="stripe row-border order-column" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>First name</th>
                                        <th>Last name</th>
                                        <th>Position</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Start date</th>
                                        <th>Salary</th>
                                        <th>Extn.</th>
                                        <th>E-mail</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="aa">
                                        <td></td>
                                        <td>Tiger</td>
                                        <td>Nixon</td>
                                        <td>System Architect</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td>2011/04/25</td>
                                        <td>$320,800</td>
                                        <td>5421</td>
                                        <td>t.nixon@datatables.net</td>
                                    </tr>
                                    
                                    <tr class="aa">
                                        <td></td>
                                        <td>Garrett</td>
                                        <td>Winters</td>
                                        <td>Accountant</td>
                                        <td>Tokyo</td>
                                        <td>63</td>
                                        <td>2011/07/25</td>
                                        <td>$170,750</td>
                                        <td>8422</td>
                                        <td>g.winters@datatables.net</td>
                                    </tr>
            
                                    <tr class="aa">
                                        <td></td>
                                        <td>Donna</td>
                                        <td>Snider</td>
                                        <td>Customer Support</td>
                                        <td>New York</td>
                                        <td>27</td>
                                        <td>2011/01/25</td>
                                        <td>$112,000</td>
                                        <td>4226</td>
                                        <td>d.snider@datatables.net</td>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

{{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        
        <script src="{{asset('/js/pdfmake.min.js')}}"></script>
        <script src="{{asset('/js/vfs_fonts.js')}}"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
        <script>
            pdfMake.fonts = {
                Roboto:{
                    normal:"THSarabun.ttf",
                    bold:"THSarabun.ttf",
                    italics:"THSarabun.ttf",
                    bolditalics:"THSarabun.ttf"  //ใช้ THSarabun.tff
                }
            };
            $(document).ready(() => {
                $('.datatable').DataTable({
                    "searching": false,
                    "paging": false,
                    "bInfo" : false,
                    dom: 'Bfrtip',
                    buttons: [
                        // {extend: 'copy',text: 'Copy'},
                        // {extend: 'csv',text: 'Export CSV'},
                        {extend: 'excel',text: 'Export Excel'},
                        {
                            extend: 'pdf',
                            text: 'Export PDF',
                        },        
                        {extend: 'print',text: 'Print'}
                    ],
                    "language": {
                        "emptyTable": "ไม่พบข้อมูล"
                    }
                });
                $('.dt-buttons').addClass('m-2')
                $('.dt-button').prop('style', 'border-radius: 20px 20px !important')
            });
            
    </script>
<script>

    function search2(){
        var invoicetype = $('#invoicetype').val();
        console.log(invoicetype)
        if(invoicetype === ""){
            return false
        }
        
        var invperiodstart = $('#invperiodstart').val();
        var invperiodend = $('#invperiodend').val();
        console.log(invperiodstart)
        $.get( "/api/reports/invoicereport", {
            "invoicetype": invoicetype,
            "invperiodstart": invperiodstart,
            "invperiodend": invperiodend,
        })
        .done(function( data ) {
            console.log(  data );
            $('.search_res_div').addClass('hidden');
            var text ="";
            if(Object.keys(data).length > 0){            
                $('.search_res_div').removeClass('hidden');
                $('.search_res').text('พบข้อมูลดังแสดงในตาราง')                
                table = $('.datatable').DataTable();  
                var i = 1;  
                data.forEach(element => {
                    table.row.add([ 
                        i++,
                        element.users.user_profile.name, 
                        element.invoice_period.inv_period_name, 
                        element.status 
                    ]);
                });              
                table.draw();

            }else{
                $('.search_res_div').removeClass('hidden');
                $('.search_res').text = 'ไม่พบข้อมูล'
            }//if
            setTimeout(() => {
                $('.search_res_div').addClass('hidden');
            }, 5000);
        });
        
    }
</script> --}}
@endsection


@section('script')
<script src="{{asset('/js/pdfmake.min.js')}}"></script>
        <script src="{{asset('/js/vfs_fonts.js')}}"></script>
        {{-- <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script> --}}
        {{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}
 <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
 {{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> --}}
 <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
 
<script>
    $(document).ready(function(){
        $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                // thaiyear: true              //Set เป็นปี พ.ศ.
        }).datepicker();  //กำหนดเป็นวันปัจุบัน
    })



    pdfMake.fonts = {
        Roboto:{
            normal:"THSarabun.ttf",
            bold:"THSarabun.ttf",
            italics:"THSarabun.ttf",
            bolditalics:"THSarabun.ttf"  //ใช้ THSarabun.tff
        }
    };
    var table = $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [ {
            extend: 'excelHtml5',
            customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 
                $('row c[r^="C"]', sheet).attr( 's', '2' );
            }
        } ]
    });

    $(document).ready(() => {
        $('.datatable').DataTable({
            "searching": false,
            "paging": false,
            "bInfo" : false,
            dom: 'Bfrtip',
            buttons: [
                // {extend: 'copy',text: 'Copy'},
                // {extend: 'csv',text: 'Export CSV'},
                {extend: 'excel',text: 'Export Excel'},
                {
                    extend: 'pdf',
                    text: 'Export PDF',
                },        
                {extend: 'print',text: 'Print'}
            ],
            "language": {
                "emptyTable": "ไม่พบข้อมูล"
            }
        });
        $('.dt-buttons').addClass('m-2')
        $('.dt-button').prop('style', 'border-radius: 20px 20px !important')


        let i = 0;
        $('tr.aa').each(function(){
            let a  = i++; 
            let t = `
                    <tr><td colspan="10">sss${a}</td></tr>   `
            var tr = $(this).closest('tr');
            var row = table.row( tr );

                    
            row.child(t).show();
        })
    });

    
    $('#example tbody').on('click', 'td:first-child', function () {
    var tr = $(this).closest('tr');
    var row = table.row( tr );

    if (row.child.isShown()) {
        // This row is already open - close it.
        row.child.hide();
        tr.removeClass('shown');
    } else {
        // Open row.
        
        
        row.child(text).show();
        tr.addClass('shown');
    }
    });

    $("#btnExport").click(function (e) {
        $("#example").table2excel({
    exclude: ".excludeThisClass",
    name: "Worksheet Name",
    filename: "SomeFile.xls", // do include extension
    preserveColors: false // set to true if you want background colors and font colors preserved
});
});
            
    </script>
@endsection
