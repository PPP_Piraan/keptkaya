@extends('layouts.adminlte')
@section('mainheader')
ประวัติจดมาตรวัดน้ำ(ป.31)
@endsection
@section('nav')
    <a href="{{'reports'}}">รายงาน</a>
@endsection
@section('report-meter_record_history')
    active
@endsection

@section('style')

    <style>
        .hidden{
            display: none !important
        }
        .dtable-container {
    max-width: 100% !important;
        }

    table {
        white-space: nowrap !important;
        width:100%!important;
        border-collapse:collapse!important;
    }
}

      
    </style>
    <link href="{{url('css/mycss.css')}}">
@endsection

@section('content')
  {{-- ฟอร์มค้นหา --}}
    <div class="card">
        <div class="card-header border-bottom"></div>
        <div class="card card-body">
            <form action="{{url('reports/meter_record_history')}}" method="GET">
                @csrf
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">ปีงบประมาณ</label>                        
                        <select class="form-control" name="budgetyear" id="budgetyear">
                            <option value="">เลือก</option>
                            @foreach ($budgetyears as $_budgetyear)
                                <option value="{{$_budgetyear->id}}" {{$_budgetyear->id == $current_budget_year->id ? 'selected' : ''}}>{{$_budgetyear->budgetyear}}</option>   
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">หมู่ที่</label>
                        <select class="form-control" name="zone_id" id="zone_id">
                            <option value="all" selected>ทั้งหมด</option>
                            @foreach ($zones as $zone)
                                <option value="{{$zone->id}}">{{$zone->zone_name}}</option>   
                            @endforeach
                        </select>
                    </div>
                    
                    
                    <div class="col-md-2">
                        <label class="control-label">&nbsp;</label>
                        <button type="submit" class="form-control btn btn-primary search-btn">ค้นหา</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
            <div class="card-title"></div>
            <div class="card-tools">
                <button class="btn btn-primary" id="printBtn">ปริ้น</button>
                <button class="btn btn-success" id="excelBtn">Excel</button>
            </div>
        </div>
        <div class="card-body ">
            <div id="DivIdToExport" >
                <table id="oweTable" class="table table-responsive">
                    <thead>
                    
                        <tr>
                            <th>เลขมิเตอร์</th>
                            <th>ชื่อผู้ใช้น้ำ</th>
                            <th >ที่อยู่</th>
                            <th >หมู่</th>
                            <th>เส้นทาง</th>
                            @foreach ($inv_period_lists as $item)
                            <th class="text-center">รอบบิล  <div>{{ $item['inv_period_name'] }}</div></th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($infos as $item)
                        <tr>
                            <td class="text-center">{{ $item['meternumber'] }}</td>
                            <td class="text-wrap text-left">{{ $item['user_profile']['name'] }}</td>
                            <td class="text-center">{{ $item['user_profile']['address'] }}</td>
                            <td>{{ $item['zone']['zone_name'] }}</td>
                            <td>{{ $item['subzone']['subzone_name'] }}</td>
                            @foreach ($inv_period_lists as $inv_period)
                            
                                <td class="text-right">
                                    <?php
                                        $inv_p = $inv_period['id'];
                                        $res = collect($item['invoice'])->filter(function($v) use ($inv_p){ return $v['inv_period_id'] == $inv_p; })->values();
                                        // dd($res);
                                        if(collect($res)->count() == 1){
                                            echo number_format($res[0]['currentmeter']);
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                   
                                </td>
                            @endforeach
                           
                          

                        </tr>
                        @endforeach
                        
                </table>
            </div>

            <!--DivIdToExport-->
            
        </div>
        <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>

    </div>



@endsection


@section('script')
    
<script src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js"></script>
<script>
     
    var table;
    let col_index = -1
    var a = true;

    $(document).ready(() => {
        getData()
    });

    

    function getData(budgetyear= 'now', zone_id = 'all'){
        console.log('b'+budgetyear+"  z"+zone_id)
        // $.get(`../api/reports/meter_record_history/${budgetyear}/${zone_id}`, function(res){
            // $('#DivIdToExport').html(res)
            table = $('#oweTable').removeAttr('width').DataTable({
                "pagingType": "listbox",
                responsive: true,
                "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "ทั้งหมด"]],
                "language": {
                    "search": "ค้นหา:",
                    "lengthMenu": "แสดง _MENU_ แถว", 
                    "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
                    "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
                    "paginate": {
                        "info": "แสดง _MENU_ แถว",
                    },
                   
                },
             
            });//oweTable
            
             if(a){
                $('#oweTable thead tr').clone().appendTo('#oweTable thead');
                a= false
            }
           
                $('#oweTable thead tr:eq(1) th').each( function (index) {
                    var title = $(this).text();
                    $(this).removeClass('sorting')
                    $(this).removeClass('sorting_asc')
                    if(index < 5){
                        $(this).html( `<input type="text" data-id="${index}" class="form-control" id="search_col_${index}" placeholder="ค้นหา" />` );
                    }else{
                        $(this).html('')
                    }
                }); 

            $('#oweTable_filter').remove()

                //custom การค้นหา
            

            $('#oweTable thead input[type="text"]').focus(function(){
                var col = parseInt($(this).data('id'))
                console.log('col= '+col+"  colindex="+ col_index)
                if(col !==  col_index && col_index >= 0){
                        $('input[type="text"]').val('')

                }
                col_index = col
            })
            setTimeout(()=>{
                $('.overlay').remove()  
            }, 2000)
           

        // });//get
    }
     $(document).on('keyup', 'input[type="text"]',function(){
                let that = $(this)
                var col = parseInt(that.data('id'))
                
                var _val = that.val()
                // if(col === 1){
                //     var val = $.fn.dataTable.util.escapeRegex(
                //         _val
                //     );
                //     table.column(col)
                //     .search( val ? '^'+val+'.*$' : '', true, false )
                //     .draw();
                // }else{
                    table.column(col)
                    .search( _val )
                    .draw();
                // }
                col_index = col
            })
    // $('.search-btn').click(()=>{
    //     let budgetyear = $('#budgetyear').val();
    //     let zone_id    = $('#zone_id').val();
    //     $('.datatable').DataTable().destroy();
    //     getData(budgetyear, zone_id)
    // })
    
    $('#printBtn').click(function(){
        // let texttitle = `<tr id="texttitle"><th colspan="9">รายงานผู้ค้างชำระค่าน้ำประปา</th></tr>`    
            $('#oweTable thead tr:eq(1)').addClass('hidden')
            
            // $('#oweTable thead').prepend(texttitle)
            var tagid = 'oweTable'
            var hashid = "#" + tagid;
            var tagname = $(hashid).prop("tagName").toLowerCase();
            var attributes = "";
            var attrs = document.getElementById(tagid).attributes;
            $.each(attrs, function (i, elem) {
                attributes += " " + elem.name + " ='" + elem.value + "' ";
            })
            var divToPrint = $(hashid).html();
            var head = '<html><head>' 
                + $("head").html() 
                + ' <style>body{background-color:white !important;}'
                +'@page { size: landscape;}@media print {td,th {font-size:15px}}'
                +'</style></head>';
            var allcontent = head + "<body  onload='window.print()' >" + "<" + tagname + attributes + ">" +
                divToPrint + "</" + tagname + ">" + "</body></html>";
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write(allcontent);
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
                // $('#texttitle').remove()

                $('#oweTable thead tr:eq(1)').removeClass('hidden')

            }, 220);
        })
    
        $('#excelBtn').click(function(){

            $("#DivIdToExport").table2excel({
            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: 'aa', //do not include extension
            fileext: ".xls" // file extension
        })
    });    
   
      
</script>
@endsection
