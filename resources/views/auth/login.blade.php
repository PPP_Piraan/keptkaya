@extends('layouts.login')

@section('content')
{{-- <div class="text-right">
    <a href="{{url('/')}}" class="text-white mr-3">Home</a>
</div> --}}
        <!-- LOGIN MODULE -->
        <div class="login">
            <div class="wrap">
                <!-- TOGGLE -->
                <div id="toggle-wrap">
                    <div id="toggle-terms">
                        <div id="cross">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
              
                <!-- RECOVERY -->
                <div class="recovery">
                    <h2>Password Recovery</h2>
                    <p>Enter either the <strong>email address</strong> or <strong>username</strong> on the account and <strong>click Submit</strong></p>
                    <p>We'll email instructions on how to reset your password.</p>
                    <form class="recovery-form" action="{{route('login')}}" method="post">
                        <input type="text" class="input" id="user_recover" placeholder="Enter Email or Username Here">
                        <input type="submit" class="button" value="Submit">
                    </form>
                    <p class="mssg">An email has been sent to you with further instructions.</p>
                </div>

                <!-- SLIDER -->
                <div class="content">
                    <!-- LOGO -->
                    <div class="logo">
                        <a href="#"><img src="https://res.cloudinary.com/dpcloudinary/image/upload/v1506186248/logo.png" alt=""></a>
                        
                    </div>
                    <!-- SLIDESHOW -->
                    <div id="slideshow">
                        <div class="one" style="width: 100%">
                            <h2><span>งานประปา</span></h2>
                            {{-- <p>Sign up to attend any of hundreds of events nationwide</p> --}}
                        </div>
                        <div class="two">
                            <h2><span>เทศบาล</span></h2>
                            {{-- <p>Thousands of instant online classes/tutorials included</p> --}}
                        </div>
                        <div class="three">
                            <h2><span>ตำบลห้องแซง</span></h2>
                            {{-- <p>Create your own groups and connect with others that share your interests</p> --}}
                        </div>
                    
                    </div>
                </div>
              
                <div class="user">
                    <!-- ACTIONS
                    <div class="actions">
                        <a class="help" href="#signup-tab-content">Sign Up</a><a class="faq" href="#login-tab-content">Login</a>
                    </div>
                    -->

                    <div class="form-wrap">
                        <!-- TABS -->
                    	<div class="tabs">
                            <h3 class="login-tab"><a class="log-in active" href="#login-tab-content"><span>Login<span></a></h3>
                    		{{-- <h3 class="signup-tab"><a class="sign-up" href="#signup-tab-content"><span>Sign Up</span></a></h3> --}}
                    	</div>
                        <!-- TABS CONTENT -->
                    	<div class="tabs-content">
                            <!-- TABS CONTENT LOGIN -->
                    		<div id="login-tab-content" class="active">
                    			<form class="login-form" action="{{route('login')}}" method="POST">
                                    @csrf
                                    {{-- <input id="email" type="username"  class="input @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}"  --}}
                                    <input id="email" type="username"  class="input @error('username') is-invalid @enderror" name="username" value="" 
                                   
                                    {{-- <input id="email" type="email"  class="input @error('password') is-invalid @enderror" name="email" value="{{ old('email') }}"  --}}
                                        placeholder="" required autocomplete="email" autofocus>
                                    <input id="password" type="password"  class="input @error('password') is-invalid @enderror" name="password"  value=""
                                        placeholder="" required autocomplete="current-password">
                    				
                    				{{-- <input type="checkbox" class="checkbox" checked id="remember_me">
                    				<label for="remember_me">Remember me</label> --}}
                    				<input type="submit" class="button" value="Login">
                    			</form>
                    			<div class="help-action">
                    				{{-- <p><i class="fa fa-arrow-left" aria-hidden="true"></i><a class="forgot" href="#">Forgot your password?</a></p> --}}
                    			</div>
                    		</div>
                            <!-- TABS CONTENT SIGNUP -->
                    		<div id="signup-tab-content">
                    			{{-- <form class="signup-form" action="" method="post">
                    				<input type="email" class="input" id="user_email" autocomplete="off" placeholder="Email">
                    				<input type="text" class="input" id="user_name" autocomplete="off" placeholder="Username">
                    				<input type="password" class="input" id="user_pass" autocomplete="off" placeholder="Password">
                    				<input type="submit" class="button" value="Sign Up">
                    			</form> --}}
                    			<div class="help-action">
                    				<p>By signing up, you agree to our</p>
                    				<p><i class="fa fa-arrow-left" aria-hidden="true"></i><a class="agree" href="#">Terms of service</a></p>
                    			</div>
                    		</div>
                    	</div>
                	</div>
                </div>
            </div>
        </div>


@endsection
