@extends('layouts.adminlte')

@section('mainheader')
    พื้นที่จดมิเตอร์น้ำประปา
@endsection
@section('zone')
    active
@endsection
@section('nav')
<a href="{{url('/users')}}"> พื้นที่จดมิเตอร์น้ำประปา</a>
@endsection
@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-{{Session::get('color')}} alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif
      <a href="{{url('zone/create')}}" class="btn btn-primary my-4 col-2 "><i class="fas fa-plus-circle"></i>สร้างพื้นที่จดมิเตอร์</a>
          
        @if (count($zone) == 0)
        <div class="col-md-5 col-sm-6 col-12">
          <div class="info-box bg-warning">
            <span class="info-box-icon"><i class="fas fa-comments"></i></span>
  
            <div class="info-box-content">
              {{-- <span class="info-box-text">Events</span> --}}
              <span class="info-box-number text-center font-weight-bold">ไม่พบข้อมูลการพื้นที่จดมิเตอร์น้ำประปา</span>
  
              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
              <span class="progress-description text-center">
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
       
        @else
            <div class="table-responsive">
              <div class="row">
                @foreach ($zone as $item)
                  <div class="col-4">
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title"> {{$item->zone_name}}</h3>
        
                        <div class="card-tools">
                          <a  href="{{url('subzone/edit/'.$item->id)}}" class="btn btn-info btn-sm mr-1" placeholder="เพิ่มเส้นทาง">เพิ่มเส้นทาง</button>
                            
                          <a href="{{url('subzone/edit/'.$item->id)}}" class="btn btn-warning btn-sm" >แก้ไข</a>
                          <a href="#" data-zone_id={{$item->id}} class="btn btn-danger btn-sm delbtn" >ลบ</a>
                          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                        <!-- /.card-tools -->
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                        <ul>
                        @foreach ($item->subzone as $subzone)
                            <li><span class="text-primary"><i class="fas fa-road"></i></span> {{$subzone['subzone_name']}}</li>
                        @endforeach
                        </ul>
                      </div>
                      <!-- /.card-body -->
                    </div>  
                  </div>  
                @endforeach
              </div>
                
            </div>
        @endif
        
      
      {{-- </div>
    </div>
  </div> --}}

  
@endsection
  
@section('script')
    <script>
      $('.delbtn').click(function(){
        //หา ว่า subzone มีใน user_meter_infos table ไหม
        var res = false
        var zone_id = $(this).data('zone_id');
         
         
         $.get(`user_meter_infos/find_subzone/${zone_id}`, function(data){
          if(data == 1){
            alert(`ไม่สามารถลบหมู่ ${zone_id} ได้เนื่องจากมีข้อมูลสมาชิกในหมู่นี้อยู่`)
          }else{

            $.get('../../api/zone/delete/'+zone_id, function(data){
              console.log(data)
              if(data == 1){
                alert('ทำการลบข้อมูลเรียบร้อยแล้ว')
                location.reload();
              }
            })
            
          }

        })
        
      });

      $(document).ready(()=>{
        setTimeout(()=>{
          $('.alert').toggle('slow')
        },2000) 
      })
    </script>
@endsection