@extends('layouts.adminlte')

@section('mainheader')
แก้ไขข้อมูลประปา <span id="undertake_zone"></span> [รอบบิล <span id="inv_period_name"></span>]
@endsection
@section('invoice')
    active
@endsection
@section('nav')
<a href="{{url('invoice/index')}}">ออกใบแจ้งหนี้</a>
@endsection


@section('content')
@if ($message = Session::get('massage'))
<div class="alert alert-{{Session::get('color')}} alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif
{{-- <div class="card  table-responsive">
    <div class="card-body ">
        <div  id="has_invoice">
            <form action="{{url('invoice/zone_update/'.$zoneInfo[0]->undertake_zone_id)}}" method="POST">
                @csrf
                @method("PUT")
                    <input type="submit" class="btn btn-primary mb-3 col-2" id="print_multi_inv"
                        value="บันทึก">

                    <table class="table mb-0 mt-3  table-striped dataTable text-nowrap">
                        <thead class="bg-light">
                            <tr>
                                <th class="text-center">เลขใบแจ้งหนี้</th>
                                <th class="text-center">เลขมิเตอร์</th>
                                <th class="text-center">ชื่อ-สกุล</th>
                                <th class="text-center">บ้านเลขที่</th>
                                <th class="text-center">ยกยอดมา</th>
                                <th class="text-center">มิเตอร์ปัจจุบัน</th>
                                <th class="text-center">จำนวนสุทธิ</th>
                                <th class="text-center">เป็นเงิน</th>
                                <th class="text-center">หมายเหตุ</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody id="app">
                            @foreach ($memberHasInvoice as $invoice)
                                <tr data-id="{{$invoice->id}}" class="data">
                                   
                                    <td class="border-0">
                                        <input type="text" value="{{$invoice->id}}" name="zone[{{$invoice->id}}][iv_id]" data-id="{{$invoice->id}}" 
                                        id="iv_id{{$invoice->id}}" class="form-control text-right iv_id" readonly>
                                    </td>
                                    <td class="border-0">
                                        <input type="text" value="{{$invoice->meternumber}}" name="zone[{{$invoice->id}}][meternumber]" 
                                        data-id="{{$invoice->id}}" id="meternumber{{$invoice->meternumber}}" class="form-control text-right meternumber border-primary" readonly>
                                        <input type="hidden" value="{{$invoice->meternumber}}" name="zone[{{$invoice->id}}][meter_id]">
                                    </td>
                                    <td class="border-0 text-left">
                                        @if ($invoice->name != null)
                                        {{$invoice->name}}
                                        @endif
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="{{$invoice->address}}" name="zone[{{$invoice->id}}][address]">
                                    </td>
                                    <td class="border-0">
                                        <input type="text" value="{{$invoice->lastmeter}}" name="zone[{{$invoice->id}}][lastmeter]" data-id="{{$invoice->id}}" 
                                        id="lastmeter{{$invoice->id}}" class="form-control text-right lastmeter" readonly>
                                    </td>
                                    <td class="border-0 text-right">
                                        <input type="text" value="{{$invoice->currentmeter}}" name="zone[{{$invoice->id}}][currentmeter]" data-id="{{$invoice->id}}" id="currentmeter{{$invoice->id}}" class="form-control text-right currentmeter">
                                    </td>
                                    <td class="border-0 text-right">
                                        <input type="text" readonly class="form-control text-right water_used_net"  id="water_used_net{{$invoice->id}}" value="{{$invoice->meter_net}}">      
                                    </td>
                                    <td class="border-0 text-right">
                                        <input type="text" readonly class="form-control text-right total" id="total{{$invoice->id}}" value="{{$invoice->total}}">
                                    </td>
                                    
                                    <td class="border-0">
                                        <input type="text" name="zone[{{$invoice->id}}][comment]"  value="{{$invoice->comment}}"
                                            data-id="{{$invoice->id}}" id="comment{{$invoice->id}}" class="form-control  comment">
                                    </td>
                                    <td class="border-0">
                                        <a href="javascript:void(0)" class="btn btn-danger delBtn" data-del_invoice_id="{{$invoice->id}}">ลบ</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </form>
        </div>
    </div>
</div> --}}

<div class="card">
    <div class="card-body">
        <form action="{{url('invoice/zone_update/'.$subzone_id)}}" method="POST">
            @csrf
            @method("PUT")
                <input type="submit" class="btn btn-primary mb-3 col-2" id="print_multi_inv"
                    value="บันทึกการแก้ไข">
                <table id="oweTable" class="table text-nowrap" width="100%"></table>
        </form>
    </div>
    <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>

</div>
@endsection


@section('script')
<script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="{{asset('/js/my_script.js')}}"></script>
<script>
    let i = 0;
    let table;
    let cloneThead = true
    let col_index = -1

     //getข้อมูลจาก api มาแสดงใน datatable
     $(document).ready(function () {
        getOweInfos()
    })

    function getOweInfos() {
            
        $.get(`../../api/invoice/zone_edit/<?php echo $subzone_id;?>`).done(function (data) {
            console.log('data',data)
            $('#inv_period_name').html(data.presentInvoicePeriod)
            $('#undertake_zone').html(data.zoneInfo)
            
            if (data.length === 0) {
                $('.res').html('<div class="card-body h3 text-center">ไม่พบข้อมูล</div>')
            } else {
                table = $('#oweTable').DataTable({
                    responsive: true,
                    // order: false,
                    // searching:false,
                    "pagingType": "listbox",
                    "lengthMenu": [
                        [10, 25, 50, 150, -1],
                        [10, 25, 50, 150, "ทั้งหมด"]
                    ],
                    "language": {
                        "search": "ค้นหา:",
                        "lengthMenu": "แสดง _MENU_ แถว",
                        "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                        "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                        "paginate": {
                            "info": "แสดง _MENU_ แถว",
                        },

                    },
                    data: data.memberHasInvoice,
                    select:false,
                    columns: [
                        {
                            'title': 'เลขใบแจ้งหนี้',
                            data: function(data){
                                return `${data.id}<input type="hidden" value="${data.id}" name="zone[${data.id}][iv_id]" data-id="${data.id}" 
                                    id="iv_id${data.id}" class="form-control text-right iv_id">`
                            },
                            'className': 'text-center'

                        }, {
                            'title': 'เลขที่',
                            data: function(data){
                                return `<center>${data.meternumber}&nbsp;&nbsp;</center><input type="hidden" value="${data.meternumber}" name="zone[${data.id}][meter_id]">`
                                },
                        },
                        {
                            'title': 'ชื่อ-สกุล',
                            data: 'name'
                        },
                        {
                            'title': 'บ้านเลขที่',
                            data: 'address',
                            'className': 'text-center'
                        },
                        {
                            'title': 'ยกยอดมา',
                            data: function(data){
                                return `${data.lastmeter.toLocaleString()}<input type="hidden" value="${data.lastmeter}" name="zone[${data.id}][lastmeter]" data-id="${data.id}" 
                                    id="lastmeter${data.id}" class="form-control text-right lastmeter" readonly>`
                            },
                            'className': 'text-right'
                        },

                        {
                            'title': 'มิเตอร์ปัจจุบัน',
                            data: function(data){
                                return `<input type="text" value="${data.currentmeter}" name="zone[${data.id}][currentmeter]" data-id="${data.id}" id="currentmeter${data.id}"
                                            class="form-control text-right currentmeter">
                                        <input type="hidden" value="0" name="zone[${data.id}][changevalue]" data-id="${data.id}" id="changevalue${data.id}">`
                                
                            },
                            'className': 'text-right'
                        },
                        {
                            'title': 'ใช้น้ำ(หน่วย)',
                            data: function(data){
                                return `<input type="text" readonly class="form-control text-right water_used_net"  id="water_used_net${data.id}" value="${data.meter_net.toLocaleString()}">`
                            }
                        },
                        {
                            'title': 'เป็นเงิน(บาท)',
                            data: function(data){
                                return ` <input type="text" readonly class="form-control text-right total" id="total${data.id}" value="${data.total.toLocaleString()}">`
                            }
                        },
                        {
                            'title': 'หมายเหตุ',
                            data: function(data){
                                let comment = data.comment === null ? "" : data.comment
                                return `<input type="text" name="zone[${data.id}][comment]"  value="${comment}"
                                        data-id="${data.id}" id="comment${data.id}" class="form-control  comment">`
                            }
                        },
                        {
                            'title': '',
                            data: function(data){
                                return `<a href="javascript:void(0)" class="btn btn-danger delBtn" data-del_invoice_id="${data.id}">ลบ</a>`
                            }
                        }

                    ],
                    
                }) //table
                // ทำการ clone thead แล้วสร้าง input text
                if(cloneThead){
                    $('#oweTable thead tr').clone().appendTo('#oweTable thead');
                    cloneThead= false
                }
                $('#oweTable thead tr:eq(1) th').each( function (index) {
                    var title = $(this).text();
                    $(this).removeClass('sorting')
                    $(this).removeClass('sorting_asc')
                    if(index < 4){
                        $(this).html( `<input type="text" data-id="${index}" class="col-md-12" id="search_col_${index}" placeholder="ค้นหา" />` );
                    }else{
                        $(this).html('')
                    }
                } );
            } //else
            $('.overlay').remove()
            $('#oweTable_filter').remove()

            //custom การค้นหา
            
            $('#oweTable thead input[type="text"]').keyup(function(){
                let that = $(this)
                var col = parseInt(that.data('id'))

                if(col !== col_index && col_index !== -1){
                    $('#search_col_'+col_index).val('') 
                    table.column(col_index)
                    .search('')
                    .draw();
                }
                setTimeout(function(){ 
                    
                    let _val = that.val()
                    if(col === 0 || col===3){
                        var val = $.fn.dataTable.util.escapeRegex(
                            _val
                        );
                        table.column(col)
                        .search( val ? '^'+val+'.*$' : '', true, false )
                        .draw();
                    }else{
                        table.column(col)
                        .search( _val )
                        .draw();
                    }
                 }, 300);
     
                col_index = col

            })

        }) //.get

    }//function getOweInfos


    //คำนวนเงินค่าใช้น้ำ
    $(document).on('keyup', '.currentmeter', function(){
        let inv_id = $(this).data('id')
        let currentmeter = $(this).val()
        let lastmeter = $(`#lastmeter${inv_id}`).val()
        let net = currentmeter - lastmeter
        let total = net * 8;
        $('#water_used_net'+inv_id).val(net)
        $('#total'+inv_id).val(total);
        $('#changevalue'+inv_id).val(1)
    });

    $('.dataTable').DataTable({
        "pagingType": "listbox",
        "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "ทั้งหมด"]],
        "language": {
            "search": "ค้นหา:",
            "lengthMenu": "แสดง _MENU_ แถว", 
            "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
            "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
            "paginate": {
                "info": "แสดง _MENU_ แถว",
            },
        }
    })
    $(document).ready(function(){
        $('.paginate_page').text('หน้า')
        let val = $('.paginate_of').text()
        $('.paginate_of').text(val.replace('of', 'จาก')); 

        setTimeout(()=>{
            $('.alert').toggle('slow')
        },2000)
        
    })

    $(document).on('click','.delBtn', function(){
        let res = window.confirm('ต้องการลบข้อมูลใช่หรือไม่ !!!')
          if(res === true){
            let inv_id = $(this).data('del_invoice_id');
            let comment = $(`#comment${inv_id}`).val()
            window.location.href = `/invoice/delete/${inv_id}/${comment}`
          }else{
            return false;
          } 
      });

</script>
@endsection


