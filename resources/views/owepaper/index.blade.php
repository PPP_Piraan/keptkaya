@extends('layouts.adminlte')

@section('mainheader')
ออกใบแจ้งเตือนชำระหนี้
@endsection
@section('nav')
<a href="{{'owepaper/index'}}">ออกใบแจ้งเตือนชำระหนี้</a>
@endsection
@section('owepaper')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }

</style>
@endsection

@section('content')


{{-- @if (collect($invoice_period)->count() == 0)

<div class="col-lg-6 col-6">
    <div class="small-box bg-warning">
        <div class="inner">
            <h3>ยังไม่ได้สร้างรอบบิลปัจจุบัน</h3>
            <p>&nbsp;</p>
        </div>
        <div class="icon">
            <i class="fas fa-exclamation-circle"></i>
        </div>
        <a href="{{url('invoice_period')}}" class="small-box-footer">สร้างรอบบิลปัจจุบัน 
            <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
@else --}}
<div class="card2">
    <div class="card-body2" id="aa">
        <div class="">
            {{-- ค้นหา --}}
            <div class="info-box">
                <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>
                <div class="info-box-content">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label">หมู่ที่</label>
                            <select class="form-control" name="zone_id" id="zone_id">
                                <option value="all" selected>ทั้งหมด</option>
                                @foreach ($zones as $zone)
                                <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">เส้นทาง</label>
                            <select class="form-control" name="subzone_id" id="subzone_id">
                                <option value="all" selected>ทั้งหมด</option>

                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">&nbsp;</label>
                            <button type="button" class="form-control btn btn-primary searchBtn">ค้นหา</button>
                        </div>
                    </div>
                </div>
            </div>

             {{-- ส่งข้อมูลที่เลือกไปปริ้น --}}
            <form action="{{url('owepaper/print')}}" method="POST" onsubmit="return check();">
                @csrf
                <input type="hidden" name="from_view" value="owepaper">

                <div class="card res">
                    <div class="card-header header hidden">
                        <div class="card-title">
                            <div class="info-box">
                                <span class="info-box-icon bg-warning"><i class="fas fa-users"></i></span>
                  
                                <div class="info-box-content">
                                  <span class="info-box-text">ค้างชำระ(คน)</span>
                                  <span class="info-box-number oweCount"></span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                        </div>
                        <div class="card-tools">
                            <input type="submit" class="btn btn-primary mb-3 float-right" id="print_multi_inv"
                                value="ปริ้นใบแจ้งเตือนชำระหนี้ที่เลือก">
                        </div>
                    </div>

                    <div class="card-body table-responsive">
                        <div id="DivIdToExport">
                            <table id="oweTable" class="table text-nowrap" width="100%"></table>
                        </div>
                    </div>
                    <!--card-body-->
                    <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>

                </div>
            </form>
        </div>

    </div>
    {{-- @endif --}}
    @endsection


    @section('script')
    <script
        src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
    </script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('js/my_script.js')}}"></script>
    <script>
        let table;    
        let cloneThead = true
        let col_index = -1


        $(document).ready(function () {
            $('.paginate_page').html('หน้า')
            let val = $('.paginate_of').text()
            $('.paginate_of').text(val.replace('of', 'จาก'));

            //เอาค่าผลรวมไปแสดงตารางบนสุด
            $('#meter_unit_used_sum').html($('.meter_unit_used_sum').val())
            $('#owe_total_sum').html($('.owe_total_sum').val())
            $('#reserve_total_sum').html($('.reserve_total_sum').val())
            $('#all_total_sum').html($('.all_total_sum').val())

            
        }) //document
        
        $(document).ready(function () {
           getOweInfos() 
        })

        $('body').on('click', 'tbody tr', function () {
            // $('.select-item').text('')
            let checked = $(this).children().first().children().first()
            if (checked.prop('checked') === false) {
                checked.prop('checked', true)
                $(this).addClass('selected')
                $(this).addClass('shown')

            } else {
                $(this).removeClass('selected')
                $(this).removeClass('shown')
                checked.prop('checked', false)
            }
        }), 

      

        $('body').on('click', '.findInfo', function () {
            let user_id = $(this).data('user_id')

            var tr = $(this).closest('tr');
            var row = table.row(tr);
            console.log('row', row)
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                //หาข้อมูลการชำระค่าน้ำประปาของ  user
                $.get(`../../api/users/user/${user_id}`).done(function (data) {
                    row.child(owe_by_user_id_format(data)).show();
                    tr.prop('shown');
                });

            }
            if ($(this).hasClass('fa-plus-circle')) {
                $(this).removeClass('fa-plus-circle')
                $(this).removeClass('text-success')
                $(this).addClass('fa-minus-circle')
                $(this).addClass('text-info')

                // aa(user_id, tr)

            } else {
                $(this).addClass('fa-plus-circle')
                $(this).addClass('text-success')
                $(this).removeClass('fa-minus-circle')
                $(this).removeClass('text-info');
            }

        });

        $('.searchBtn').click(function(){
            let zone_id = $('#zone_id').val()
            let subzone_id = $('#subzone_id').val()
            console.log(subzone_id)
            if(zone_id !== 'all' && subzone_id === ''){
                alert('ยังไม่ได้เลือกเส้นทาง')
                $('#subzone_id').addClass('border border-danger rounded')
                return false
            }
            $('#oweTable').DataTable().destroy();

            getOweInfos(zone_id, subzone_id)
        });

        function getOweInfos(zone_id = 'all', subzone_id = 'all'){
            let params = { zone_id: zone_id, subzone_id: subzone_id}
            $.get(`../api/owepaper/index`,params).done(function (data) {
                if (data.length === 0) {
                    $('.res').html('<div class="card-body h3 text-center">ไม่พบข้อมูล</div>')
                } else {
                    $('.oweCount').html(data.length)
                    $('.header').removeClass('hidden')

                    table = $('#oweTable').DataTable({
                        responsive: true,
                        // order: false,
                        "pagingType": "listbox",
                        "lengthMenu": [
                            [10, 25, 50, 150, -1],
                            [10, 25, 50, 150, "ทั้งหมด"]
                        ],
                        "language": {
                            "search": "ค้นหา:",
                            "lengthMenu": "แสดง _MENU_ แถว",
                            "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                            "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                            "paginate": {
                                "info": "แสดง _MENU_ แถว",
                            },
                        
                        },
                        dom: 'lBfrtip',
                        buttons: [
                           
                        ],
                        select: {
                            style: 'multi',
                        },
                        data: data,
                        columns: [
            
                            {
                                'title': '',
                                data: 'user_id',
                                orderable: false,
                                render: function (data) {
                                    return `
                                    <input type="checkbox" class="invoice_id" style="opacity:0" 
                                            name="user_id[${data}]">
                                    <i class="fa fa-plus-circle text-success findInfo" 
                                            data-user_id="${data}"></i>`
                                }
                            },
                            // {
                            //     'title': 'เลขผู้ใช้งาน',
                            //     data: 'user_id',
                            // },
                            {
                                'title': 'ชื่อ-สกุล',
                                data: 'name',
                            },
                            {
                                'title': 'เลขมิเตอร์',
                                data: 'meternumber',
                                'className': 'text-center',
                            },
                            {
                                'title': 'บ้านเลขที่',
                                data: 'address',
                                'className': 'text-right',
                            },
                            {
                                'title': 'หมู่ที่',
                                data: 'zone_name',
                                'className': 'text-center',
                            },
                            {
                                'title': 'เส้นทาง',
                                data: 'subzone_name',
                                'className': 'text-center',
                            },
                            {
                                'title': 'ค้างจ่าย(เดือน)',
                                data: 'owe_count',
                                'className': 'text-center',
                                
                            }



                        ],
                    
                       
                    }) //table
                    $('.dt-buttons').prepend(`
                        <a href="javascript:void(0)" class="dt-button buttons-excel buttons-html5 ml-5 show_all_btn all" >เลือกทั้งหมด</a>`)
                } //else
                $('.overlay').remove()
                $('#oweTable_filter').remove()
                if(cloneThead){
                    $('#oweTable thead tr').clone().appendTo('#oweTable thead');
                        cloneThead= false
                    }
                    $('#oweTable thead tr:eq(1) th').each( function (index) {
                        var title = $(this).text();
                        $(this).removeClass('sorting')
                        $(this).removeClass('sorting_asc')
                        if(index >0  && index <=6) {
                            $(this).html( `<input type="text" data-id="${index}" class="col-md-12" id="search_col_${index}" placeholder="ค้นหา" />` );
                        }else{
                            $(this).html('')
                        }
                } );

                $('#oweTable thead input[type="text"]').keyup(function(){
                let that = $(this)
                var col = parseInt(that.data('id'))

                if(col !== col_index && col_index !== -1){
                    $('#search_col_'+col_index).val('') 
                    table.column(col_index)
                    .search('')
                    .draw();
                }
                setTimeout(function(){ 
                    
                    let _val = that.val()
                    if( col >2  && col <=5){
                        var val = $.fn.dataTable.util.escapeRegex(
                            _val
                        );
                        table.column(col)
                        .search( val ? '^'+val+'.*$' : '', true, false )
                        .draw();
                    }else{
                        table.column(col)
                        .search( _val )
                        .draw();
                    }
                 }, 300);
     
                col_index = col

            })
             
            }) //.get
        }

       $('#zone_id').change(function () {
            //get ค่าsubzone 
            $.get(`../api/subzone/${$(this).val()}`)
                .done(function (data) {
                    console.log($(this).val())
                    let text = $(this).val() !== 'all' ? '<option value="">เลือก</option>': '';
                    if(data.length > 1){
                        text += `<option value="all">ทั้งหมด</option>`;
                    }
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.subzone_name}</option>`
                    });
                    $('#subzone_id').html(text)
                });
        });


        function check() {
            var res = 0;
            $('.invoice_id').each(function (index, ele) {
                if ($(this).is(":checked")) {
                    res = 1;
                    return false;
                }
            })
            if (res === 0) {
                alert('กรุณาเลือกผู้ใช้น้ำที่ต้องการออกใบแจ้งเตือนชำระหนี้')
                return false
            } else {
                return true
            }
        }

        $('body').on('click', '.show_all_btn',function(){
            let _val = $(this).hasClass('all') ? 'all' : '';
            openAllChildTable(_val)
        })

        function openAllChildTable(_val){            
            if(_val === 'all'){
                $("table > tbody > tr[role='row']").each(function (index, val) {
                  
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    let checked = tr.children().first().children().first()
                    if (checked.prop('checked') === false) {
                        checked.prop('checked', true)

                    }
                    tr.addClass('shown');
                    tr.addClass('selected')
                    $('.show_all_btn').removeClass('all')
                    $('.show_all_btn').text('ยกเลิกเลือกทั้งหมด')
                })

            }else{
                $("table > tbody > tr[role='row']").each(function () {
                    var tr = $(this).closest('tr');
                    let checked = tr.children().first().children().first()

                    var row = table.row(tr);
                    row.child.hide();
                    tr.removeClass('shown');
                    tr.removeClass('selected');
                    checked.prop('checked', false)

                    $('.show_all_btn').addClass('all')
                    $('.show_all_btn').text('เลือกทั้งหมด')
                    $('.show_detail').prop('checked', false);
                })

            }
            
        }

       

    </script>
    @endsection
