@extends('layouts.adminlte')

@section('mainheader')
เพิ่มผู้รับผิดชอบพื้นที่จดมิเตอร์
@endsection
@section('undertaker-subzone')
    active
@endsection
@section('nav')
<a href="{{url('/undertaker_subzone')}}" style="font-size: .95rem"> กำหนดผู้รับผิดชอบพื้นที่จดมิเตอร์</a>
@endsection
@section('content')
      @if (Session::has('message'))
        <div class="alert alert-info alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h5><i class="icon fas fa-info"></i> {{Session::get('message')}}</h5>
          
        </div>
      @endif
          
        @if (count($tw_mans) == 0)
        <div class="info-box col-6">
          <span class="info-box-icon bg-warning" style="width: 90px !important; font-size:3.2rem !important"><i class="fas fa-exclamation-circle"></i></span>

          <div class="info-box-content text-center" >
            <span class="info-box-number h4">ยังไม่ได้เพิ่มข้อมูลเจ้าหน้าที่จดมิเตอร์</span>
            <a class="btn btn-info" href="{{url('staff')}}">เพิ่มข้อมูลเจ้าหน้าที่จดมิเตอร์</a>

          </div>

          <!-- /.info-box-content -->
        </div>
         
        @else
          <form action="{{url('undertaker_subzone/store')}}" method="post" onsubmit="return check()">
            @csrf
            <div class="row">
              <div class="col-md-3">
                <label class="control-label">เลือกเเจ้าหน้าที่จดมิเตอร์</label>
                <select class="form-control" name="twman_id" id="twman_id">
                  <option value="">เลือก..</option>
                  @foreach ($tw_mans as $twman)
                      <option value="{{$twman->id}}">{{$twman->user_profile->name}}</option>
                  @endforeach
                </select>

                <div class="card card-outline card-info mt-3">
                  <div class="card-header text-center">
                    ข้อมูลเจ้าหน้าที่<br>กับเส้นทางจดมิเตอร์
                  </div>
                  <div class="card-body">
                      @foreach ($tw_mans as $twman)
                          <div class="card card-widget card-warning card-twman card-outline mt-2" data-id="{{$twman->id}}" id="card-twman{{$twman->id}}">
                            <div class="card-header">
                              <div class="user-block">
                                <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
                                <span class="username"><a href="#">{{$twman->user_profile->name}}</a></span>
                                <span class="description"></span>
                              </div>
                              <!-- /.user-block -->
                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                              </div>
                              <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <ul class="nav flex-column">
                                  
                                    @foreach ($twman->undertaker_subzone as $item)
                                      <li class="nav-item">
                                        <a href="#" class="nav-link">
                                          {{$item->subzone->zone->zone_name}} 
                                          <span class="float-right badge bg-info">เส้น: {{$item->subzone->subzone_name}}</span>
                                        </a>
                                      </li>
                                    @endforeach
                          
                                </ul>
                            </div>
                          </div>
                      @endforeach
                    </div>
                  </div><!--card card-outline card-info mt-3-->
              </div><!--col-md-3-->
              <div class="col-md-9">
                <div class="card card-primary card-outline">
                  <div class="card-header">
                    <div class="card-title h-4">
                      เลือกเส้นทางจัดเก็บค่าน้ำประปา
                    </div>
                    <div class="card-tools">
                      <input type="submit" class="btn btn-primary" value="บันทึก">
                    </div>
                  </div>
                  <div class="card-body p-2">
                    <div class="row">
                      @foreach ($subzone as $item)
                        <div class="col-3">
                          <div class="card card-outline card-primary">
                            <div class="card-header">
                              <h3 class="card-title"> {{$item[0]->zone_name}}</h3>
                              <div class="card-tools">
                                
                              </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body  p-1">
                              <div class="row">
                                <div class="col-md-9 text-center pt-3">
                                  <span class="text-primary"><i class="fas fa-road"></i></span> 
                                  <span class="h5">{{$item[0]->subzone_name}}</span>
                                </div>
                                <div class="col-md-3">
                                  <input type="checkbox"  name="on[{{$item[0]->zone_id.'-'.$item[0]->subzone_id}}]" 
                                      style="width: 30px; height:50px" class="subzone_checkbox"  id="{{'sz'.$item[0]->subzone_id}}">
                                </div>
                              </div>
                            </div>
                            <!-- /.card-body -->
                          </div>  
                        </div>  
                      @endforeach
                    </div><!--col-md-9>row-->                     
                  </div><!--card-body-->
                </div>
              </div><!--col-md-9-->
            </div><!--row-->
          </form>      
        @endif
@endsection


@section('script')
    <script>
      $('#twman_id').change(function(){
        let id = $(this).val();
        if(id == ""){
          $('.card-twman').removeClass('hidden');
        }else{
          $('.card-twman').addClass('hidden');
          $(`#card-twman${id}`).removeClass('hidden');
        }
      });

    function check(){
      let res = true;
      let resCheckbox =  false;
      let errTxt = '';
      let i = 0;

      if($('#twman_id').val() === "" ){
          errTxt += '- ยังไม่ได้เลือกผู้รับผิดชอบพื้นที่จดมิเตอร์\n'
          res =  false;
      }
    
      errCheckbox = '- ยังไม่ได้ทำการเลือกเส้นทางจดมิเตอร์\n'
      $('input.subzone_checkbox:checked').each(function(val){
          resCheckbox = true;
          errCheckbox = '';
          return false;
      });

      console.log(resCheckbox)
      if(res === false || resCheckbox === false){
          errTxt += errCheckbox
          alert(errTxt);
          return false;
      }else{
        return true;
      }  
      
    }
    </script>
@endsection
  