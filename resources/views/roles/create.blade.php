@extends('layouts.adminlte')

@section('mainheader')
 เพิ่มกฎและสิทธิ์
@endsection
@section('role_and_permission')
    active
@endsection
@section('nav')
<a href="{{url('/role_and_permission')}}"> กฎและสิทธิ์การเข้าใช้งานระบบ</a>
@endsection

@section('content')
    
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title"></h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ url('role_and_permission/store') }}" method="POST" role="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                    <label for="role">กฏ</label>
                    <input type="text" class="form-control" id="role" name="role" placeholder="กำหนดชื่อเป็นภาษาอังกฤษ">
                    </div>
                    <div class="form-group">
                    <label for="display_name">ชื่อกฏที่ใช้แสดง</label>
                    <input type="text" class="form-control" id="display_name" name="display_name" placeholder="">
                    </div>
                    <div class="form-group">
                    <label for="description">คำอธิบาย</label>
                    <textarea class="form-control" id="description" name="description" ></textarea>
                    </div>
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
            <!-- /.card -->

        </div>                    
    </div>
      
@endsection