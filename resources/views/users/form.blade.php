
@extends('layouts.adminlte')


@section('content')
<style>
    .err{
        color: red;
        font-size: 0.85rem
    }
</style>
<?php $meternumber = substr($user->meternumber,2); ?>
<form action="{{url('users/update/'.$user->id)}}" method="POST" onsubmit="return checkStatus();">
    @csrf
    @method('PUT')
    <input type="hidden" name="changemeterstatus" value="{{$changemeter}}">
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                            src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                    </div>
                    <p class="text-muted text-center">สมาชิกผู้ใช้น้ำ</p>
                    <a href="#" class="btn btn-primary btn-block"><b> {{$user->meternumber}}</b></a>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">ข้อมูลทั่วไป</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">เกี่ยวมิเตอร์มาตรวัด</a>
                        </li>
                        {{-- <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">สถานะและค่าธรรมเนียม</a>
                        </li> --}}
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="activity">
                            <div class="card card-small mb-4">

                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-3">
                                        <div class="row">
                                            <div class="col">                                          
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label for="feFirstName">ชื่อ - สกุล</label>
                                                            <input type="text" class="form-control" id="name" name="name"
                                                                value="{{isset($user->user_profile->name) ? $user->user_profile->name : ''}}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="feGender">เพศ</label>
                                                            <select name="gender" id="gender" class="form-control">
                                                                <option>เลือก..</option>
                                                                <option value="m" {{$user->user_profile->gender == 'm' ? 'selected' : ''}}>ชาย</option>
                                                                <option value="w" {{$user->user_profile->gender == 'w' ? 'selected' : ''}}>หญิง</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label for="feID_card">เลขบัตรประจำตัวประชาชน</label>
                                                            <input type="text" class="form-control" id="id_card" value="{{$user->user_profile->id_card}}" name="id_card">
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label for="fePhone">เบอร์โทรศัพท์</label>
                                                            <input type="text" class="form-control" id="phone" name="phone" value="{{$user->user_profile->phone}}"> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-2">
                                                            <label for="feInputAddress">ที่อยู่</label>
                                                            <input type="text" class="form-control" id="address" name="address" value="{{$user->user_profile->address}}"> 
                                                        </div>
                                                        <div class="form-group col-2">
                                                            <label>หมู่ที่</label>
                                                            <select class="form-control" name="zone_id" id="zone_id">
                                                                <option>เลือก...</option>
                                                                @foreach ($zones as $zone)
                                                                    <option value="{{$zone->id}}" {{$zone->id == $user->user_profile->zone_id ? 'selected' : ''}}>{{$zone->zone_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-3">
                                                            <label>จังหวัด</label>
                                                            <select class="form-control" name="province_code" id="province_code"
                                                                onchange="getDistrict()">
                                                                <option value="35">ยโสธร</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-3">
                                                            <label>อำเภอ</label>
                                                            <select class="form-control" name="district_code" id="district_code"
                                                                onchange="getTambon()">
                                                                <option value="3508" >เลิงนกทา</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-2">
                                                            <label>ตำบล</label>
                                                            <select class="form-control" name="tambon_code" id="tambon_code"
                                                                onchange="getZone()">
                                                                <option value="350805" >ห้องแซง</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="timeline">
                            <div class="card card-small col-6">
                                {{-- <div class="card-header border-bottom">
                                    <h6 class="m-0">เกี่ยวมิเตอร์</h6>
                                </div> --}}
                                <div class="card-body p-0">
                                    <ul class="list-group list-group-small list-group-flush">
                                        <li class="list-group-item d-flex px-3">
                                            <span class="text-semibold text-fiord-blue">เลขที่ผู้ใช้น้ำประปา</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray">
                                                <input type="text" class="form-control" readonly name="meter_id" value="{{$meternumber}}">
                                                <input type="hidden" value="{{$user->id}}" name="old_meter_id">
                                                <input type="hidden" value="{{$user->user_id}}" name="user_id">
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3 row">
                                            <span class="text-semibold text-fiord-blue col-6">เลขมิเตอร์</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray input-group col-6">
                                            {{-- {{dd($user->user_profile->userMeterInfos)}} --}}
                                                <input type="text" class="form-control"  readonly name="meternumber" id="meternumber" value="{{$user->meternumber}}">
                                                {{-- <span class="input-group-append">
                                                    <button type="button" class="btn btn-info btn-flat createmeternumber">
                                                        <i class="fas fa-plus-square"></i>
                                                    </button>
                                                </span> --}}
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3">
                                            <span class="text-semibold text-fiord-blue">ประเภทมิเตอร์</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray col-6">
                                                <div class="err" id="metertypeErr"></div>
                                                <select class="form-control" name="metertype" id="metertype">
                                                    <option>เลือก...</option>
                                                    @foreach ($tabwatermeters as $tabwatermeter)
                                                        <option value="{{$tabwatermeter->id}}" {{$tabwatermeter->id == $user->metertype ? 'selected' : ''}}>{{$tabwatermeter->typemetername}}</option>
                                                    @endforeach
                                                </select>
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3 row">
                                            <span class="text-semibold text-fiord-blue  col-6">ราคาต่อหน่วย</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray input-group col-6">
                                                <input type="text" class="form-control" readonly name="counter_unit" id="counter_unit" value="{{$user->counter_unit}}">
                                                <span class="input-group-append">
                                                    <button type="button" class="btn btn-outline-info btn-flat">
                                                        บาท
                                                    </button>
                                                </span>
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3 row">
                                            <span class="text-semibold text-fiord-blue col-6">ขนาดมิเตอร์</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray input-group col-6">
                                                <input type="text" class="form-control" readonly name="metersize" id="metersize" value="{{$user->metersize}}">
                                                <span class="input-group-append">
                                                    <button type="button" class="btn btn-outline-info btn-flat">
                                                        หุน
                                                    </button>
                                                </span>
                                            </span>
                                        </li>
                                        
                                        <li class="list-group-item d-flex px-3"> 
                                            {{-- {{dd($user->undertake_zone_id)}} --}}
                                            <span class="text-semibold text-fiord-blue">พื้นที่จัดเก็บ</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray col-6">
                                                <div class="err" id="undertake_zone_idErr"></div>
                                                <select class="form-control" name="undertake_zone_id" id="undertake_zone_id" 
                                                    onchange="getSubzone()">
                                                    <option>เลือก...</option>
                                                    @foreach ($zones as $zone)
                                                        <option value="{{$zone->id}}" {{$zone->id == $user->undertake_zone_id ? 'selected' : ''}} >{{$zone->zone_name}}</option>
                                                    @endforeach
                                                </select>
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3">
                                            <span class="text-semibold text-fiord-blue">เส้นทางจัดเก็บ</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray col-6">
                                                <div class="err" id="undertake_subzone_idErr"></div>
                                                <select class="form-control" name="undertake_subzone_id" id="undertake_subzone_id">
                                                    <option>เลือก..</option>
                                                    @foreach ($user_subzone as $un_subzone)
                                                        <option value="{{$un_subzone->id}}" {{$un_subzone->id == $user->undertake_subzone_id ? 'selected' : ''}}>{{$un_subzone->subzone_name}}</option>
                                                    @endforeach
                                                </select>
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3">
                                            <span class="text-semibold text-fiord-blue">วันที่ขอใช้น้ำ</span>
                                            <span class="ml-auto text-right text-semibold ">
                                                {{-- <php $year = date('Y')+543; $now = date('d/m/'.$year); ?> --}}
                                                <input type="text" class="form-control datepicker"
                                                    name="acceptance_date" id="acceptance_date" value="{{$user->acceptace_date}}">
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3">
                                            <span class="text-semibold text-fiord-blue">วิธีชำระเงิน</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray">
                                                <input type="text" class="form-control" readonly name="payment_id" value="1">
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3">
                                            <span class="text-semibold text-fiord-blue">ประเภทผู้ได้ส่วนลด</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray">
                                                <input type="text" class="form-control" name="discounttype" value="1" readonly>
                                            </span>
                                        </li>
                                        <li class="list-group-item d-flex px-3">
                                            <span class="text-semibold text-fiord-blue">สถานะ</span>
                                            <span class="ml-auto text-right text-semibold text-reagent-gray col-6">
                                                <select class="form-control" name="status" id="status">
                                                    <option>เลือก...</option>
                                                    <option value="active" {{$user->status == 'active' ? 'selected' : ''}}>เปิดใช้งาน</option>
                                                    {{-- <option value="changemeter" {{$user->status == 'changemeter' ? 'selected' : ''}}>เปลี่ยนมิเตอร์</option> --}}
                                                    <option value="inactive" {{$user->status == 'inactive' ? 'selected' : ''}}>ยกเลิกการใช้งาน</option>
                                                </select>
                                            </span>
                                        </li>
                                    </ul>
                                  

                                </div>
                                
                            </div>
                            <button type="submit" class="btn btn-success col-6">บันทึก</button>

                        </div>
                       
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

</form>

@endsection

@section('script')
<script>
    $(document).ready(function(){
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                // thaiyear: true              //Set เป็นปี พ.ศ.
            }).datepicker();  //กำหนดเป็นวันปัจุบัน

        })
    $('#metertype').change(function(){
        let id = $(this).val()
        $.get(`/tabwatermeter/infos/${id}`).done(function(data){
            console.log(data)
            $('#counter_unit').val(data.price_per_unit)
            $('#metersize').val(data.metersize)            
        })
    });
  function getDistrict() {
      var id = $("#province_code").val();
      $.get("/district/getDistrict/" + id).done(function (data) {
          var text = "<option>--Select--</option>";
          data.forEach(function (val) {
              text += "<option value='" + val.district_code + "'>" + val.district_name + "</option>";
          });
          $("#district_code").html(text);
      });
  }

  function getTambon() {
      
      var id = $("#district_code").val();
      $.get("/tambon/getTambon/" + id).done(function (data) {
        console.log(data)
          var text = "<option>--Select--</option>";
          data.forEach(function (val) {
              text += "<option value='" + val.tambon_code + "'>" + val.tambon_name + "</option>";
          });
          $("#tambon_code").html(text);
      });
  }

  function getZone() {
      var id = $("#tambon_code").val();
      $.get("/zone/getZone/" + id).done(function (data) {
          var text = "<option>--Select--</option>";
          data.forEach(function (val) {
              text += "<option value='" + val.id + "'>" + val.zone_name + "</option>";
          });
          $("#zone_id").html(text);
      });
  }
  function getSubzone() {
      var id = $("#undertake_zone_id").val();
      $.get("../../../subzone/getSubzone/" + id).done(function (data) {

    //   $.get("/subzone/getSubzone/" + id).done(function (data) {
          var text = "<option>เลือก...</option>";
          console.log('data', data)
          if(data.length == 0){
                text += "<option value='0'>-</option>";
          }else{
            data.forEach(function (val) {
                text += "<option value='" + val.id + "'>" + val.subzone_name + "</option>";
            });
          }
         
          $("#undertake_subzone_id").html(text);
      });

    //   $('#subzone_id').change(function(){
    //       $('.subzone_id').val($(this).val())
    //     $('#meternumber').val(`HS-${$('.zone_id').val()}-${$(this).val()}`)
    //   })
  }

  function checkStatus(){
      let status = $('#status').val()
      if(status === 'inactive'){
        let res = window.confirm('คุณต้องการยกเลิกการใช้งานมิเตอร์ใช่หรือไม่ !!!!');
        if(res === true){
            return true;
        }else{
            return false;
        }
      }
    $("#undertake_subzone_idErr").text("")
    $("#metertypeErr").text("")
    $("#undertake_zone_idErr").text("")
    console.log($("#metertype").val())
    if($("#metertype").val() === "เลือก..."){
        
        $("#metertypeErr").text("กรุณาเลือกชนิดมิเตอร์")
        return false;
    }

    if($("#undertake_zone_id").val() === "เลือก..."){
        $("#undertake_zone_idErr").text("กรุณาเลือกพื้นที่จัดเก็บ")
        return false;
    }
    else{
        if($("#undertake_subzone_id").val() === "เลือก..."){
            $("#undertake_subzone_idErr").text("กรุณาเลือกเส้นทางจัดเก็บ")
            return false;
        }
    } 
    return true;  

    }



</script>
@endsection