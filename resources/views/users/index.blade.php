@extends('layouts.adminlte')

@section('mainheader')
 ข้อมูลผู้ใช้น้ำประปา
@endsection
@section('users')
    active
@endsection
@section('nav')
<a href="{{url('/users')}}"> ข้อมูลผู้ใช้น้ำประปา</a>
@endsection

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif

  <div class="row">
    @if (collect($checkZone)->isEmpty() || $checkTabwatermeter == 0)
      @if (collect($checkZone)->isEmpty())
        <div class="col-md-5 col-sm-6 col-12">
          <div class="info-box bg-warning">
            <span class="info-box-icon"><i class="fas fa-comments"></i></span>

            <div class="info-box-content">
              <span class="info-box-number text-center font-weight-bold">ยังไม่ได้สร้างพื้นที่จดมิเตอร์</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
              <span class="progress-description text-center">
                <a href="{{ url('/zone') }}" class="btn btn-info btn-sm">สร้างพื้นที่จดมิเตอร์</a>
              </span>
            </div>
          </div>
        </div>
      @endif
      @if ($checkTabwatermeter == 0)
      <div class="col-md-5 col-sm-6 col-12">
        <div class="info-box bg-warning">
          <span class="info-box-icon"><i class="fas fa-comments"></i></span>

          <div class="info-box-content">
            <span class="info-box-number text-center font-weight-bold">ยังไม่ได้ตั้งค่าขนาดมิเตอร์</span>

            <div class="progress">
              <div class="progress-bar" style="width: 100%"></div>
            </div>
            <span class="progress-description text-center">
              <a href="{{ url('/tabwatermeter') }}" class="btn btn-info btn-sm">ตั้งค่าขนาดมิเตอร์</a>
            </span>
          </div>
        </div>
      </div>
    @endif
    @else

      <div class="col-12 col-sm-12">
        <a href="{{url('users/create')}}" class="btn btn-info pull-right mb-2">เพิ่มผู้ใช้น้ำประปา</a>

        <div class="card card-primary card-tabs">
          <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="false"> ใช้งานอยู่</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">ยกเลิกการใช้งาน</a>
              </li>
              <li class="nav-item">
                {{-- <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="true">เปลี่ยนมิเตอร์</a> --}}
              </li>
          
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content" id="custom-tabs-one-tabContent">
              <div class="tab-pane fade active show table-responsive" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                <table id="example" class="display text-nowrap" width="100%">
                  <tr>
                    <td>
                        <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div></div>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                {{-- เลิกใช้งาน --}}
                @if (collect($inactiveMeter)->isEmpty())
                    <div class="card">
                      <div class="card-body text-center"><h3>ไม่มีข้อมูล</h3></div>
                    </div>
                @else
                    <div id="cancelMeter" class="table-responsive">
                      <table class="table text-nowrap"  id="example2">
                        <thead>
                          <tr>
                            <th>รหัสผู้ใช้น้ำ</th>
                            <th>เลขมิเตอร์</th>
                            <th>ชื่อ-สกุล</th>
                            <th>ที่อยู่</th>
                            <th>หมู่ที่</th>
                            <th>วันที่ยกเลิกใช้งาน</th>
                            {{-- <th>หมายเหตุ</th> --}}
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($inactiveMeter as $inactive)
                        {{-- {{dd($inactive)}} --}}
                        @if (collect($inactive->user_profile)->isEmpty())
                            {{dd($inactive)}}
                        @endif
                          <tr>
                            <td>  {{$inactive->user_profile->id}}</td>
                            <td>  {{$inactive->meternumber}}</td>
                            <td>  {{$inactive->user_profile->name}}</td>
                            <td>  {{$inactive->user_profile->address}}</td>
                            <td>  {{$inactive->user_profile->zone == null ? '' : $inactive->user_profile->zone->zone_name}}</td>
                            <td>  {{$inactive->updated_at}}</td>
                            {{-- <td>  {{$inactive->comment}}</td> --}}
                            <td>
                              <a href="{{url('/users/show/'.$inactive->user_profile->user_id)}}" class="btn btn-info">ดูประวัติ</a>
                              <a href="{{url('/users/create/1/'.$inactive->user_profile->user_id)}}" class="btn btn-success">ขอกลับมาใช้น้ำใหม่</a>

                            </td>
                          </tr>

                        @endforeach
                        </tbody>
                      </table>
                    </div>
                @endif

              </div>
              <div class="tab-pane fade " id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                {{-- เลิกใช้งาน --}}
                {{-- @if (collect($changeMeter)->isEmpty())
                    <div class="card">
                      <div class="card-body text-center"><h3>ไม่มีข้อมูล</h3></div>
                    </div>
                @else
                  <table class="table">
                    <tr>
                      <th>เลขผู้ใช้น้ำ</th>
                      <th>ชื่อ-สกุล</th>
                      <th>ที่อยู่</th>
                      <th>หมู่ที่</th>
                      <th>เบอร์โทรศัพท์</th>
                      <th></th>
                    </tr>
                    @foreach ($changeMeter as $change)
                      <tr>
                        <td>  {{$change->meternumber}}</td>
                        <td>  {{$change->user_profile->name}}</td>
                        <td>  {{$change->user_profile->address}}</td>
                        <td>  {{$change->user_profile->zone->zone_name}}</td>
                        <td>  {{$change->user_profile->phone}}</td>
                        <td>
                          <a href="{{url('/users/create/1/'.$change->id)}}" class="btn btn-success">ขอกลับมาใช้น้ำใหม่</a>
                        </td>
                      </tr>

                    @endforeach
                  </table>
                @endif --}}
              </div>
            
            </div>
          </div>
          <!-- /.card -->
        </div>
      </div>
    @endif
  </div>
@endsection


@section('script')
    <script>    
      // $('.dataTable').dataTable();
      $(document).ready(function(){
        $.get('../../api/users').done(function(data){
          // return data;
        // $.get('api/users').done(function(data){
          $('#example').DataTable( {
            "pagingType": "listbox",
            "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "All"]],
            "language": {
                "search": "ค้นหา:",
                "lengthMenu": "แสดง _MENU_ แถว", 
                "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
                "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
                "paginate": {
                    "info": "แสดง _MENU_ แถว",
                },
            },

            data: data,
            columns: [
              {title: 'เลขผู้ใช้น้ำ', data: 'user_id_str', 'width':'15%'},
              { title: "เลขมิเตอร์" , data: 'meternumber', 'width':'10%'},
              { title: "ชื่อ-สกุล" , data: 'name', 'width':'27%'},
              { title: "ที่อยู่" , data: 'address', 'width':'13%'},
              { title: "หมู่" , data: 'zone_name', 'width':'10%'},
              { title: "เส้นทาง" , data: 'subzone_name', 'width':'10%'},
              { title: "" , data: 'showLink', 'width':'4%'},
              { title: "" , data: 'editLink', 'width':'4%'},
              // { title: "" , data: 'deleteLink', 'width':'3%'},
                
            ]
          } );

          $('.paginate_page').text('หน้า')
          let val = $('.paginate_of').text()
          $('.paginate_of').text(val.replace('of', 'จาก')); 

          setTimeout(()=>{
            $('.alert').toggle('slow')
          }, 1500)
      });//document.ready

        

      $('#example2').DataTable( {
            "pagingType": "listbox",
            "lengthMenu": [[10, 25, 50, 150, -1], [10, 25, 50, 150, "All"]],
            "language": {
                "search": "ค้นหา:",
                "lengthMenu": "แสดง _MENU_ แถว", 
                "info":       "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว", 
                "infoEmpty":  "แสดง 0 ถึง 0 จาก 0 แถว",
                "paginate": {
                    "info": "แสดง _MENU_ แถว",
                },
            },

          });
          $('.paginate_page').text('หน้า')
          let val = $('.paginate_of').text()
          $('.paginate_of').text(val.replace('of', 'จาก')); 
       

      });

      

    </script>
@endsection