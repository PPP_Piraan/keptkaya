@extends('layouts.adminlte')
ี<?php
  $func = new App\Http\Controllers\Api\FunctionsController;

?>
@section('mainheader')
  ประวัติ
@endsection
@section('users')
    active
@endsection
@section('nav')
<a href="{{url('/users')}}"> ข้อมูลผู้ใช้น้ำ</a>
@endsection

@section('style')
    <style>
      th{
        text-align: center;
      }
      .card-danger{
        border: 2px solid red !important;
        border-radius: 5px 5px;
      }
    </style>
@endsection
@section('content')
    <div class="card">
        <div class="card-body {{$user[0]->status == 'active' ? ''  : 'card-danger'}}">
            <div class="row">
                <div class="col-3">
                    <div class="card card-info card-outline">
                        <div class="card-body box-profile ">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset('public/adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                                <h3 class="profile-username text-center" id="feFirstName">
                                  {{$user[0]->user_profile->name}}
                                </h3>
                            </div>
                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>ที่อยู่</b> 
                                    <a class="float-right" id="feInputAddress">
                                      <?php
                                        echo  "บ้านเลขที่ ".$user[0]->user_profile->address." หมู่ ".$user[0]->user_profile->zone_id
                                      ?>
                                    </a>
                                </li>
                              
                                <li class="list-group-item">
                                  <b>เบอร์โทรศัพท์</b> <a class="float-right" id="phone">
                                    {{$user[0]->user_profile->phone}}
                                  </a>
                              </li>

                              <li class="list-group-item text-center">
                                <b>สถานะมิเตอร์</b> <br><a class=" h4 {{$user[0]->status == 'active' ? 'text-success' : 'text-danger'}}" id="phone">
                                  {{$user[0]->status == 'active' ? ' เปิดใช้งาน' : 'ยกเลิกการใช้งาน'}}
                                </a>
                            </li>


                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-9 table-responsive">
                  <table class="table table-striped text-nowrap">
                    <thead>
                        <tr>
                        <th>เลขใบแจ้งหนี้</th>
                        <th>รหัสผู้ใช้งาน</th>
                        <th>เลขมิเตอร์</th>
                        <th>รอบบิล</th>
                        <th>วันที่จดมิเตอร์</th>
                        <th>ยอดครั้งก่อน</th>
                        <th>ยอดปัจจุบัน</th>
                        <th>จำนวนที่ใช้</th>
                        <th>คิดเป็นเงิน(บาท)</th>
                        <th>สถานะ</th>
                        <th>หมายเหตุ</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($user[0]->invoice_by_user_id as $item)
                        <?php
                          $bg_status = '';
                          if($item->status == 'owe'){
                            $bg_status = 'bg-warning';
                          }elseif ($item->status == 'invoice') {
                            $bg_status = 'bg-info';
                          }
                        ?>
                        <tr class="{{$bg_status}}">
                          <td class="text-right">IVHS{{$func->createInvoiceNumberString($item->id)}}</td>
                          <td>TWU{{$func->createInvoiceNumberString($item->usermeterinfos->user_id)}}</td>
                          <td>{{$item->usermeterinfos->meternumber}}</td>
                          <td>{{$item->invoice_period->inv_period_name}}</td>
                          <td>
                            <?php
                                $exp  = explode(' ', $item->updated_at);
                                $date = $func->engDateToThaiDateFormat($exp[0]);
                                $time = explode(":", $exp[1]);
                            ?>
                            {{$date."  ".$time[0].":".$time[1]."น."}}
                          </td>
                          <td class="text-right">{{$item->lastmeter}}</td>
                          <td class="text-right">{{$item->currentmeter}}</td>
                          <td class="text-right">{{$item->currentmeter - $item->lastmeter }}</td>
                          <td class="text-right">{{($item->currentmeter - $item->lastmeter)*8 }}</td>
                          <td class="text-center">{{$func->statusThai($item->status)}}</td>
                          <td>{{$item->comment}}</td>
                        </tr>
                      @endforeach      
                  </tbody>
                </table>
                </div>
            </div>

        </div><!-- /.card-body -->
    </div>

@endsection

