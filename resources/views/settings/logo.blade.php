<div class="card card-primary">
    <div class="card-header ">รูปตราหน่วยงานที่ใช้ปัจจุบัน</div>

    <div class="card-body">
        {{-- <img src="{{url('img/hslogo.jpg')}}" alt=""> --}}
        @if (isset($logo['values']))
            @if($logo['values'] != "")
                <img src="{{url('logo/'.$logo['values'])}}"  width="200"/>
            @endif
        @endif
      
    </div>
</div>

<div class="card  card-secondary">
    <div class="card-header">เลือกภาพที่ต้องการอัพโหลด</div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="logo" onchange="preview('0')" class="form-control mt-4">
            </div>
            <div class="col-md-6 text-center">
                <img id="frame0" src="" width="100px" />

            </div>
        </div>
    </div>
</div>



@section('script')
    <script>
        function preview(id) {
            console.log('id',id)
            $(`#frame${id}`).attr('src', URL.createObjectURL(event.target.files[0]));
        }

        let i = 1;
        $(document).on('click','.append_sign_form_btn',()=>{
            
            let text = `
            <div class="form-group row" id="form${i}">
                        <div class="col-sm-1  trash_div" onclick="del('${i}')">
                            <label for="organize_address" class="col-form-label ">&nbsp;</label>
                            <i class="fas fa-trash-alt text-danger form-control"></i>
                        </div>
                        <div class="col-sm-6 row">
                            <div class="col-md-12">
                                <label for="organize_address" class=" col-form-label">ชื่อ-สกุล</label>
                                    <input type="text" class="form-control" name="sign[${i}][name]" id="sign${i}">
                            </div>
                            <div class="col-md-12">
                                <label for="organize_address" class=" col-form-label">ตำแหน่ง</label>
                                    <input type="text" class="form-control" name="sign[${i}][position]">
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label for="organize_address" class="col-sm-2 col-form-label">&nbsp;</label>
                            <img id="frame${i}" src="" class="mt-4" width="200px" />

                            <input type="file" onchange="preview(${i})" class="form-control" style="position: absolute;
                                bottom: 0px;" name="filenames[]">
                        </div>
                    
                    </div>

            `;
            $('#append_sign_form').append(text)
            i = i+1;
        });


        function del(id){
            $(`#form${id}`).remove()
        }
    </script>
@endsection