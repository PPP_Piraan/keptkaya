<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title"></h3>
    </div>
    <!-- /.card-header -->
        <div class="card-body row">
            <div class="form-group col-md-3">
                <label for="meternumber_code">รหัสเลขมิเตอร์</label>
                <div class="input-group mb-3 ">
                    <input type="text" class="form-control" name="meternumber_code" id="meternumber_code" 
                    value="{{ isset($meternumber_code['values']) ? $meternumber_code['values'] : ''  }}"
                    
                    placeholder="HS-IV">
                    <div class="input-group-append">
                      <span class="input-group-text">-00001</span>
                    </div>
                  </div>
            </div>
        </div>
</div>