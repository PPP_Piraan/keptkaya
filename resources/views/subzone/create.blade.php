@extends('layouts.adminlte')

@section('title')
ตั้งค่า / พื้นที่จดมิเตอร์น้ำประปา / เพิ่มข้อมูลพื้นที่จดมิเตอร์น้ำประปา
@endsection

@section('content')
<form action="{{url('subzone/store')}}" method="POST">
    @csrf
    <div class="row">

        <div class="col-lg-8 mb-4">
            <div class="card card-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-info">
                    <h3 class="widget-user-username">รายการเส้นทางจดมิเตอร์</h3>
                    <h5>หมู่ที่ {{$zone->zone_name}}</h5>
                    <h5> {{$zone->location}}</h5>
                </div>
                
                <div class="card-footer">
                    <div id="routList">
                        <div class="input-group mb-3 route last">
                            <input type="text" class="form-control" name="subzone[0]" id="routeNameTemp" value="{{$zone->subzone[0]['subzone_name']}}">
                                <a href="javascript:void(0)" class="btn btn-primary  addRoute">เพิ่ม</a>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success submitBtn hidden">บันทึก</button>
                </div>
              </div>
            
        </div>
    </div>

</form>

@endsection

@section('script')
    <script>
        let i = 1;
        //เพิ่ม list เส้นทาง
        $('body').on('click','.addRoute',function(e){
            console.log('sdf')
            if($(this).parent().parent().hasClass('last')){
                $(this).parent().parent().removeClass('last')
            }
            let text = `
                <div class="input-group mb-3 route last">
                    <input type="text" class="form-control" name="subzone[${i}]"  value="${$('#routeNameTemp').val()}">
                    <a href="javascript:void(0)" class="btn btn-primary addRoute">เพิ่ม</a>
                </div>

            `;
            
            $('#routList').append(text);
            i++;

            //เปลี่ยนคุณสมบัติ list  ทุกอัน ยกเว้น list สุดท้าย
            $('.addRoute').each(function(index, element){
                if(!$(this).parent().parent().hasClass('last')){
                    $(this).removeClass('addRoute');
                    $(this).addClass('removeRoute');
                    $(this).html('ลบ')
                    $(this).removeClass('btn-primary');
                    $(this).addClass('btn-danger')
                }else{
                    $(this).parent().siblings().val('')
                }
            });

        });

        //ลบ list เส้นทาง
        $("body").on("click",".removeRoute",function(e){
            $(this).parent().parent().remove();
        });

        
        $('#zone').change(function(){
            if($('.submitBtn').hasClass('hidden')){
                $('.submitBtn').removeClass('hidden');
                $('.submitBtn').addClass('show');
            }
        });

    </script>
@endsection