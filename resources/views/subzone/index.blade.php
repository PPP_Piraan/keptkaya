@extends('layouts.adminlte')

@section('title')
ตั้งค่า / เส้นทางจดมิเตอร์น้ำประปา
@endsection

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-{{Session::get('color')}} alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif
<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header border-0">
                <h3 class="mb-0">ตารางเส้นทางจดมิเตอร์น้ำประปา</h3>
                <a href="/subzone/create" class="btn btn-primary my-4 float-r">เพิ่มเส้นทางจดมิเตอร์น้ำประปา</a>
            </div>
            @if (count($subzone) == 0)
            <nav class="navbar navbar-expand-lg navbar-dark bg-warning" style="margin:10px 10px">
                <div class="container">
                    <a class="navbar-brand" href="#">ไม่พบข้อมูลการเส้นทางจดมิเตอร์น้ำประปา</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default"
                        aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                </div>
            </nav>
            @else
            <div class="table-responsive">
              <table class="table align-items-center table-flush example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>เส้นทาง</th>                   
                        <th>โซน</th>
                        <th>ที่ตั้ง</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1 ?>
                      @foreach ($subzone as $item)
                        <tr>
                            {{-- {{dd($item->zone->tambon)}} --}}
                          <td>{{$i++}}</td>
                          <td>{{$item->subzone_name}}</td>
                          <td>{{$item->zone->zone_name}}</td>
                          <td>
                            ต.{{$item->zone->tambon != null ? $item['zone']['tambon']->tambon_name : ''}}
                            อ.{{ $item->zone->tambon != null ? $item['zone']['tambon']['district']->district_name : ''}} 
                            จ.{{ $item->zone->tambon != null ? $item['zone']['tambon']['district']['province']->province_name : ''}} 
                             
                          <td>
                              <a href="/subzone/edit/{{$item->id}}"
                                class="mb-2 btn btn-warning  mr-2">แก้ไข</a>
                            <a href="/subzone/delete/{{$item->id}}"
                                data-id="{{$item->id}}"
                                class="mb-2 btn btn-danger pl-3 pr-3  mr-2"
                                data-toggle="modal" data-target="#exampleModal">ลบ</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
              </table>

            </div>
            @endif


        </div>
    </div>
</div>


@endsection
@if (count($subzone) > 0)
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ยืนยันการลบข้อมูล</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ต้องการลบข้อมูลหรือไม่ ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                <a href="/subzone/delete/{{$item->id}}" class="btn btn-danger"> ต้องการลบข้อมูล</a>
            </div>
        </div>
    </div>
</div>

@endif