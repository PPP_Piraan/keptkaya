@extends('layouts.adminlte')

@section('mainheader')
เพิ่มข้อมูลเจ้าหน้าที่งานประปา
@endsection
@section('nav')
<a href="{{url('staff')}}"> ข้อมูลเจ้าหน้าที่งานประปา</a>
@endsection
@section('staff')
    active
@endsection
@section('content')
<form action="{{url('/staff/update/'.$user->id)}}" method="post">
    @csrf
    @method("PUT")
    <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                            src="{{asset('public/adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                    </div>
                    <p class="text-muted text-center">เจ้าหน้าที่งานประปา</p>
                    <a href="#" class="btn btn-primary btn-block"><b></b></a>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="activity">
                            <div class="card card-small mb-4">

                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-3">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="feFirstName">ชื่อ - สกุล</label>
                                                        <input type="text" class="form-control" id="name" name="name"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->name : ''}}">
                                                    </div>

                                                    <div class="form-group col-md-">
                                                        <label for="feGender">เพศ</label>
                                                        <select name="gender" id="gender" class="form-control">
                                                            <option>เลือก..</option>
                                                            @if (collect($user)->isNotEmpty())
                                                            <option value="m"
                                                                {{ $user->user_profile->gender == 'm' ? 'selected' : ''}}>
                                                                ชาย</option>
                                                            <option value="w"
                                                                {{ $user->user_profile->gender == 'w' ? 'selected' : ''}}>
                                                                หญิง</option>
                                                            @else
                                                            <option value="m">ชาย</option>
                                                            <option value="w">หญิง</option>
                                                            @endif

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-4"> 
                                                        <label for="feID_card">ตำแหน่ง</label>
                                                        <select name="usercategory_id" id="usercategory_id" class="form-control">
                                                            <option>เลือก..</option>
                                                            @foreach ($usercategories as $item)
                                                                <option value="{{$item->id}}" {{$item->id == $user->user_cat_id ? 'selected' : ''}}>{{$item->user_cate_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-5">
                                                        <label for="feID_card">เลขบัตรประจำตัวประชาชน</label>
                                                        <input type="text" class="form-control" id="id_card"
                                                            name="id_card"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->id_card : ''}}">
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <label for="fePhone">เบอร์โทรศัพท์</label>
                                                        <input type="text" class="form-control" id="phone" name="phone"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->phone : ''}}">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-3">
                                                        <label for="feInputAddress">ที่อยู่</label>
                                                        <input type="text" class="form-control" id="address"
                                                            name="address"
                                                            value="{{collect($user)->isNotEmpty() ? $user->user_profile->address : ''}}">
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <label>หมู่ที่</label>
                                                        <select class="form-control" name="zone_id" id="zone_id">
                                                            <option>เลือก...</option>
                                                            @if (collect($user)->isNotEmpty())
                                                            @foreach ($zones as $zone)
                                                            <option value="{{$zone->id}}"
                                                                {{ $user->user_profile->zone_id == $zone->id ? 'selected' : ''}}>
                                                                {{$zone->zone_name}}</option>
                                                            @endforeach
                                                            @else
                                                            @foreach ($zones as $zone)
                                                            <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                                                            @endforeach
                                                            @endif

                                                        </select>
                                                    </div>


                                                    <div class="form-group col-3">
                                                        <label>จังหวัด</label>
                                                        <select class="form-control" name="province_code"
                                                            id="province_code" onchange="getDistrict()">
                                                            <option value="35">ยโสธร</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <label>อำเภอ</label>
                                                        <select class="form-control" name="district_code"
                                                            id="district_code" onchange="getTambon()">
                                                            <option value="3508">เลิงนกทา</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <label>ตำบล</label>
                                                        <select class="form-control" name="tambon_code" id="tambon_code"
                                                            onchange="getZone()">
                                                            <option value="350805">ห้องแซง</option>

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item p-3">
                                        <div class="row">
                                            <div class="form-group col-4">
                                                <label for="feInputAddress">Username</label>
                                                <input type="text" class="form-control" id="username"
                                                    name="username"
                                                    value="{{collect($user)->isNotEmpty() ? $user->username : ''}}">
                                            </div>
                                            <div class="form-group col-4">
                                                <label for="feInputAddress">Password</label>
                                                <input type="text" class="form-control" id="addrpasswordess"
                                                    name="password" value="">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <button type="submit" class="btn btn-success col-6">บันทึก</button>

                        </div>
            
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>

</form>


@endsection


@section('script')
<script>
    $(document).ready(function(){
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                // thaiyear: true              //Set เป็นปี พ.ศ.
            }).datepicker();  //กำหนดเป็นวันปัจุบัน
    });

    $('#metertype').change(function(){
        let id = $(this).val()
        $.get(`../../../tabwatermeter/infos/${id}`).done(function(data){//server
        // $.get(`/tabwatermeter/infos/${id}`).done(function(data){
            $('#counter_unit').val(data.price_per_unit)
            $('#metersize').val(data.metersize)            
        })
    });
    function getDistrict() {
        var id = $("#province_code").val();
        $.get("/district/getDistrict/" + id).done(function (data) {
            var text = "<option>--Select--</option>";
            data.forEach(function (val) {
                text += "<option value='" + val.district_code + "'>" + val.district_name + "</option>";
            });
            $("#district_code").html(text);
        });
    }

    function getTambon() {
        var id = $("#district_code").val();
        $.get("/tambon/getTambon/" + id).done(function (data) {
            console.log(data)
            var text = "<option>--Select--</option>";
            data.forEach(function (val) {
                text += "<option value='" + val.tambon_code + "'>" + val.tambon_name + "</option>";
            });
            $("#tambon_code").html(text);
        });
    }

    function getZone() {
        var id = $("#tambon_code").val();
        $.get("../../../zone/getZone/" + id).done(function (data) {
        // $.get("/zone/getZone/" + id).done(function (data) {
            var text = "<option>--Select--</option>";
            data.forEach(function (val) {
                text += "<option value='" + val.id + "'>" + val.zone_name + "</option>";
            });
            $("#zone_id").html(text);
        });
    }

    function getSubzone() {
        var id = $("#undertake_zone_id").val();
        $.get("../../../subzone/getSubzone/" + id).done(function (data) {
        // $.get("/subzone/getSubzone/" + id).done(function (data) {

            var text = "<option>เลือก...</option>";
            console.log('data', data)
            if(data.length == 0){
                    text += "<option value='0'>-</option>";
            }else{
                data.forEach(function (val) {
                    text += "<option value='" + val.id + "'>" + val.subzone_name + "</option>";
                });
            }
            $("#undertake_subzone_id").html(text);
        });
  }
</script>
@endsection