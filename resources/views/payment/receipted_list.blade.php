@extends('layouts.adminlte')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
@endsection
@section('mainheader')
ข้อมูลใบเสร็จรับเงิน
@endsection
@section('payment-search')
    active
@endsection
@section('nav')
<a href="{{url('/payment/search')}}"> ค้นหาใบเสร็จรับเงิน</a>
@endsection

 @section('style')
     <link  rel="stylesheet" href="{{asset('/adminlte/plugins/select2/css/select2.min.css')}}">
     <link rel="stylesheet" href="{{asset('/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    
 @endsection

@section('content')

    <div class="row" id="aa" >
        <div class="user_info col-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile row">
                        <div class="col-md-2">
                            <img class="profile-user-img img-fluid img-circle"
                                src="{{asset('/adminlte/dist/img/user4-128x128.jpg')}}"
                                alt="User profile picture">
                        </div>
                        <div class="col-md-2 col-sm-3 col-lg-3">
                                <h3 class="profile-username text-center" id="feFirstName">{{ $receipted_list['user']->name }} </h3>
                                <h4 class="profile-username text-center" id="meternumber2">{{ $receipted_list['user']->meternumber }}</h4>
                        </div>
                        <div class="col-md-7 col-sm-7 col-lg-6">
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <a class="float-right" id="feInputAddress">
                                        <?php
                                            echo "<b>ที่อยู่:</b>";
                                            echo "     ".$receipted_list['user']->address." ". $receipted_list['user']->zone_name."<br>";
                                            echo " ตำบล". $receipted_list['user']->tambon_name;
                                            echo " อำเภอ". $receipted_list['user']->district_name;
                                            echo " จังหวัด". $receipted_list['user']->province_name;
                                        ?>
                                    </a>
                                </li>
            
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
        <!-- /.col -->
    </div>


    <div class="row" >
        <div class="col-12 ">
            @if(collect($receipted_list['reciepts'])->count()> 0)
                    
                @foreach ($receipted_list['reciepts'] as $elements)
                    
                    <div class="card card-success card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4 h5 text-bold">
                                    ใบเสร็จรับเงินเลขที่  {{ $elements[0]->receipt_id }}
                                </div>
                                <div class=" col-md-4 text-center"> วันที่ {{ $elements[0]->receipt_th_date }}</div>
                                <div class="col-md-4 text-right">
                                    <a href="{{ url('payment/print_payment_history/'.$elements[0]->receipt_id) }}" class="btn btn-primary">ปริ้นใบเสร็จ</a>
                                    <a href="{{ url('payment/remove/'.$elements[0]->receipt_id) }}" class="btn btn-warning">ยกเลิกใบเสร็จรับเงิน</a>
                                </div>
                            </div><!--row-->                                   
                        </div>
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-striped table-bordered text-nowrap">
                                <thead>
                                    <tr>
                                    <th class="text-center">เลขใบแจ้งหนี้</th>
                                    <th class="text-center">เลขมิเตอร์</th>
                                    <th class="text-center">รอบบิล</th>
                                    <th class="text-center">ค่าน้ำประปา(บาท)</th>
                                    <th class="text-center">ค่ารักษามิเตอร์(บาท)</th>
                                    <th class="text-center">คิดเป็นเงิน(บาท)</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                <?php  $total = 0; $i = 0; ?>

                                    @foreach($elements as $element)
                                        <?php
                                            //รายละเอียดแต่ละรอบบิล
                                            $diff = $element->currentmeter - $element->lastmeter;
                                            $_total =  $diff*8;
                                            $reserve = $diff == 0 ? 10 : 0;
                                            $total += $_total + $reserve; 
                                        ?>
                                        <tr>
                                            <td class="text-center">{{ $element->iv_id }}</td>
                                            <td class="text-center">{{ $element->meternumber }}</td>
                                            <td class="text-center">{{ $element->inv_period_name }}</td>
                                            <td class="text-right"> {{ $_total }}</td>
                                            <td class="text-right"> {{ $reserve }}</td>
                                            <td class="text-right"> {{ $_total + $reserve }}</td>                            
                                        </tr>  

                                        <?php $i++; ?>
                                        @if(collect($elements)->count() == $i)
                                            <tr>
                                                <td colspan="5" class="text-success h5">รวมเป็นเงิน </td>
                                                <td class="text-right text-success h5">{{ $total }}</td>
                                            </tr>
                                            
                                            <?php $i = 0 ?>
                                        @endif
                                    @endforeach <!-- $elements as $element -->
                                </tbody>
                            </table>
                        </div><!-- card-body -->
                    </div><!--card-->
                 
                @endforeach <!-- $receipted_list['reciepts'] as $elements -->
         
            @else
                <div class="card card-warning card-outline">
                    <div class="card-body text-center">
                    <div class="text-center text-success h4">ไม่มีรายการใบเสร็จรับเงิน</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
       
@endsection


