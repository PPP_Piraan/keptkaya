@extends('layouts.adminlte')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>@page { size: A5 }</style>
@endsection
@section('mainheader')
ใบแจ้งหนี้
@endsection
@section('payment')
    active
@endsection
@section('nav')
<a href="{{url('/payment')}}"> จ่ายค่าน้ำประปา</a>
@endsection

 @section('style')
     <link  rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css')}}">
     <link rel="stylesheet" href="{{asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
 @endsection

@section('content')    
    <div class="invoice p-3 mb-3">
        <!-- title row -->
        <div class="row">
            <div class="col-12">
                <h4>
                    <i class="fas fa-globe"></i> ทต. ห้องแซง
                    <small class="float-right">Date: 2/10/2014</small>
                </h4>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>Admin, Inc.</strong><br>
                    795 Folsom Ave, Suite 600<br>
                    San Francisco, CA 94107<br>
                    Phone: (804) 123-5432<br>
                    Email: info@almasaeedstudio.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address> 
                    <strong>{{$payments[0]->usermeterinfos->user_profile->name}}</strong><br>
                    {{$payments[0]->usermeterinfos->user_profile->address}}<br>
                    San Francisco, CA 94107<br>
                    Phone: (555) 539-1037<br>
                    Email: john.doe@example.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice #007612</b><br>
                <br>
                <b>Order ID:</b> 4F3S8J<br>
                <b>Payment Due:</b> 2/22/2014<br>
                <b>Account:</b> 968-34567
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>เลขมิเตอร์</th>
                            <th>รอบบิล</th>
                            <th>ยอดครั้งก่อน</th>
                            <th>ยอดปัจจุบัน</th>
                            <th>จำนวนที่ใช้</th>
                            <th>คิดเป็นเงิน(บาท)</th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach ($payments as $item)   
                       {{-- {{dd($item)}} --}}
                            <tr>
                                <td>{{ $item->usermeterinfos->meternumber }}</td>
                                <td>{{ $item->invoice_period->inv_period_name }}</td>
                                <td>{{ $item->lastmeter }}</td>
                                <td>{{ $item->currentmeter }}</td>
                                <td><?php
                                    $remain = $item->currentmeter - $item->lastmeter;
                                    echo $remain == 0 ? 10 : $remain;
                                    ?>
                                </td>
                                <td class="text-right">{{ number_format(($item->currentmeter - $item->lastmeter)*8,2)  }}</td>

                            </tr>
                       @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-6">
               
            </div>
            <div class="col-6">

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="width:50%">Subtotal:</th>
                                <td class="text-right">{{number_format($total,2)}}</td>
                            </tr>
                            <tr>
                                <th>Vat (7.0%)</th>
                                <td class="text-right">-</td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td class="text-right">{{number_format($total,2)}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-12">
                <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> 
                    ชำระเงิน
                </button>
            
            </div>
        </div>
    </div>
@endsection
   