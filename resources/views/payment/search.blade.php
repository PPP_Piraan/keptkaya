@extends('layouts.adminlte')

@section('mainheader')
ค้นหาใบเสร็จรับเงิน
@endsection
@section('nav')
<a href="{{'payment/index'}}">ค้นหาใบเสร็จรับเงิน</a>
@endsection
@section('payment-search')
active
@endsection
@section('style')
<style>
    .hidden {
        display: none
    }

</style>
@endsection

@section('content')
<table id="example2" class="display" style="width:100%">

</table>
@if (collect($invoice_period)->count() == 0 && $oweInvCountGroupByUserId == 0)

<div class="col-lg-6 col-6">
    <div class="small-box bg-warning">
        <div class="inner">
            <h3>ยังไม่ได้สร้างรอบบิลปัจจุบัน</h3>
            <p>&nbsp;</p>
        </div>
        <div class="icon">
            <i class="fas fa-exclamation-circle"></i>
        </div>
        <a href="{{url('invoice_period')}}" class="small-box-footer">สร้างรอบบิลปัจจุบัน
            <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-8  col-xxl-7" id="aa">
        {{-- ค้นหา --}}
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-search"></i></span>
            <div class="info-box-content">
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">หมู่ที่</label>
                        <select class="form-control" name="zone_id" id="zone_id">
                            <option value="all" selected>ทั้งหมด</option>
                            @foreach ($zones as $zone)
                            <option value="{{$zone->id}}">{{$zone->zone_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">เส้นทาง</label>
                        <select class="form-control" name="subzone_id" id="subzone_id">
                            <option value="all" selected>ทั้งหมด</option>

                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">&nbsp;</label>
                        <button type="button" class="form-control btn btn-primary searchBtn">ค้นหา</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xxl-5">
            <div class="card res">
                <div class="card-header header hidden">
                    <div class="card-title">
                        <div class="info-box">
                            <span class="info-box-icon bg-warning"><i class="fas fa-users"></i></span>
              
                            <div class="info-box-content">
                              <span class="info-box-text">สมาชิก(คน)</span>
                              <span class="info-box-number oweCount"></span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                    </div>
                </div>

                <div class="card-body table-responsive">
                    <div id="DivIdToExport">
                        <table id="oweTable" class="table text-nowrap" width="100%"></table>
                    </div>
                </div>
                <!--card-body-->
                <div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>

            </div>
    </div>
    
</div>
    @endif
    <div class="modal fade" id="modal-success">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            </div>
            <form action="{{url('payment/store')}}" method="post" onsubmit="return check()">
                @csrf
                <div class="modal-body">
                    <div id="activity">             
                        <div class="row">
                            <div class="col-md-3">
                                {{-- ข้อมููลผู้ใช้งานที่ถูกเลือก --}}
                                <div class="card card-primary card-outline">
                                    <div class="card-body box-profile">
                                        <div class="text-center">
                                        <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}">
                                        </div>
                        
                                        <h3 class="profile-username text-center" id="feFirstName"></h3>
                        
                                        <p class="text-muted text-center" id="meternumber2"></p>
                        
                                        <ul class="list-group list-group-unbordered mb-3">
                                        <li class="list-group-item">
                                            <b>ที่อยู่</b> <a class="float-right" id="feInputAddress"></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>เบอร์โทรศัพท์</b> <a class="float-right" id="phone"></a>
                                        </li>
                                        </ul>
                        
                                    </div>
                                    <!-- /.card-body -->
                                    </div>
                                
                            </div><!--col-md-3-->
                            <div class="col-md-9">
                                    {{-- ข้อมูลใบแจ้งหนี้และการชำระ --}}
                                    <input type="hidden" name="mode" id="mode" value="payment">
                                    <input type="hidden" name="user_id" id="user_id" value="">
                                        
                                    <div id="payment_res"> </div>
                                    
                                    <div class="row">
                                        {{-- <div class="col-md-2"></div> --}}
                                        <div class="col-md-3">
                                            <label class="control-label">ยอดที่ต้องชำระ</label>
                                            <input type="text" class="form-control text-bold text-center mustpaid" readonly 
                                                style="font-size: 4rem; width:10rem; height:7rem" name="mustpaid"> 
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">รับเงินมา</label>
                                            <input type="text" class="form-control text-bold text-center cash_from_user" 
                                                style="font-size: 4rem; width:10rem; height:7rem" name="cash_from_user"> 
                                        </div>
                                        <div class="col-md-1 text-bold pt-2 display-2 bottom">=</div>
                                        <div class="col-md-3">
                                            <label class="control-label">เงินทอน</label>
                                            <input type="text" class="form-control text-bold text-center border-success cashback" 
                                                    readonly style="font-size: 4rem; width:10rem; height:7rem" value="" name="cashback"> 
                                        </div>
                                        <button type="submit" class="col-md-2 btn btn-success mt-4  hidden submitbtn">ชำระเงิน</button>
                                        
                                        {{-- <button type="button" class="col-md-2 btn btn-success mt-4  hidden submitbtn"  onclick="check()" >ชำระเงิน</button> --}}
                                    </div>
                            </div><!--class="col-md-9"-->
                        </div><!--row-->   
                    </div><!--activity-->
                </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    
    @endsection


    @section('script')
    <script
        src="https://www.jqueryscript.net/demo/Export-Html-Table-To-Excel-Spreadsheet-using-jQuery-table2excel/src/jquery.table2excel.js">
    </script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="{{asset('/js/my_script.js')}}"></script>
    <script>
        let a = true
        let table;
        //getข้อมูลจาก api มาแสดงใน datatable
        $(document).ready(function () {
            getOweInfos()
        })
        $(document).ready(function () {
            $('.paginate_page').html('หน้า') 
            let val = $('.paginate_of').text()
            $('.paginate_of').text(val.replace('of', 'จาก'));

            //เอาค่าผลรวมไปแสดงตารางบนสุด
            $('#meter_unit_used_sum').html($('.meter_unit_used_sum').val())
            $('#owe_total_sum').html($('.owe_total_sum').val())
            $('#reserve_total_sum').html($('.reserve_total_sum').val())
            $('#all_total_sum').html($('.all_total_sum').val())
        }) //document

       

        $('body').on('click', '#oweTable tbody tr', function () {
            let user_id = $(this).find('td.user_id').text()
            // findReciept(meternumber)

            var tr = $(this).closest('tr');
            var row = table.row( tr );
            console.log('row', row)
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                //หาข้อมูลการชำระค่าน้ำประปาของ  user
            // $.get(`../../api/users/user/${user_id}`).done(function(data){
                let data = 'ss';
                row.child( format2(1)).show();

                    tr.addClass('shown');
                // });
            }
            });  


            function format2(user_id){
                // return user_id;
                // return `<table><tr><th>${user_id}</th></tr></table>`
                let txt= '';
                let i = 0
                $.get(`../api/payment/history/${user_id}/receipt_history`).done(function(invoices){
                $('#receiptInfos').html('<div class="overlay"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>')

                // $('.res').each(function($index){
                //     $(this).removeClass('hidden');
                //     $(this).addClass('show');
                // })  
                if(Object.keys(invoices['reciepts']).length > 0){
                    
                    invoices['reciepts'].forEach((elements, key) => {
                        console.log('invoices[elements]',elements);
                    txt += `
                            <div class="card card-success card-outline">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-4">sfsdf</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    dfsdf
                                </div>
                                
                            </div>

                    `
                        //รอบบิลที่
                        // txt += `
                        //     <div class="card card-success card-outline">
                        //         <div class="card-header">
                        //             <div class="row">
                        //                 <div class="col-md-4 h5 text-bold">
                        //                     ใบเสร็จรับเงินเลขที่  ${elements[0].receipt_id}
                        //                 </div>
                        //                 <div class=" col-md-4 text-center"> วันที่ ${elements[0].receipt_th_date}</div>
                        //                 <div class="col-md-4 text-right">
                        //                     <a href="../payment/print_payment_history/${elements[0].receipt_id}" 
                        //                                                     class="btn btn-primary">ปริ้นใบเสร็จ</a>
                        //                     <a href="../payment/remove/${elements[0].receipt_id}" class="btn btn-warning">ยกเลิกใบเสร็จรับเงิน</a>
                        //                 </div>
                        //             </div><!--row-->                                   
                        //         </div>
                        //         <div class="card-body p-0 table-responsive">
                        //             <table class="table table-striped table-bordered text-nowrap">
                        //                 <thead>
                        //                     <tr>
                        //                     <th class="text-center">เลขใบแจ้งหนี้</th>
                        //                     <th class="text-center">เลขมิเตอร์</th>
                        //                     <th class="text-center">รอบบิล</th>
                        //                     <th class="text-center">ค่าน้ำประปา(บาท)</th>
                        //                     <th class="text-center">ค่ารักษามิเตอร์(บาท)</th>
                        //                     <th class="text-center">คิดเป็นเงิน(บาท)</th>
                        //                     </tr>
                        //                 </thead> 
                        //                 <tbody>
                        // `;
                        // let total = 0; 

                        // elements.forEach((element, index) => {
                        //     //รายละเอียดแต่ละรอบบิล
                        //     let diff = element.currentmeter - element.lastmeter;
                        //     let _total =  diff*8;
                        //     reserve = diff == 0 ? 10 : 0;
                        //     total += _total + reserve; 

                        //     txt +=`<tr>
                        //                 <td class="text-center">${element.iv_id}</td>
                        //                 <td class="text-center">${element.meternumber}</td>
                        //                 <td class="text-center">${element.inv_period_name}</td>
                        //                 <td class="text-right">${_total}</td>
                        //                 <td class="text-right">${reserve}</td>
                        //                 <td class="text-right">${_total + reserve}</td>                            
                        //             </tr>`;    

                        //     i++;
                        //  if(elements.length === i){
                        //      txt += `<tr>
                        //                  <td colspan="5" class="text-success h5">รวมเป็นเงิน </td>
                        //                  <td class="text-right text-success h5">${total}</td>
                        //              </tr>` 
                        //     i = 0;   
                        //  }
                     

                        // });//element  
                        // txt += `
                        //                 </tbody>
                        //             </table>
                        //         </div><!-- card-body -->
                        //      </div><!--card-->
                        // `;  
                                             
                    }); //elements
                  
                }
                console.log('txt',txt)
                return txt;
        
                $('.overlay').html('')
      
            

            });//$.get
            }    

        $('.close').click(()=>{
            $('.modal').removeClass('show')
        })
        
        
        $('.searchBtn').click(function () {
            let zone_id = $('#zone_id').val()
            let subzone_id = $('#subzone_id').val()
            if (zone_id !== 'all' && subzone_id === '') {
                alert('ยังไม่ได้เลือกเส้นทาง')
                $('#subzone_id').addClass('border border-danger rounded')
                return false
            }
            $('#oweTable').DataTable().destroy();
            $('#subzone_id').removeClass('border border-danger rounded')

            getOweInfos(zone_id, subzone_id)
        });
        function getOweInfos(zone_id = 'all', subzone_id = 'all') {
            let params = {
                zone_id: zone_id,
                subzone_id: subzone_id
            }
            $.get(`../api/payment/users`, params).done(function (data) {
                console.log(data)
                if (data.length === 0) {
                    $('.res').html('<div class="card-body h3 text-center">ไม่พบข้อมูล</div>')
                } else {
                    $('.oweCount').html(data.length)
                    $('.header').removeClass('hidden')

                    table = $('#oweTable').DataTable({
                        responsive: true,
                        // order: false,
                        // searching:false,
                        "pagingType": "listbox",
                        "lengthMenu": [
                            [10, 25, 50, 150, -1],
                            [10, 25, 50, 150, "ทั้งหมด"]
                        ],
                        "language": {
                            "search": "ค้นหา:",
                            "lengthMenu": "แสดง _MENU_ แถว",
                            "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                            "infoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                            "paginate": {
                                "info": "แสดง _MENU_ แถว",
                            },

                        },
                        data: data,
                        select:true,
                        columns: [
                            {
                                'title': 'เลขที่',
                                data: 'user_id',
                                'className': 'text-center user_id'
                            },
                            {
                                'title': 'ชื่อ-สกุล',
                                data: 'name'
                            },
                            {
                                'title': 'เลขมิเตอร์',
                                data: 'meternumber',
                                'className': 'text-center',
                            },
                            {
                                'title': 'บ้านเลขที่',
                                data: 'address',
                                'className': 'text-right'
                            },
                            {
                                'title': 'หมู่',
                                data: 'zone_name',
                                'className': 'text-center'
                            },

                            {
                                'title': 'เส้นทางจดมิเตอร์',
                                data: 'subzone_name',
                                'className': 'text-center'
                            },
                            {
                                'title': '',
                                data: 'user_id',
                                'className': 'text-center',
                                'render': function(data){
                                    return `<a href="../payment/receipted_list/${data}" class="btn btn-info">ดูข้อมูล</a>`
                                }
                            }



                        ],
                        
                    }) //table
                    if(a){
                       $('#oweTable thead tr').clone().appendTo('#oweTable thead');
                        a= false
                    }
                    $('#oweTable thead tr:eq(1) th').each( function (index) {
                        var title = $(this).text();
                        $(this).removeClass('sorting')
                        $(this).removeClass('sorting_asc')
                        if(index < 4){
                            $(this).html( `<input type="text" data-id="${index}" class="col-md-12" id="search_col_${index}" placeholder="ค้นหา" />` );
                        }else{
                            $(this).html('')
                        }
                    } );
                } //else
                $('.overlay').remove()
                $('#oweTable_filter').remove()

                //custom การค้นหา
                let col_index = -1
                $('#oweTable thead input[type="text"]').keyup(function(){
                    let that = $(this)
                    var col = parseInt(that.data('id'))

                    if(col !== col_index && col_index !== -1){
                        $('#search_col_'+col_index).val('') 
                        table.column(col_index)
                        .search('')
                        .draw();
                    }
                    setTimeout(function(){ 
                        
                        let _val = that.val()
                        if(col === 0 || col===3){
                            var val = $.fn.dataTable.util.escapeRegex(
                                _val
                            );
                            table.column(col)
                            .search( val ? '^'+val+'.*$' : '', true, false )
                            .draw();
                        }else{
                            table.column(col)
                            .search( _val )
                            .draw();
                        }
                    }, 300);
        
                    col_index = col

                })


            }) //.get

        }

        $('#zone_id').change(function () {
            //get ค่าsubzone 
            $.get(`../api/subzone/${$(this).val()}`)
                .done(function (data) {
                    let text = '<option value="">เลือก</option>';
                    if (data.length > 1) {
                        text += `<option value="all">ทั้งหมด</option>`;
                    }
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.subzone_name}</option>`
                    });
                    $('#subzone_id').html(text)
                });
        });

        $(document).on('click','#checkAll', function(){
            let res = $(this).prop('checked')
            $('.checkbox').prop('checked', res)
            checkboxclicked()
        })

        function  showEmptyDataBox(val){
            $('#activity').removeClass('hidden');
            $('.overlay').html('')
            txt = `<div class="text-center h3 mt-2"><b>${val}</b></div>
                    <div class="text-center text-success h4 mb-2">ไม่มีรายการค้างชำระ</div>`;
            
            $('#activity').html(txt)
        }//showEmptyDataBox

        $('.cash_from_user').keyup(function(){
            let mustpaid = $('.mustpaid').val()
            let cash_from_user = $(this).val()
            let cashback = cash_from_user - mustpaid 
          
            $('.cashback').val(cashback)
            if(cash_from_user === ""){
                $('.cashback').val("")
            }
            if(cashback >= 0){
                mustpaid == 0 ? $('.submitbtn').attr('style', 'display:none') : $('.submitbtn').attr('style', 'display:block')
            }else{
                $('.submitbtn').attr('style', 'display:none')
            }
          
        }); //$('.cash_from_user')

        $(document).on('click','.checkbox', function(){
            checkboxclicked()
        });//$(document).on('click','.checkbox',

        function checkboxclicked(){
            let _total = 0;
           // $('#checkAll').prop('checked', true)

            $('.checkbox').each(function(index, element){
                if ($(this).is(":checked")) {
                    let sum =  $(this).parent().siblings('.total').text()
                    _total += parseInt(sum)     
                }else{
                    $('#checkAll').prop('checked', false)

                }
            });
            if( $('.cash_from_user').val() > 0){
                let remain = $('.cash_from_user').val() - _total
                $('.cashback').val(remain)
            }
            
            if(_total == 0){
                $('.cash_form_user').attr('readonly')
            }else if(_total > 0){
                $('.cash_form_user').removeAttr('readonly')
            }

            $('.mustpaid').val(_total)
        }

        $(document).ready(function(){
            let _total = 0;
            $('.checkbox').each(function(index, element){
                if ($(this).is(":checked")) {
                    let sum =  $(this).parent().siblings('.total').text()
                    _total += parseInt(sum)    
                }
            });
            $('.mustpaid').val(_total)
            $('.cash_from_user').val(0)
            $('.cashback').val(0)
        });//$(document).ready(function()

        function check(){
            let checkboxChecked = false;
            let cashbackRes = false;
            let errText = '';
            $('.checkbox:checked').each(function(index, element){
                checkboxChecked = true;
            });
            if(checkboxChecked == false){
                errText += '- ยังไม่ได้เลือกรายการค้างชำระ\n';
            }
            let mustpaid = $('.mustpaid').val()
            let cash_from_user = $('.cash_from_user').val()
            let cashback = cash_from_user - mustpaid 
            if(!Number.isNaN(cashback) && cashback >= 0){
                cashbackRes = true;
            }else{
                errText += '- ใส่จำนวนเงินไม่ถูกต้อง'
            }
            if(checkboxChecked == false || cashbackRes == false){
                alert(errText)
                return  false;
            }else{
                return true;
                // let inv_id = [];
                // $('.checkbox').each(function(index, element){
                //     if ($(this).is(":checked")) {
                //         inv_id.push($(this).attr('data-inv_id'))
                //     }

                // });
                // let params = { 
                //     "mustpaid": $('.mustpaid').val() , 
                //     "inv_id": inv_id
                // }

                // var csrf = document.querySelector('meta[name="csrf-token"]').content;
                // $.ajax({
                //     url: '../../api/payment/store',
                //     type: "POST",
                //     data: { 'value': params, '_token': csrf },
                //     success: function (response) {
                //         console.log('response', response)
                //         window.location.href = '../../payment/receipt_print/'+response
                //     }
                // });

            }
            
            
        }

    </script>
    @endsection