@extends('layouts.adminlte')

@section('mainheader')
<?php 
  if(collect($currentBudgetYear)->isNotEmpty()){
    echo "ปีงบประมาณ". $currentBudgetYear->budgetyear;
   }
?>
@endsection
@section('nav')
<a href="{{url('/dashboard/index')}}"> หน้าหลัก</a>

@endsection
@section('dashboard')
    active
@endsection

@section('content')
{{-- ถ้ายังไม่ได้ตั้งค่าทั่วไปให้ไปตั้งก่อน --}}

<section class="content">
  @if (collect($settings)->count() == 0)
      <div class="card">
        <div class="card-body text-center">
          <h4>ยังไม่ได้ทำการตั้งค่าทั่วไป</h4>
          <a href="{{url('settings')}}" class="btn btn-info">ตั้งค่าทั่วไป</a>
        </div>
      </div>
  @else
  <h2>งานประปา {{ $tambonInfos['organization_name'] }}</h2>
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h4>{{number_format($membersTotal)}}<sup style="font-size: 20px"> คน</sup></h4>

              <p>สมาชิก</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{url('users')}}" class="small-box-footer">ดูข้อมูลเพิ่มเติม <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h4>{{number_format($all_total_water_used)}}<sup style="font-size: 20px"> ม.<sup>3</sup></sup></h4>

              <p>ใช้น้ำแล้ว</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url('reports/water_used')}}" class="small-box-footer">ดูข้อมูลเพิ่มเติม <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h4>{{ number_format($total_paid) }}<sup style="font-size: 20px"> บาท</sup></h4>

              <div>เก็บเงินได้   </div>
              <span class="text-sm"> ( รวมค่ารักษามิเตอร์ {{number_format($all_reserve_meter_status_paid)}} <sup>หน่วย</sup> )</span>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{url('reports/payment')}}" class="small-box-footer">ดูข้อมูลเพิ่มเติม <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h4>{{number_format($total_owe_and_invoice)}}<sup style="font-size: 20px"> บาท</sup></h4>

              <p>ยอดค้างชำระ </p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{url('reports/owe')}}" class="small-box-footer">ดูข้อมูลเพิ่มเติม <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
       
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">
                <i class="fas fa-users mr-1"></i>
                จำนวนสมาชิกแยกตามเส้นทางจัดเก็บ
              </h4>
        
            </div><!-- /.card-header -->
            <div class="card-body" id="stocks-div" style="height: 400px;">
                <!-- Morris chart - Sales -->
                <div class="chart tab-pane active" id="invoiceChart"
                     style="position: relative; height: 300px;">
                 </div>        
             
                  @columnchart('membersBysubzone', 'invoiceChart')
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->


          <!-- /.card -->
        </section>
        @if (isset($currentBudgetYear->budgetyear))
          <section class="col-12">
            <div class="row">
              <div class="col-12">
                <div class="card direct-chat direct-chat-primary">
                  <div class="card-header">
                    <h4 class="card-title">ประมาณการใช้น้ำ ปี {{$currentBudgetYear->budgetyear}} แยกตามเส้นทางจัดเก็บ</h4>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body mb-3">
                    <div class="chart tab-pane active" id="water_used_by_subzone" style="width: 100%; height: 400px;">
                      </div>    
                  </div>
                    @columnchart('waterUsedBysubzone', 'water_used_by_subzone')
            
                </div>
              </div>
            </div>
          </section>
        @endif
       
       
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->

  @endif
  </section>
          
@endsection

{{-- <script src="{{asset('/js/app.js')}}"></script> --}}
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

@section('script')
  <script>
    

      var d = new Date();
      var mTemp = d.getMonth()+1;
      var m = mTemp < 10 ? `0${mTemp}` : mTemp;
      var y = d.getFullYear();
      var curr_d = new Date(y, m , 0);
      var curr_d_split = curr_d.toString().split(" "); 
      start = `${y}-${m}-01`;
      end   = `${y}-${m}-${curr_d_split[2]}`;

     
</script>

@endsection