@extends('layouts.payment')

@section('title')
    จ่ายค่าน้ำประปา
@endsection


@section('content')
  <div class="container-fluid mt--7">
     @include('/admin/form');
  </div>
@endsection