@extends('layouts.payment')

@section('title')
    ประวัติการจ่ายค่าน้ำประปา ของ นายสมชาย แสงวงค์
@endsection

@section('content')
  <div class="container-fluid mt--7">
      <div class="row">
          <div class="col-xl-5 order-xl-2 mb-5 mb-xl-0">
              <div class="card card-profile shadow">
                  <div class="row justify-content-center">
                        <div class="col-lg-7 order-lg-2">
                                <div class="text-center">
                                        <h3>
                                            Jessica Jones<span class="font-weight-light"></span>
                                        </h3>
                                        <div class="h5 font-weight-300">
                                            <i class="ni location_pin mr-2"></i>สมาชิกผู้ใช้น้ำ
                                        </div>
                                      
                                    </div>
                          </div>
                      <div class="col-lg-5 order-lg-2">
                          <div class="card-profile-image">
                              <a href="#">
                                  <img src="{{asset('argon/img/theme/team-4-800x800.jpg')}}" class="rounded-circle">
                              </a>
                          </div>
                      </div>

                  </div>
                  <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-5">
                  </div>
                 
              </div>
          </div>
          <div class="col-xl-7 order-xl-1">
              <div class="card bg-secondary shadow">
                  <div class="card-header bg-white border-0">
                      <div class="row align-items-center">
                          <div class="col-8">
                              <form class="navbar-search  form-inline mr-3 d-none d-md-flex ml-lg-auto">
                                    <div class="form-group ml-6">
                                      <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text"><i class="fas fa-search"></i></span>
                                        </div>
                                        <input class="form-control" id="memberSearch" placeholder="Search" type="text">
                                      </div>
                                    </div>
                                  </form>
                          </div>
                          <div class="col-4 text-right">
                              <a href="#!" class="btn btn-lg  btn-primary">Print</a>
                          </div>
                      </div>
                  </div>
                  <div class="card-body">
                      
                  </div>
              </div>
              
          </div>
      </div>
      <div class="row">
            <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush" id="example">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Project</th>
                                <th scope="col">Budget</th>
                                <th scope="col">Status</th>
                                <th scope="col">Users</th>
                                <th scope="col">Completion</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/bootstrap.jpg')}}">
                                        </a>
                                        <div class="media-body">
                                            <span class="mb-0 text-sm">Argon Design System</span>
                                        </div>
                                    </div>
                                </th>
                                <td>
                                    $2,500 USD
                                </td>
                                <td>
                                    <span class="badge badge-dot mr-4">
                                        <i class="bg-warning"></i> pending
                                    </span>
                                </td>
                                <td>
                                    <div class="avatar-group">
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Ryan Tompson">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-1-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Romina Hadid">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-2-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Alexander Smith">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-3-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Jessica Doe">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-4-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <span class="mr-2">60%</span>
                                        <div>
                                            <div class="progress">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                    aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                    style="width: 60%;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0)" onClick="getUserBillInfo(1)">ดูรายละเอียด</a>
                                            
                                    
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/angular.jpg')}}">
                                        </a>
                                        <div class="media-body">
                                            <span class="mb-0 text-sm">Angular Now UI Kit PRO</span>
                                        </div>
                                    </div>
                                </th>
                                <td>
                                    $1,800 USD
                                </td>
                                <td>
                                    <span class="badge badge-dot">
                                        <i class="bg-success"></i> completed
                                    </span>
                                </td>
                                <td>
                                    <div class="avatar-group">
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Ryan Tompson">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-1-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Romina Hadid">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-2-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Alexander Smith">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-3-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Jessica Doe">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-4-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <span class="mr-2">100%</span>
                                        <div>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width: 100%;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0)" onClick="getUserBillInfo(1)">ดูรายละเอียด</a>
                                    
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/sketch.jpg')}}">
                                        </a>
                                        <div class="media-body">
                                            <span class="mb-0 text-sm">Black Dashboard</span>
                                        </div>
                                    </div>
                                </th>
                                <td>
                                    $3,150 USD
                                </td>
                                <td>
                                    <span class="badge badge-dot mr-4">
                                        <i class="bg-danger"></i> delayed
                                    </span>
                                </td>
                                <td>
                                    <div class="avatar-group">
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Ryan Tompson">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-1-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Romina Hadid">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-2-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Alexander Smith">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-3-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Jessica Doe">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-4-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <span class="mr-2">72%</span>
                                        <div>
                                            <div class="progress">
                                                <div class="progress-bar bg-danger" role="progressbar"
                                                    aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"
                                                    style="width: 72%;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0)" onClick="getUserBillInfo(1)">ดูรายละเอียด</a>
                                            
                                    
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/react.jpg')}}">
                                        </a>
                                        <div class="media-body">
                                            <span class="mb-0 text-sm">React Material Dashboard</span>
                                        </div>
                                    </div>
                                </th>
                                <td>
                                    $4,400 USD
                                </td>
                                <td>
                                    <span class="badge badge-dot">
                                        <i class="bg-info"></i> on schedule
                                    </span>
                                </td>
                                <td>
                                    <div class="avatar-group">
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Ryan Tompson">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-1-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Romina Hadid">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-2-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Alexander Smith">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-3-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Jessica Doe">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-4-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <span class="mr-2">90%</span>
                                        <div>
                                            <div class="progress">
                                                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90"
                                                    aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0)" onClick="getUserBillInfo(1)">ดูรายละเอียด</a>
                                            
                                    
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/vue.jpg')}}">
                                        </a>
                                        <div class="media-body">
                                            <span class="mb-0 text-sm">Vue Paper UI Kit PRO</span>
                                        </div>
                                    </div>
                                </th>
                                <td>
                                    $2,200 USD
                                </td>
                                <td>
                                    <span class="badge badge-dot mr-4">
                                        <i class="bg-success"></i> completed
                                    </span>
                                </td>
                                <td>
                                    <div class="avatar-group">
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Ryan Tompson">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-1-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Romina Hadid">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-2-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Alexander Smith">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-3-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip"
                                            data-original-title="Jessica Doe">
                                            <img alt="Image placeholder" src="{{asset('argon/img/theme/team-4-800x800.jpg')}}"
                                                class="rounded-circle">
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <span class="mr-2">100%</span>
                                        <div>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                                    style="width: 100%;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0)" onClick="getUserBillInfo(1)">ดูรายละเอียด</a>
                                            
                                    
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
      </div>

  </div>

  <!---------------------->

    <div id="printThis">
        <div id="MyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true">
    
            <div class="modal-dialog modal-lg">
    
                <!-- Modal Content: begins -->
                <div class="modal-content">
    
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                    </div>
    
                    <!-- Modal Body -->
                    <div class="modal-body" id="modal-body">
                        <div class="body-message">
                            <h4>Any Heading</h4>
                            <p>And a paragraph with a full sentence or something else...</p>
                        </div>
                    </div>
    
                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button id="btnPrint" type="button" class="btn btn-default">Print</button>
                    </div>
    
                </div>
                <!-- Modal Content: ends -->
    
            </div>
        </div>
    </div>
@endsection