<div class="row">
    <div class="col-xl-4 order-xl-1 mb-5 mb-xl-0">
        <div class="card card-profile shadow">
            <div class="row justify-content-center">
                <div class="col-lg-3 order-lg-2">
                    <div class="card-profile-image">
                        <a href="#">
                            <img src="{{asset('argon/img/theme/team-4-800x800.jpg')}}" class="rounded-circle">
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
            </div>
            <div class="card-body pt-0 pt-md-4">
                @if ($formMode == 'create')

                <div class="card-profile-stats  mt-md-5">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">เลือกผู้ใช้น้ำประปา</label>
                        <select id="input-username"
                            class="form-control form-control-alternative js-example-basic-single">

                        </select>
                    </div>
                </div>

                @else
                <div class="row">
                    <div class="col">
                        <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                            <div>
                                <span class="heading">22</span>
                                <span class="description">Friends</span>
                            </div>
                            <div>
                                <span class="heading">10</span>
                                <span class="description">Photos</span>
                            </div>
                            <div>
                                <span class="heading">89</span>
                                <span class="description">Comments</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <h3>
                        Jessica Jones<span class="font-weight-light"></span>
                    </h3>
                    <div class="h5 font-weight-300">
                        <i class="ni location_pin mr-2"></i>สมาชิกผู้ใช้น้ำ
                    </div>
                    <div class="h5 mt-4">
                        <i class="ni business_briefcase-24 mr-2"></i>
                    </div>
                    <div>
                        <i class="ni education_hat mr-2"></i>208/12 ตำบลห้องแซง อำเภอเลิงนกทา จังหวัดยโสธร
                    </div>
                    <hr class="my-4">
                    {{-- <p>Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music.</p>
                            <a href="#">Show more</a> --}}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-xl-8 order-xl-2">
        <div id="form1" class="card bg-secondary shadow hidden">
            <div class="card-header bg-white border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">My account</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group focused">
                                <label class="form-control-label" for="input-username">รหัสรอบบิล</label>
                                <input type="text" id="input-username" class="form-control form-control-alternative"
                                    placeholder="Username" value="HZ-W-INV-006" readonly>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label" for="input-email">ปีงบประมาณ</label>
                                <input type="email" id="input-email" class="form-control form-control-alternative"
                                    placeholder="2562" readonly>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group focused">
                                <label class="form-control-label" for="input-first-name">รอบบิลที่</label>
                                <input type="text" id="input-first-name" class="form-control form-control-alternative"
                                    placeholder="First name" value="05-62" readonly>
                            </div>
                        </div>



                    </div>
                    <!-- Address -->
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="input-city">ราคา:หน่วย</label>
                                    <input type="text" id="input-city" class="form-control form-control-alternative"
                                        value="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="input-country">ยอดจดครั้งก่อน</label>
                                    <input type="text" id="input-country" class="form-control form-control-alternative"
                                        value="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-country">จำนวนน้ำที่ใช้</label>
                                    <input type="number" id="input-postal-code"
                                        class="form-control form-control-alternative">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group focused">
                                    <label class="form-control-label text-green"
                                        for="input-city">คิดเป็นเงิน(บาท)</label>
                                    <input type="text" id="input-city" class="form-control is-valid" value="" readonly>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Description -->
                    <div class="form-group focused">
                        <textarea rows="2" class="form-control form-control-alternative"
                            placeholder="หมายเหตุ"></textarea>
                    </div>
                    <div class="col-4">
                        <a href="#!" class="btn btn-lg  btn-primary pull-right">บันทึก</a>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
