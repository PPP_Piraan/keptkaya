@extends('layouts.app')
@section('title')
หน่วยนับขยะ
@endsection

@section('sidebar')
@include('settings.settings_sidebar',['index' => 3])
@endsection
@section('content')

@if (session('message'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info alert-with-icon" data-notify="container">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="fas fa-fw fa-plus"></i>
            </button>
            <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
            <span data-notify="message">{{session('message')}}</span>
        </div>

    </div>
</div>

@endif


<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-md-6"> <h6 class="m-0 font-weight-bold text-primary">รายการหน่วยนับ</h6></div>
            <div class="col-md-6"> <a href="{{url('settings/units/create')}}" class="btn btn-info float-right">สร้างหน่วยนับ</a></div>

        </div>
       
       
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                            @if (count($units) >  0)

                        <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid"
                            aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">ชื่อหน่วยนับ</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1">ชื่อย่อหน่วยนับ</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">ชื่อหน่วยนับ</th>
                                    <th rowspan="1" colspan="1">ชื่อย่อหน่วยนับ</th>
                                    <th rowspan="1" colspan="1">สถานะ</th>
                                    <th rowspan="1" colspan="1"></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($units as $unit)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{$unit->c_unit_name}}</td>
                                        <td>{{$unit->c_unit_short_name}}</td>
                                        <td>{{$unit->c_unit_stuse}}</td>
                                        <td>
                                            <a href="{{url('settings/units/edit', $unit->count_unit_id)}}" class="btn btn-warning" title="แก้ไข"><i class="fas fa-fw fa-wrench"></i></a>
                                            <a href="{{url('settings/units/delete', $unit->count_unit_id)}}" class="btn btn-danger delete_btn" title="ลบ"><i class="fas fa-fw fa-trash"></i></a>
                                        </td>
                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2 class="text-center"> ไม่พบข้อมูล</h2>
                        @endif

                    </div>
                </div>
               
            </div>
        </div>
    </div><!-- card-body-->
</div>



@endsection
