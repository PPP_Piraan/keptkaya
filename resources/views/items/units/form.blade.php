<div class="form-group">
    <label for="area_name" class="control-label">ชื่อหน่วยนับขยะ</label>
    <input type="text" name="unit_name" value="{{isset($unit) ? $unit->c_unit_name : ''}}" class="form-control">
</div>
<div class="form-group">
    <label for="area_name" class="control-label">ชื่อย่อหน่วยนับขยะ</label>
    <input type="text" name="unit_short_name" value="{{isset($unit) ? $unit->c_unit_short_name : ''}}" class="form-control">
</div>
<div class="form-group">
    <label for="area_name" class="control-label">สถานะ</label>
    <select name="status" class="form-control">
        <option value="inactive" {{$unit->c_unit_status == 'inactive' ? 'selected' : ''}}>inactive</option>
        <option value="active" {{$unit->c_unit_status == 'active' ? 'selected' : ''}}>active</option>
    </select>
</div>
<div class="form-group">
    <button type="submit" class="btn {{$formMode == 'create' ? 'btn-success' : 'btn-warning'}}">
        {{$formMode == 'create' ? 'บันทึก' : 'บันทึกการแก้ไข'}}
    </button>
</div>
