@extends('layouts.app')
@section('title')
     แก้ไขหน่วยนับขยะ
@endsection

@section('sidebar')
    @parent
    @include('settings.settings_sidebar',['index' => 7])
@endsection

@section('content')
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <h6 class="card-title">ฟอร์มแก้ไขหน่วยนับขยะ</h6>
                </div>
                <div class="card-body ">
                    <form action="{{url('settings/units/update/'.$unit->count_unit_id)}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        @include('settings/units.form', ['formMode' => 'update'])
                    </form>
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col md 12 -->
    </div><!-- row -->
</div><!-- container-fluid -->                  

@endsection