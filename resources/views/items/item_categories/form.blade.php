<div class="form-group">
    <label for="area_name" class="control-label">ชื่อประเภทขยะ</label>
    <input type="text" name="trash_category_name" value="{{isset($trash_category) ? $trash_category->name : ''}}" class="form-control">
</div>

<div class="form-group">
    <label for="area_name" class="control-label">สถานะ</label>
    <select name="status" class="form-control">
        <option value="inactive" {{$trash_category->status == 'inactive' ? 'selected' : ''}}>inactive</option>
        <option value="active" {{$trash_category->status == 'active' ? 'selected' : ''}}>active</option>
    </select>
</div>
<div class="form-group">
    <button type="submit" class="btn {{$formMode == 'create' ? 'btn-success' : 'btn-warning'}}">
        {{$formMode == 'create' ? 'บันทึก' : 'บันทึกการแก้ไข'}}
    </button>
</div>
