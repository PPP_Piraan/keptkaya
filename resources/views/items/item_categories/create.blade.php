@extends('layouts.app')
@section('title')
สร้างหน่วยนับขยะ
@endsection

@section('sidebar')
@parent
@include('trash/trash_sidebar',['index' => 7])
@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <h6 class="card-title">ฟอร์มสร้างประเภทขยะ</h6>
                </div>
                <div class="card-body ">
                    <form action="{{url('settings/trash_categories/store')}}" method="post" class="form-horizontal">
                        @csrf
                        @include('settings/trash_categories.form', ['formMode' => 'create'])
                    </form>
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col md 12 -->
    </div><!-- row -->
</div><!-- container-fluid -->

@endsection
